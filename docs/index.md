---
hide:
  - navigation
  - toc
---

# **C/C++ starter kit**
_________________

Welcome to the **C/C++ starter kit documentation index**.

* Check **[requirement](requirement.md)** to learn about the different **operating systems** and their specific characteristics on which the example programs were carried out.
* Check **[tools](tools.md)** to find more information about a **C/C++ tool**.
* Check **[C/C++](C++.md)** to get more information about a **C/C++ language** himself.

<br>

Each **tool** is covered following the same page model:

* A brief `presentation` of the tool.
* An `installation` chapter on how to **install** the **tool** and its **dependencies**.
* A `how to use` segment covering the **basic usage** of the tool with **C/C++ code examples**.
* A `Pros and cons` section that reviews the **advantages** and **disadvantages** of the tool.
* A `extra` part that will cover **related framework**, **plugin** or **extensions** to the tool.