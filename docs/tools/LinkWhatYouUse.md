---
hide:
  - navigation
---

# **LINK_WHAT_YOU_USE** :star::star::star::star:
_________________

**[LINK_WHAT_YOU_USE](https://cmake.org/cmake/help/latest/prop_tgt/LINK_WHAT_YOU_USE.html)** is a `CMake` option that controls how the linker resolves symbols when building a `C/C++` project.

It is a `CMake` feature related to the `Link Time Optimization (LTO)` of `C/C++` code.

With `LTO`, the compiler generates intermediate object files with extra information that is not normally available during the compilation of individual translation units.

This information is used at the linking stage to perform additional optimizations across all object files, resulting in potentially faster and smaller executables.

The `CMAKE_LINK_WHAT_YOU_USE` variable checks the dependencies used by the project and removes the unused ones.

## Installation
_________________

To utilize the `CMake` property **LINK_WHAT_YOU_USE**, **[CMake](https://cmake.org/)** installed.

``` bash
sudo apt install cmake
```

???+ info
    This tutorial will use `CMake` version 3.29.0

## How to use
_________________

Setting the **LINK_WHAT_YOU_USE** `CMake` property to `ON`, allows `CMake` to configure the build process to perform `LTO`.

???+ note
    For now, it is only supported for **[ELF](https://fr.wikipedia.org/wiki/Executable_and_Linkable_Format)** platforms and is applicable to executable and shared or module library targets.

    This property will be ignored for any other targets and configurations.

`LINK_WHAT_YOU_USE` executes `ldd -u -r`.

This command is executed after the linking step to inspect library usage.

`ldd` is a `Unix` utility that prints the shared object dependencies.

The `-u` option stands for `--unused` and prints the unused direct dependencies, whereas `-r` is related to `--function-relocs`, an option that performs relocations for both data objects and functions and reports any missing objects or functions.

Here is a basic example illustrating an unused dependency linked to the `main` executable:

``` cpp title="addition.hpp" linenums="1"
int addition(int a, int b);
```

``` cpp title="addition.cpp" linenums="1"
#include "addition.hpp"

int addition(int a, int b)
{
    return a + b;
}
```

``` cpp title="substraction.hpp" linenums="1"
int substraction(int a, int b);
```

``` cpp title="substraction.cpp" linenums="1"
#include "substraction.hpp"

int substraction(int a, int b)
{
    return a - b;
}
```

``` cpp title="main.cpp" linenums="1"
#include <iostream>

#include "addition.hpp"

int main()
{
    std::cout << addition(5, 3) << std::endl;

    return 0;
}
```

``` cmake title="CMakeLists.txt" linenums="1" hl_lines="5"
cmake_minimum_required(VERSION 3.29)

project(LINK_WHAT_YOU_USE LANGUAGES C CXX)

set(CMAKE_LINK_WHAT_YOU_USE ON)

add_library(Addition SHARED addition.cpp addition.hpp)
add_library(Substraction SHARED substraction.cpp substraction.hpp)

add_executable(Exe main.cpp)

target_link_libraries(Exe PRIVATE Addition Substraction)
```

The last step is to configure and build the cmake project.

The unused dependencies can be read on the logs.

``` bash linenums="1" hl_lines="9 10"
[ 16%] Building CXX object CMakeFiles/Addition.dir/addition.cpp.o
[ 33%] Linking CXX shared library libAddition.so
[ 33%] Built target Addition
[ 50%] Building CXX object CMakeFiles/Substraction.dir/substraction.cpp.o
[ 66%] Linking CXX shared library libSubstraction.so
[ 66%] Built target Substraction
[ 83%] Building CXX object CMakeFiles/Exe.dir/main.cpp.o
[100%] Linking CXX executable Exe
Warning: Unused direct dependencies:
	/home/CODE/LINK_WHAT_YOU_USE/build/libSubstraction.so
[100%] Built target Exe
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Straightforward setup process
    - [x] Direct integration with CMake
???+ danger "Cons"
    - [ ] Not supported on all targets and configurations