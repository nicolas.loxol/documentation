---
hide:
  - navigation
---

# **Mkdocs** :star::star::star::star:
_________________

**[MkDocs](https://www.mkdocs.org/)** is a fast, simple, and downright gorgeous static site generator geared towards building project documentation.

Documentation source files are written in `Markdown` and configured with a single `YAML` configuration file.

## Installation
_________________

???+ warning
    `Python` is required to use **MkDocs**.

    === "Windows"

        Download the latest `Python` release on the [official website](https://www.python.org/downloads/windows/)

    === "Unix-like system"

        ``` bash
        sudo apt install python3
        ```

To install **MkDocs**, run the following command:

``` bash
pip install mkdocs
```

???+ info
    This tutorial will use **MkDocs** version 1.6.0

## How to use
_________________

Various commands and options are available.

For a complete list of commands, use the `--help` flag:

``` bash
mkdocs --help
```

### Creating a new project

Getting started is very easy.

To create a new project, run the following command in the command line:

``` bash
mkdocs new my-project-name
```

![Basic Mkdocs project](../ressources/img/mkdocs_project.png)

There is a single configuration file named `mkdocs.yml` and a folder named `docs` that will contain the documentation source files.

Right now, the `docs` folder contains a single documentation page named `index.md`.

**MkDocs** comes with a built-in dev server that previews the documentation as work is done on it.

Go to the same directory as the `mkdocs.yml` configuration file, and then start the server by running the `mkdocs serve` command:

``` bash
$ mkdocs serve
INFO    -  Building documentation...
INFO    -  Cleaning site directory
[I 160402 15:50:43 server:271] Serving on http://127.0.0.1:8000
[I 160402 15:50:43 handlers:58] Start watching changes
[I 160402 15:50:43 handlers:60] Start detecting changes
```

Open `http://127.0.0.1:8000/`, and the default home page will be displayed:

![Basic Mkdocs index page](../ressources/img/mkdocs_generated.png)

### Adding pages

Documentation pages can be added by creating new `Markdown` files in the `docs` folder.

The generated page `index.md` shows some examples of `Markdown` usage.

### Theming the documentation

**MkDocs** includes two built-in themes (`mkdocs` and `readthedocs`).

However, many third party themes are available to choose from as well.

To choose a theme, set the theme configuration option in the `mkdocs.yml` config file.

``` yaml title="mkdocs.yml" hl_lines="6"
site_name: MyWebsite
site_url: https://example.com/
nav:
    - Home: index.md
    - About: about.md
theme: readthedocs
```

### Building the site

Build the documentation using the following command:

``` bash
mkdocs build
```

This will create a new directory, named `site` containing the site in `HTML` format.

### Deploying

#### Github Pages

For projects hosted on [GitHub](https://github.com/), [GitHub Pages](https://pages.github.com/) can be used to easily host the project's documentation.

Project Pages sites are deployed to a branch within the project repository, typically the `gh-pages` branch by default.

After checking out the primary working branch (usually `master` or `main`) of the repository, the documentation can be deployed using the following command:

``` bash
mkdocs gh-deploy
```

Behind the scenes, **MkDocs** will build the documentation and utilize the `ghp-import` tool to commit them to the `gh-pages` branch, then push the `gh-pages` branch to `GitHub`.

#### Gitlab Pages

For projects hosted on [GitLab](https://about.gitlab.com/), deploying to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) can be accomplished using the `GitLab CI task runner`.

At the root of the repository, create a file named `.gitlab-ci.yml` and paste the following contents:

``` yaml title=".gitlab-ci.yml" linenums="1"
image: python:latest
pages:
  stage: deploy
  script:
    - pip install mkdocs
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
```

Moving forward, if the documentation is modified locally and pushed to the remote repository, the `GitLab CI runner` will automatically deploy the updated documentation.

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Huge amount of plugins
    - [x] Infinite possibilities of customization

???+ danger "Cons"
    - [ ] `Markdown` lacks certain advanced features found in `HTML`
    - [ ] Some plugins require payment
    - [ ] No direct `GitLab` deployment

## Extra
_________________

### Using material theme

**[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)** is a theme designed specifically for **MkDocs**.

To install `Material for MkDocs`, download the package using `pip`.

``` bash
pip install mkdocs-material
```

Once installed, simply add the following lines to the `mkdocs.yml` file to enable the `material` theme:

``` yaml title="mkdocs.yml"
theme:
  name: material
```

???+ tip "Recommended: configuration validation and auto-complete"

    In order to minimize friction and maximize productivity, `Material for MkDocs`
    provides its own `schema.json` for `mkdocs.yml`.

    It is recommended to set up `YAML` schema validation.

    === "Visual Studio Code"

        1.  Install `vscode-yaml` for `YAML` language support.
        2.  Add the schema under the `yaml.schemas` key in the user or workspace `settings.json`:

            ``` json
            {
              "yaml.schemas": {
                "https://squidfunk.github.io/mkdocs-material/schema.json": "mkdocs.yml"
              },
              "yaml.customTags": [
                "!ENV scalar",
                "!ENV sequence",
                "tag:yaml.org,2002:python/name:materialx.emoji.to_svg",
                "tag:yaml.org,2002:python/name:materialx.emoji.twemoji",
                "tag:yaml.org,2002:python/name:pymdownx.superfences.fence_code_format"
              ]
            }
            ```

    === "Other"

        1.  Ensure the editor of choice has support for `YAML` schema validation.
        2.  Add the following lines at the top of `mkdocs.yml`:

            ``` yaml
            # yaml-language-server: $schema=https://squidfunk.github.io/mkdocs-material/schema.json
            ```

### An example of configuration file

``` yaml title="mkdocs.yml" linenums="1"
# yaml-language-server: $schema=https://squidfunk.github.io/mkdocs-material/schema.json

# Project information
site_name: MyWebsite
site_url: https://example.com

# Repository
repo_name: myrepo/myproject
repo_url: https://gitlab.com/myprojecturl

# Copyright
copyright: Copyright &copy; 2023 - XXXX

# Configuration
theme: # (1)
  name: material
  logo: XXX.png
  favicon: XXX.png
  palette:
    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
  features:
    - navigation.tabs
    - search.suggest
    - navigation.footer
    - content.code.copy
    - content.code.annotate
    - content.tabs.link

# Customization
extra:
  social:
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/XXXX
    - icon: fontawesome/brands/linkedin
      link: https://www.linkedin.com/in/XXXX
  generator: true

# Plugins
plugins: # (2)
  - search
  - git-revision-date-localized:
      enable_creation_date: true
      type: date

# Extensions
markdown_extensions: # (3)
  - admonition
  - abbr
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true

# Page tree
nav:
  - Home : index.md
  - Getting started:
    - Requirement: requirement.md
  - Tools:
    - Documentation:
      - Doxygen: tools/Doxygen.md
      - MkDocs: tools/MkDocs.md
    - IDE:
      - Visual Studio: tools/VisualStudio.md
      - Visual Studio Code: tools/VisualStudioCode.md
```

1. Refer to : [https://squidfunk.github.io/mkdocs-material/setup/](https://squidfunk.github.io/mkdocs-material/setup/)

2. Refer to : [https://squidfunk.github.io/mkdocs-material/plugins/](https://squidfunk.github.io/mkdocs-material/plugins/)

3. Refer to : [https://squidfunk.github.io/mkdocs-material/reference/](https://squidfunk.github.io/mkdocs-material/reference/)