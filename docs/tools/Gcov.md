---
hide:
  - navigation
---

# **Gcov** :star::star::star::star::star:
_________________

**[Gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html)** is a source code coverage analysis and statement-by-statement profiling tool.

It generates exact counts of the number of times each statement in a program is executed and annotates the source code to add instrumentation.

**Gcov** is included as a standard utility with the `GNU Compiler Collection (GCC)` suite.

It provides information on how often a program executes segments of code, producing a copy of the source file annotated with execution frequencies.

???+ info
    While **Gcov** does not produce any time-based data and works only on code compiled with `GCC`, it is also compatible with LLVM-generated files, despite claims in the manual that it is not compatible with any other profiling or test coverage mechanism.

## Installation
_________________

As mentioned earlier, **Gcov** comes as a standard utility with the `GNU Compiler Collection (GCC)` suite.

=== "C"

    ``` bash
    sudo apt install gcc
    ```

=== "C++"

    ``` bash
    sudo apt install g++
    ```

???+ info
    This tutorial will use `GCC` version 13.2

## How to use
_________________

### Create a log file

**Gcov** produces a test coverage analysis of a specially instrumented program.

The options `-fprofile-arcs` (record branch statistics) and  `-ftest-coverage` (save line execution count) should be used to compile the program for coverage analysis, `-lgcov` should also be used to link the program.

The `-lgcov` option should also be used to link the program.

???+ info
    The option `--coverage` of the `gcc` compiler is equivalent to `fprofile-arcs`, `-ftest-coverage` and `-lgcov` arguments

Running the instrumented program will create several files with `.gcno` (on compilation) and `.gcda` (on execution) extensions.

These files can be analyzed by **Gcov**.

=== "C"

    ``` bash
    gcc --coverage main.c -o main
    ./main
    ```

=== "C++"

    ``` bash
    g++ --coverage main.cpp -o main
    ./main
    ```

For each source file, **Gcov** creates an associated log file with the `.gcov` extension.

These files indicate how many times each line of the associated source file has been executed.

Each line of source code is prefixed with the number of times it has been executed; lines that have not been executed are prefixed with `#####`.

=== "C"

    ``` bash
    gcov main.c
    ```

    ```c++ title="main.c.gcov" linenums="1"
    -:    0:Source:main.c
    -:    0:Graph:main.gcno
    -:    0:Data:main.gcda
    -:    0:Runs:1
    1:    1:int addition(int a, int b)
    -:    2:{
    1:    3:	return a + b;
    -:    4:}
    -:    5:
    #####:    6:int soustraction(int a, int b)
    -:    7:{
    #####:    8:	return a - b;
    -:    9:}
    -:   10:
    1:   11:int test()
    -:   12:{
    1:   13:	int a = 1;
    1:   14:    int j = 0;
    -:   15:
    1:   16:	if(a)
    -:   17:	{
    1:   18:		j = 5;
    -:   19:	}
    -:   20:	else
    -:   21:	{
    #####:   22:		j = 3;
    -:   23:	}
    -:   24:
    1:   25:	return j;
    -:   26:}
    -:   27:
    1:   28:int main()
    -:   29:{
    1:   30:    test();
    1:   31:	addition(5, 3);
    -:   32:
    1:   33:	return 0;
    -:   34:}
    ```

=== "C++"

    ``` bash
    gcov main.cpp
    ```

    ```c++ title="main.cpp.gcov" linenums="1"
    -:    0:Source:main.cpp
    -:    0:Graph:main.gcno
    -:    0:Data:main.gcda
    -:    0:Runs:1
    1:    1:int addition(int a, int b)
    -:    2:{
    1:    3:	return a + b;
    -:    4:}
    -:    5:
    #####:    6:int soustraction(int a, int b)
    -:    7:{
    #####:    8:	return a - b;
    -:    9:}
    -:   10:
    1:   11:int test()
    -:   12:{
    1:   13:	bool a = true;
    1:   14:	int j = 0;
    -:   15:
    1:   16:	if(a)
    -:   17:	{
    1:   18:		j = 5;
    -:   19:	}
    -:   20:	else
    -:   21:	{
    #####:   22:		j = 3;
    -:   23:	}
    -:   24:
    1:   25:	return j;
    -:   26:}
    -:   27:
    1:   28:int main(int, char**)
    -:   29:{
    1:   30:    test();
    1:   31:	addition(5, 3);
    -:   32:
    1:   33:	return 0;
    -:   34:}
    ```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Detailed report
    - [x] Annotated source code

???+ danger "Cons"
    - [ ] GCC dependant

## Extra
_________________

### Lcov

**[Lcov](https://github.com/linux-test-project/lcov)** is a graphical front-end for **Gcov**.

It collects **Gcov** data for multiple source files and creates `HTML` pages containing the source code annotated with coverage information.

Additionally, it provides overview pages for easy navigation within the file structure.

`Lcov` supports statement, function, and branch coverage measurement.

To install `Lcov`, use:

``` bash
sudo apt install lcov
```

The first step is to generate a file `report.info` that contains information about all `.gcda` file.

```bash
lcov --directory . --capture --output-file report.info
```

The next step is to create the `HTML` report based on the file `report.info`.

```bash
genhtml --output-directory  ../report report.info
```

![html generated](../ressources/img/gcov_html.png)

### gcovr

**[gcovr](https://github.com/gcovr/gcovr)** provides a utility for managing the use of the **Gcov** utility and generating summarized code coverage results.

The `gcovr` command can produce different kinds of coverage reports including:

* `--txt`
* `--html`
* `--csv`
* `--json`
* `--lcov`

To install `gcovr`, use:

``` bash
sudo apt install gcovr
```

Then, the `gcovr` utility can be run as follows:

``` bash
gcovr --html-details coverage.html
```

![gcovr html generated](../ressources/img/gcovr_html.png)