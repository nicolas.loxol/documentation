---
hide:
  - navigation
---

# **GDB** :star::star::star::star:
_________________

**[The GNU Debugger (GDB)](https://www.sourceware.org/gdb/)** is a portable debugger that runs on many `Unix-like` systems and supports multiple programming languages, including `C/C++`.

**GDB** allows users to inspect the state of a program while it executes or examine its behavior at the moment of a crash.

**GDB** can perform four main actions to help identify and debug bugs:

* Start the program, specifying any conditions that might affect its behavior.
* Set breakpoints to make the program stop on specified conditions.
* Examine the program's state when it has stopped.
* Modify the program to correct one bug and explore another through experimentation.

## Installation
_________________

**GDB** can be installed using the following command:

``` bash
sudo apt install gdb
```

???+ info
    This tutorial will cover **GDB** version 14.2

## How to use
_________________

### Compilation for debugging

When using the `GCC` compiler, the code must be compiled with the `-g` flag to include appropriate `debug information` in the generated binary, making it possible to inspect the program using **GDB**.

``` bash
g++ -g main.cpp -o main
```

### Execute the program

The **GDB** utility can launch the binary using:

``` bash
gdb main
```

The program can then be executed using the `run` command:

=== "Classic"

    ``` bash
    (gdb) run
    ```

=== "With args"

    ``` bash
    (gdb) run -arg1 -arg2 ... -argn
    ```

???+ warning
    `(gdb)` is the prompt of the **GDB** executable

Execution proceeds as if the program were running normally.

The debugging session can be ended with the `quit` command.

``` bash
(gdb) quit
```

### Set up breakpoints

The `breakpoint` command is used to set `breakpoints`.

A `line number` in the source code or the `name of a function` can be specified.

The `run` command interrupts the execution of the program at each encountered `breakpoint`.

???+ info
    If the desired `function` is located in a `namespace`, it is accessible from `namespace::myfunction`

=== "Line number"

    ``` bash
    (gdb) break main.cpp:5
    ```

=== "Function name"

    ``` bash
    (gdb) break myfunction
    ```

To display the list of `breakpoints`, use `info breakpoints`.

Each `breakpoint` is identified by a number.

``` bash
(gdb) info breakpoints
```

Breakpoint can be deleted using its identification number.

``` bash
(gdb) delete 1
```

???+ info
    `watchpoints` can be introduced using the command `watch`.

    To create a watchpoint, the program must be running.

    The command interrupt the program execution when the value of a variable is modified.

    ```bash
    (gdb) watch myvar
    ```

### Examine the code

When the execution is interrupted, the program's memory can be examined.

For example, by displaying the value of a variable using the `print` command.

=== "Classic"

    ``` bash
    (gdb) print myvar
    ```

=== "Hexadecimal"

    ``` bash
    (gdb) print/h myvar
    ```

=== "Array"

    ``` bash
    (gdb) print mytab@15
    ```

The `list` command can be used to display the context lines in the code where the execution was interrupted.

``` bash
(gdb) list
```

### Move through the code

The `step` command moves forward step by step in the execution to control the program's evolution.

It moves to the next line in the source.

``` bash
(gdb) step
```

The `next` command is similar to the `step` command but provides less detail.

It does not enter function calls within the current scope block.

``` bash
(gdb) next
```

The `continue` command resumes execution until the next `breakpoint`.

``` bash
(gdb) continue
```

The `finish` command executes instructions until the end of the program.

``` bash
(gdb) finish
```

### Examine the call stack

The `backtrace` command displays the execution stack.

``` bash
(gdb) backtrace
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Easy to use
    - [x] Can be use though graphical interface
???+ danger "Cons"
    - [ ] No really friendly compared to an integrated IDE debugger

## Extra
_________________

### DWARF

**[Debugging With Attributed Record Formats (DWARF)](https://dwarfstd.org/)** is a debugging information format used by many compilers, debuggers, and other tools to facilitate debugging of programs.

`DWARF` provides a standardized way to encode debugging information, such as variable names, types, and source code locations, into object files or executables during the compilation process.

`-gsplit-dwarf` is a compiler option used with the `GNU Compiler Collection (GCC)` and related compilers like `Clang`.

It is primarily used to split the debugging information generated during compilation into a separate file.

Normally, when a program is compiled with debugging symbols enabled (`-g` flag), the debugging information is included in the executable or object file itself.

However, with `-gsplit-dwarf`, this debugging information is extracted and placed in a separate file.

By placing debugging information in a separate file, the size of the executable or object file is reduced.

This can be significant, especially for large projects with extensive debugging symbols, resulting in smaller binaries.

Separating debugging information can also improve compilation and linking times, particularly for incremental builds.

When only the code changes, there is no need to regenerate the separate debugging file, leading to faster build times.

Despite the debugging information being in a separate file, tools like debuggers can still easily access and use this information for debugging purposes.

This separation does not impact the debugging experience for developers.

Here is a basic example demonstrating the efficiency of the `-gsplit-dwarf` option:

``` title="main.cpp" linenums="1"
#include <iostream>

int main()
{
    int a = 0;

    a += 5;

    std::cout << a << std::endl;

    return 0;
}
```

``` bash
g++ main.cpp -o main -gsplit-dwarf -g
g++ main.cpp -o main1 -g

du -h main*

28K	main
36K	main1
52K	main.dwo
```

### DDD

**[GNU DDD](https://www.gnu.org/software/ddd/)** is a graphical front end for the command-line `debugger GDB`.

Besides usual frontend features such as viewing source texts, `DDD` has become famous through its interactive graphical data display, where data structures are displayed as graphs.

`DDD` can be installed using the following command:

``` bash
sudo apt install ddd
```

To debug the executable use:

``` bash
ddd main
```