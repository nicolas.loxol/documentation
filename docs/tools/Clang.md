---
hide:
  - navigation
---

# **Clang** :star::star::star::star:
_________________

**[Clang](https://clang.llvm.org/)** is a `compiler` for the `C/C++` programming languages.

It is part of the `LLVM` project, which is a collection of modular and reusable `compiler` and `toolchain` technologies.

**Clang** itself is responsible for the `parsing`, `syntax checking`, and other early stages of the `compilation` process.

Here are some key features and aspects of **Clang**:

* *Efficiency*: **Clang** is known for its `speed` and `memory efficiency`.

* *Modularity*: **Clang** is designed as a set of `reusable libraries and components`.

* *Diagnostics*: **Clang** is well-regarded for its `clear and informative error and warning messages`.

* *Standards Compliance*: **Clang** is committed to adhering to `C and C++ standards`.

* *Cross-Platform*: **Clang** is available on `multiple platforms`, including `Linux`, `macOS`, `Windows`, and more.

* *IDE Integration*: Many Integrated Development Environments, such as `Xcode`, `Visual Studio Code`, and others, use **Clang** as their underlying `compiler`.

* *Open Source*: **Clang** is an open-source project, which means that its source code is freely available for inspection and modification.

## Installation
_________________

To install **Clang** and its dependencies, follow these instructions:

=== "Windows"

    Go to the last tag on the official [github page](https://github.com/llvm/llvm-project/releases/) and download `LLVM-X.Y.Z-win64.exe`

=== "Unix-like system"

    ???+ warning
        The `<version number>` should be replace with the version of **Clang** the user want to install

    ``` bash
    wget https://apt.llvm.org/llvm.sh
    chmod +x llvm.sh
    sudo ./llvm.sh <version number>

    sudo apt install clang-<version number> clang-tools-<version number> clang-<version number>-doc libclang-common-<version number>-dev libclang-<version number>-dev libclang1-<version number> clang-format-<version number> python3-clang-<version number> clangd-<version number> clang-tidy-<version number>
    ```

???+ info
    This tutorial will cover `clang 18`

## How to use
_________________

### Command line

**Clang** acts as a drop-in replacement for the `GNU Compiler Collection (GCC)`, supporting most of its `compilation flags` and `unofficial language extensions`.

=== "C"

    Here is an example of the **Clang** command line utility usage for a `C` program:

    ``` c title="main.cpp" linenums="1"
    #include <stdio>

    int main()
    {
        printf("Hello world");

        return 0;
    }
    ```

    ``` bash
    clang main.c -o main -std=c++11 -g -O2 -Wall -Wextra
    ```

=== "C++"

    Here is an example of `clang++` command line utility usage for a `C++` program:

    ``` cpp title="main.cpp" linenums="1"
    #include <iostream>

    int main(int, char**)
    {
        std::cout << "Hello World" << std::endl;

        return 0;
    }
    ```

    ``` bash
    clang++ main.cpp -o main -g -O2 -Wall -Wextra
    ```

The full option summary list can be found on the [official documentation page](https://clang.llvm.org/docs/ClangCommandLineReference.html).

### Visual Studio

`Clang/LLVM` can be used withing `Visual Studio` to edit, build, and debug `C/C++ Visual Studio projects`.

Clang compiler tools can be installed by opening the `Visual Studio Installer` and choosing `C++ Clang compiler for Windows` or `MSBuild support for LLVM` (`MSBuild` usage) on the `Compilers, build tools, and runtimes` of the `Individual components` menu.

![Install](../ressources/img/clang_install_visual.png)

To configure a `Visual Studio project` to use **Clang**, ++right-button++ on the project node in `Solution Explorer` and choose `Properties`.

Then, under `General` > `Platform Toolset`, choose `LLVM (clang-cl)`.

![Install](../ressources/img/clang_visual_build.png)

### Visual Studio Code

`Clang/LLVM` can be used withing `Visual Studio Code` to edit, build, and debug `C/C++ Visual Studio Code projects`.

Select the `Terminal > Run Build Task` command (++ctrl+shift+b++) from the main menu.

![Visual Studio Code C++ compiler clang](../ressources/img/visual_studio_code_compiler_clang.png)

This will display a dropdown with various compiler found on the machine.

Pick the `clang/clang++` on the list.

## Pros and cons
_________________

???+ success "Pros"
    - [x] Cross-platform
    - [x] Standards Compliance
    - [x] Bunch of tools

???+ danger "Cons"
    - [ ] Documentation might be unclear

## Extra
_________________

### LLVM

**[LLVM](https://llvm.org/)**, which stands for `Low-Level Virtual Machine` is a collection of modular and reusable `compiler` and `toolchain` technologies.

It is designed to provide a solid foundation for building various programming language `compilers` and related development tools.

* `Intermediate Representation (IR)`: `LLVM` introduces a powerful and flexible intermediate representation called `LLVM IR`.

It is a low-level, platform-independent representation of code that can be used as a common language for optimization and code generation.

Compilers targeting `LLVM` often translate their source code into `LLVM IR` before further processing.

![LLVM](../ressources/img/llvm.jpg)

### Clang Tools

`Clang Tools` are standalone command line (and potentially GUI) tools designed for use by `C/C++` developers.

These tools provide functionalities such as `fast syntax checking`, `automatic formatting`, `refactoring`, etc.

#### ClangCheck

**[ClangCheck](https://clang.llvm.org/docs/ClangCheck.html)** is a the official `Clang static analyzer`.

A `static analyser` is a software tool used in computer programming and software development to analyze source code without executing it.

It is designed to find potential issues, errors, vulnerabilities, and other code quality issues in a program by examining the code's structure, syntax, and semantics.

ClangCheck also allows developers to see the generated `AST` of his code.

The `Abstract Syntax Tree (AST)` is a data structure used in `compilers` to represent the hierarchical structure of the source code.

The `AST` breaks down the code into a tree-like structure where each node in the tree corresponds to a syntactic element in the code, such as a variable declaration, function call, or control flow statement.

These nodes are organized in a way that reflects the nested structure of the code, making it easier for the `compiler` to analyze and manipulate the code during the compilation process.

Running of `static analysis` can be done using:

``` bash
clang-check main.cpp --analyze --
```

With `AST`:

``` bash
clang-check main.cpp --ast-dump --
```

Generated `compile_commands` usage:

``` bash
clang-check compile_commands.json main.cpp --
```

???+ info
    To generate a compilation database using `CMake`, the variable `-DCMAKE_EXPORT_COMPILE_COMMANDS` must be configured.