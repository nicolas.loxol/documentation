---
hide:
  - navigation
---

# **MSBuild** :star::star::star:
_________________

**[`Microsoft Build Engine (MSBuild)`](https://learn.microsoft.com/en-us/visualstudio/msbuild/msbuild?view=vs-2022)** is a platform for building applications.

This engine provides an `XML schema` for a project file that controls how the build platform processes and builds software.

`Visual Studio` uses **MSBuild**, but **MSBuild** does not depend on `Visual Studio`.

Building products in environments where `Visual Studio` is not installed can be accomplished by invoking `msbuild.exe` on a `(.vcxproj)` file.

## Installation
_________________

**MSBuild** can be installed using:

=== "Visual studio"

    If `Visual Studio` is installed, then **MSBuild** should be installed.

    With `Visual Studio 2022`, it is installed under the `Visual Studio` installation folder.

    For a typical default installation, `MSBuild.exe` is under the installation folder in `MSBuild\Current\Bin`.

=== "Visual studio installer"

    In the `Visual Studio installer`, navigate to `Individual Components`, and locate the checkbox for **MSBuild**.

=== "MSBuild"

    To install **MSBuild** on a system that does not have `Visual Studio`, go to `Build Tools for Visual Studio 2022` on the [downloads](https://visualstudio.microsoft.com/downloads/) page.


???+ info
    This tutorial will cover `Visual studio 2022` and **MSBuild** version 17.9.8

## How to use
_________________

### Use MSBuild in Visual Studio

`Visual Studio` uses **MSBuild** to load and build managed projects.

The project files in `Visual Studio` (`.vcxproj`) contain `MSBuild XML code` that executes when building a project.

`Visual Studio` projects import all the necessary settings and build processes to do typical development work, but it can be extended within `Visual Studio` or by using an `XML` editor.

Here is an example of the `MSBuild XML code`:

``` xml title="main.vcxproj" linenums="1"
<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup Label="ProjectConfigurations">
    <ProjectConfiguration Include="Debug|Win32">
      <Configuration>Debug</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Release|Win32">
      <Configuration>Release</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Debug|x64">
      <Configuration>Debug</Configuration>
      <Platform>x64</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Release|x64">
      <Configuration>Release</Configuration>
      <Platform>x64</Platform>
    </ProjectConfiguration>
  </ItemGroup>
  <PropertyGroup Label="Globals">
    <VCProjectVersion>16.0</VCProjectVersion>
    <Keyword>Win32Proj</Keyword>
    <ProjectGuid>{8d28644e-8354-4d47-a3b5-9fd9f8ea9453}</ProjectGuid>
    <RootNamespace>main</RootNamespace>
    <WindowsTargetPlatformVersion>10.0</WindowsTargetPlatformVersion>
  </PropertyGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.Default.props" />
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>true</UseDebugLibraries>
    <PlatformToolset>v143</PlatformToolset>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>false</UseDebugLibraries>
    <PlatformToolset>v143</PlatformToolset>
    <WholeProgramOptimization>true</WholeProgramOptimization>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|x64'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>true</UseDebugLibraries>
    <PlatformToolset>v143</PlatformToolset>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|x64'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>false</UseDebugLibraries>
    <PlatformToolset>v143</PlatformToolset>
    <WholeProgramOptimization>true</WholeProgramOptimization>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.props" />
  <ImportGroup Label="ExtensionSettings">
  </ImportGroup>
  <ImportGroup Label="Shared">
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Debug|x64'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Release|x64'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <PropertyGroup Label="UserMacros" />
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <SDLCheck>true</SDLCheck>
      <PreprocessorDefinitions>WIN32;_DEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <ConformanceMode>true</ConformanceMode>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <FunctionLevelLinking>true</FunctionLevelLinking>
      <IntrinsicFunctions>true</IntrinsicFunctions>
      <SDLCheck>true</SDLCheck>
      <PreprocessorDefinitions>WIN32;NDEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <ConformanceMode>true</ConformanceMode>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>
      <OptimizeReferences>true</OptimizeReferences>
      <GenerateDebugInformation>true</GenerateDebugInformation>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Debug|x64'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <SDLCheck>true</SDLCheck>
      <PreprocessorDefinitions>_DEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <ConformanceMode>true</ConformanceMode>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Release|x64'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <FunctionLevelLinking>true</FunctionLevelLinking>
      <IntrinsicFunctions>true</IntrinsicFunctions>
      <SDLCheck>true</SDLCheck>
      <PreprocessorDefinitions>NDEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <ConformanceMode>true</ConformanceMode>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>
      <OptimizeReferences>true</OptimizeReferences>
      <GenerateDebugInformation>true</GenerateDebugInformation>
    </Link>
  </ItemDefinitionGroup>
  <ItemGroup>
    <ClCompile Include="main.cpp" />
  </ItemGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.targets" />
  <ImportGroup Label="ExtensionTargets">
  </ImportGroup>
</Project>
```

### Use MSBuild command-line

In general, it is recommended to use `Visual Studio` to set `project properties` and invoke the **MSBuild** system.

However, the **MSBuild** tool can be directly utilized from the `command prompt`.

The build process is controlled by the information in a project file (`.vcxproj`).

The project file specifies `build options` based on `build stages`, `conditions`, and `events`.

In addition, zero or more command-line options arguments can be specified.

``` bash
MSBuild [ project_file ] [ options ]
```

* `[ project_file ]` : The .vcxproj file
* `[ options ]` : The compile options

Here are some basics examples:

=== "Property"

    Compilation of the solution `MyProj.sln` using the configuration `Debug x64`

    ``` bash
    MSBuild MyProj.sln /p:Configuration=Debug /p:Platform=x64
    ```

=== "Target"

    Compilation of the target `rebuild` of the solution `MyProj.sln`

    ``` bash
    MSBuild MyProject.sln /t:rebuild
    ```

=== "Property and target"

    Compilation of the targets `PrepareResources` and `Compile` of the solution `MyProj.sln` with warning flags and a specific output directory for binaries.

    ``` bash
    MSBuild MyProj.sln /p:WarningLevel=2;OutDir=bin/Debug /t:PrepareResources;Compile
    ```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Integrated on Visual studio IDE
    - [x] Easy to use

???+ danger "Cons"
    - [ ] Though to edit manually the `.vcxproj` file
    - [ ] Lack of documentation

## Extra
_________________

### Microsoft Visual C++

[MSVC](https://learn.microsoft.com/en-us/cpp/build/building-on-the-command-line?view=msvc-170) refers to the `Microsoft Visual C++ compiler`, which is a component of the `Microsoft Visual Studio development environment`.

It is responsible for `compiling` `C` and `C++` code into `executable` or `libraries`.

`MSVC` supports various versions of the `C++` language standard and provides a rich set of optimization options, debugging tools, and libraries.

To build `C/C++` code using a specific `Visual Studio`, it is needed to specify a specific version of `MSVC build tools`:

=== "Visual Studio 2015"

    ``` txt
    MSVC v140 build tools
    ```

=== "Visual Studio 2017"

    ``` txt
    MSVC v141 build tools
    ```

=== "Visual Studio 2019"

    ``` txt
    MSVC v142 build tools
    ```

=== "Visual Studio 2022"

    ``` txt
    MSVC v143 build tools
    ```

???+ info
    It is possible to compile a project using the Visual Studio 2015 compiler on a Visual Studio 2022 installation.

    These compilers are called `MSVC toolsets`.

    They are are downloadable as standalone components on the Visual Studio Installer or using the [official website](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2022).

To work correctly, `MSVC` require several `specific environment variables` to be set.

These variables are used to add the `tools` to the path, and to set the locations of `include` files, `library` files, and `SDKs`.

The simplest way to set these `environment variable` is to build the environment using `vcvarsall.bat`

Execute `vcvarsall.bat` to set environment variables to configure the command line for native `32-bit` or `64-bit` compilation.

The user can specified cross-compilation arguments to `x86`, `x64`, `ARM`, or `ARM64` processors.

It is also possible to specify the `platform toolset version` to use.

When used with no arguments, `vcvarsall.bat` configures the environment variables to use the current `x86-native` compiler for `32-bit Windows Desktop targets`.

`vcvarsall.bat` displays an error message if the specified configuration is not installed, or not available.

Here are some examples:

=== "Architecture x86"

    ``` bash
    vcvarsall.bat x86
    ```

=== "Architecture x64"

    ``` bash
    vcvarsall.bat x64
    ```

=== "Architecture x64 and Visual Studio 2019 build tools"

    ``` bash
    vcvarsall.bat x64 -vcvars_ver=14.2
    ```

In summary, **MSBuild** is a `build automation tool` used to define and manage the build process for a variety of project types, while `MSVC` is a `C++ compiler` provided as part of `Microsoft Visual Studio`, specifically for compiling `C` and `C++` code.

**MSBuild** can utilize `MSVC` as the underlying `compiler` for `C++` projects when building them.

``` bash
vcvarsall.bat x64 -vcvars_ver=14.2
MSBuild MyProj.sln /p:Configuration=Debug /p:Platform=x64
```