---
hide:
  - navigation
---

# **Gprof** :star::star::star:
_________________

**[Gprof](http://sourceware.org/binutils/docs/gprof/)** is a `performance analysis tool` for `Unix applications`.

It used a hybrid of `instrumentation` and `sampling` and was created as an extended version of the older `prof` tool.

Unlike `prof`, **Gprof** is capable of limited call graph collecting and printing.

## Installation
_________________

**Gprof** can be installed using the following command.

``` bash
sudo apt install binutils
```

???+ info
    This tutorial will cover **Gprof** version 2.38

## How to use
_________________

### Compiling and link the program with profiling

The first step in generating profile information for a program is to `compile` and `link` it with `profiling` enabled.

The `-pg` option must be used when compiling.

Here are examples:

=== "C"

    ``` bash
    gcc -pg main.c -o main
    ```

=== "C++"

    ``` bash
    g++ -pg main.cpp -o main
    ```

### Execute the program

Once the program is `compiled` for `profiling`, it must run in order to generate the information that **Gprof** needs.

Simply run the program as usual, using the normal arguments, file names, etc.

``` bash
./main
```

The program should run normally, producing the same output as usual.

It will, however, run somewhat slower than normal because of the time spent collecting and writing the profile data.

The program will write the profile data into a file called `gmon.out` just before exiting.

### Run Gprof

Once the `gmon.out` file is generated, run **Gprof** to interpret the information in it.

The **gprof** program prints a flat profile and a call graph on standard output.

Typically users would redirect the output of **gprof** into a file with `>`.

It is time to run the **gprof** utility:

``` bash
gprof main gmon.out > profile-data.txt
```

```txt title="profile-data.txt" linenums="1"
Flat profile:

Each sample counts as 0.01 seconds.
%    cumulative self           self    total
time seconds    seconds calls  ms/call ms/call  name
96.43 0.81      0.81      1    810.00  810.00   func3
3.57 0.84       0.03      1    30.00   840.00   func1
0.00 0.84       0.00      1    0.00    810.00   func2
0.00 0.84       0.00      1    0.00    0.00     func4

Call graph (explanation follows)

granularity: each sample hit covers 4 byte(s) for 1.19% of 0.84 seconds

index % time self children called name
             0.03 0.81     1/1       main [2]
[1]    100.0 0.03 0.81      1     func1 [1]
             0.00 0.81     1/1       func2 [3]
-----------------------------------------------
                                     <spontaneous>
[2]    100.0 0.00 0.84            main [2]
             0.03 0.81     1/1       func1 [1]
             0.00 0.00     1/1       func4 [5]
-----------------------------------------------
             0.00 0.81     1/1       func1 [1]
[3]    96.4  0.00 0.81      1     func2 [3]
             0.81 0.00     1/1       func3 [4]
-----------------------------------------------
             0.81 0.00     1/1       func2 [3]
[4]    96.4  0.81 0.00      1      func3 [4]
-----------------------------------------------
             0.00 0.00     1/1       main [2]
[5]    0.0   0.00 0.00      1      func4 [5]
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Setup is straightforward
    - [x] Detailed reports are generated
    - [x] Graphs can be generated with gprof2dot
???+ danger "Cons"
    - [ ] Not multi-platform
    - [ ] More advanced profilers available

## Extra
_________________

### gprof2dot

**[gprof2dot](https://github.com/jrfonseca/gprof2dot)** is a `Python` script to convert the output from many profilers into a `dot graph`.

#### Installation

**[python](https://www.python.org/)** and **[graphviz](https://graphviz.org/)** are needed to run the `gprof2dot` executable.

```bash
sudo apt install python3 graphviz
```

Use `pip` to install `gprof2dot`.

```bash
pip install gprof2dot
```

#### Usage

```bash
gprof main | gprof2dot | dot -Tpng -o graph.png
```

Here is an example of output:

![gprof2dot output](../ressources/img/gprof2dot.svg)