---
hide:
  - navigation
---

# **Visual Studio** :star::star::star::star:
_________________

**[Visual Studio](https://visualstudio.microsoft.com/)** is an `integrated development environment (IDE)` developed by `Microsoft`.

It is utilized for developing various types of computer programs, including websites, web apps, web services, and mobile apps.

**Visual Studio** support a variety of programming languages, including `C` and `C++`.

## Installation
_________________

**Visual Studio** is an exclusive `Windows IDE`.

It can be installed by downloading the official [installer](https://visualstudio.microsoft.com/downloads/).

Upon launching the executable, the `Visual Studio Installer` will be installed, allowing the user to manage the different versions of **Visual Studio**.

![Visual studio installer](../ressources/img/visual_studio_installer.png)

???+ info
    This tutorial will cover `Visual Studio 2022 Community edition` version 17.9.6

## How to use
_________________

Here are the different functionalities of **Visual Studio** for `C/C++` development:

### Develop C and C++ applications

The `C++ desktop development package` can be installed using the `Visual Studio Installer`, allowing modern `C` and `C++` apps for `Windows` to be built using various tools, including `MSVC`, `Clang`, `CMake`, and `MSBuild`.

![C++ package](../ressources/img/visual_studio_installer_c++_package.png)

Among other features, this package contains:

* `MSVC v143 x64/x86`
* `C/C++ profiling tools`
* `C++ CMake tools for windows`
* `IntelliCode`
* `vcpkg packet manager`
* `Windows 11 SDK`

Once installed, this package enables the development of `C` and `C++` applications.

### Property page

Project properties can be modified by ++right-button++ on the `project` and then select `Properties`.

![Properties](../ressources/img/visual_studio_properties.png)

![Properties2](../ressources/img/visual_studio_properties2.png)

Below are the most useful properties of a `C/C++` project:

* `General`
    * `Output Directory` : The path where the target is generated
    * `Target Name` : The target name
    * `Configuration Type` : The target type (exe, dll, lib, ...)
    * `Windows SDK Version` : The Windows SDK version
    * `Platform Toolset` : The build tools used to generate the target
    * `C++ Language Standard` : The C++ standard
    * `C Language Standard` : The C standard
* `Debugging`
    * `Command Arguments` : The argument of the entry point
* `C/C++`
    * `General`
        * `Additional Include Directories` : The additional includes directories
    * `Command Line` : The command line used during the compilation phase
* `Linker`
    * `General`
        * `Additional Library Directories` : The additional libraries directories
    * `Command Line` : The command line used during the linking phase
* `Build event`
    * Pre-Build Event : The command executes on pre-build
    * Post-Build Event : The command executes on post-build

### IntelliSense

`IntelliSense`, a code-completion feature, provides context-aware suggestions and information as code is written.

It aids in faster coding and reduces errors by offering autocompletion for code elements such as classes, methods, and properties, along with relevant details like parameter lists and method signatures.

Additionally, `IntelliSense` offers tooltips, quick info, and navigation options to facilitate exploration and understanding of APIs.

`IntelliCode`, integrated into the `C/C++` package, is an AI-assisted feature that enhances `IntelliSense` by providing intelligent code completion suggestions based on learned patterns and practices from numerous open-source projects.

It utilizes machine learning algorithms to analyze code patterns and predict the most probable next code element the user may use.

`IntelliCode` learns from prevalent coding practices and can offer more precise and relevant suggestions compared to regular IntelliSense.

It aids in faster code writing by suggesting the most appropriate code completions based on the code's context.

### Visual studio debugger

Debugging is the process of identifying and resolving errors, exceptions, and unexpected behaviors in software code.

It is an essential step in the software development life cycle, ensuring code correctness and robustness.

Debugging helps developers save time and resources by detecting and fixing issues early in the development process.

The `Visual Studio Debugger` is an integrated debugging tool provided by `Microsoft's Visual Studio IDE`.

It allows developers to inspect, monitor, and manipulate the execution of their code during runtime.

The `debugger` offers a wide range of features, making the debugging process more efficient and effective.

#### Main features

To debug, the app needs to be started with the debugger attached to the app process.

This can be done by pressing ++f5++ (`Debug > Start Debugging`)

However, if there are no breakpoints set to examine the app code, setting one is necessary before starting debugging.

##### Set a breakpoint and start the debugger

`Breakpoints` are the most basic and essential feature of reliable debugging.

Breakpoints indicate where **Visual Studio** should suspend the running code so developers can examine variable values, memory behavior, or code execution branches.

A `breakpoint` indicates where **Visual Studio** should suspend the running code so the user can take a look at the values of variables, or the behavior of memory, or whether or not a branch of code is getting run.

![Breakpoint](../ressources/img/visual_studio_breakpoint.png)

???+ info
    Control over when and where a breakpoint executes can be achieved by setting conditions.

    The condition can be any valid expression that the debugger recognizes

    To set a `breakpoint condition`:

    1. ++right-button++ the `breakpoint symbol` and select `Conditions` (or press ++alt+f9++, ++c++)

    ![Conditional Breakpoint](../ressources/img/visual_studio_conditional_breakpoint.png)

    1. In the dropdown, select `Conditional Expression`, `Hit Count`, or `Filter`, and set the value accordingly

    1. Select `Close` to close the `Breakpoint Settings window`

    `Breakpoints` with `conditions` set appear with a `+` symbol in the source code and `Breakpoints windows`

##### Navigate code in the debugger using step commands

Navigation on a line of code that is a function or method call, can be done using ++f10++ (`Debug > Step Over`) or ++f11++ (`Debug > Step Into`).

++f10++ advances the debugger without stepping into functions or methods in the app code (the code still executes).

++f11++ advances the debugger stepping into functions or methods in the app code.

++shift+f11++ (or Debug > Step Out) resumes app execution (and advances the debugger) until the current function returns.

The line of code currently being debugged is indicated by the cursor, symbolized by a yellow arrow on the left margin.

![Cursor](../ressources/img/visual_studio_cursor.png)

##### Inspect variables with data tips

Now that breakpoints have been placed, variable inspection can be initiated with the debugger.

Features facilitating variable inspection are among the most useful features of the debugger, and there are various ways to do it.

Often, when debugging an issue, the goal is to determine whether variables store the expected values in a particular app state.

While paused in the debugger, an object's value or its default property value is displayed by hovering over it with the mouse.

![Property value](../ressources/img/visual_studio_property_value.png)

If the variable has properties, the object can be expanded to view all its properties.

##### Inspect variables with the Autos and Locals windows

In the `Autos` window, variables are displayed along with their current value and their type. In `C++`, the window shows variables in the preceding three lines of code.

![Auto window](../ressources/img/visual_studio_auto_window.png)

Next, look at the `Locals window`.

The `Locals` window displays the variables that are currently in scope.

![Local Window](../ressources/img/visual_studio_local_window.png)

##### Set a watch

A `Watch` window can be utilized to specify a variable or an expression that is monitored throughout the program execution.

While debugging, ++right-button++ a variable or an object and choose `Add Watch`.

Once the watch is set on the object, the Watch window can be used to check the variable value.

![Watch Window](../ressources/img/visual_studio_watch_window.png)

##### Examine the call stack

The `Call Stack window` shows the order in which methods and functions are getting called.

The top line shows the current function.

The second line shows the function or property it was called from, and so on.

The `call stack` is a good way to examine and understand the execution flow of an app.

Click the `Call Stack window` while debugging, which is by default open in the lower right pane.

![Call Stack Window](../ressources/img/visual_studio_callstack_window.png)

Left-clicking a line of code navigate to that source code, changing the current scope being inspected by the debugger.

This action does not advance the debugger.

##### Inspect an exception

When an app throws an exception, the debugger directs to the line of code that threw the exception and provides additional information, including the type of exception and an error message.

![Exception](../ressources/img/visual_studio_exception.png)

#### Live code editing

`Hot Reload` is a feature in `Visual Studio 2022` that allows developers to make code changes while an application is running and see those changes immediately applied without the need to stop and restart the application.

???+ info
    For the project to support Hot Reload, the following requirements need to be met:

    1. Open the `Property Pages` dialog box for the project

    1. Click the `C/C++` node. Set `Debug Information Format `Program Database (/Zi)`

    1. Expand `Linker` and click the `General` node. Set `Enable Incremental Linking` to `Yes (/INCREMENTAL:YES)`

It provides a quicker feedback loop and speeds up the development process by reducing the need for frequent restarts.

While debugging the application, hot reload can be utilized using ++alt+f10++.

### Visual studio profiler

#### Measure memory usage in Visual Studio

The `Memory Usage tool` can collect `memory snapshots` at any time in the `Memory Usage tool`.

Actions like setting breakpoints, stepping, and others can help focus performance investigations on relevant code paths.

Performing these actions while the app is running can filter out irrelevant code and substantially reduce diagnosis time.

Memory usage measurement will be conducted on a basic `C++` program.

![Memory CPP](../ressources/img/visual_studio_memory_cpp.png)

Three breakpoints have been placed:

* Before allocation
* After allocation
* After deallocation

Memory usage will be investigated at each of these steps.

First open the `Diagnostic Tools window` with ++ctrl+alt+f2++ (`Debug > Windows > Show Diagnostic Tools`).

![Diagnostic tool](../ressources/img/visual_studio_diagnostic_tool.png)

Choose `Memory Usage` with the `Select Tools` setting on the toolbar.

![Memory usage](../ressources/img/visual_studio_diag_tool.png)

Start debugging ++f5++.

On the `Diagnostic Tools window`, select the `Memory Usage tab`, and then choose `Heap Profiling`.

![Heap profiling](../ressources/img/visual_studio_heap_profiling.png)

On each `breakpoint`, take `snapshot` on the Memory Usage

![Snapshot](../ressources/img/visual_studio_snapshot.png)

The memory can be inspected using the different `snapshots`.

![Snapshot list](../ressources/img/visual_studio_snapshot_list.png)

Click on a particular snapshot to inspect the memory

![Snapshot information](../ressources/img/visual_studio_snapshot_information.png)

#### Measure application performance by analyzing CPU usage

When the `debugger` pauses, the `CPU Usage tool` in the `Diagnostic Tools window` collects information about the functions executing in the application.

It lists the functions performing work and provides a timeline graph for focusing on specific segments of the sampling session.

First open the `Diagnostic Tools window` with ++ctrl+alt+f2++ (`Debug > Windows > Show Diagnostic Tools`).

![Diagnostic tool](../ressources/img/visual_studio_diagnostic_tool.png)

Choose `CPU Usage` with the `Select Tools` setting on the toolbar.

![CPU usage](../ressources/img/visual_studio_diag_tool.png)

Start debugging ++f5++.

While the `debugger` is paused, enable the collection of the `CPU Usage data` and then open the `CPU Usage tab`.

When `Record CPU Profile` is chosen, **Visual Studio** begins recording the functions and the time they take to execute.

The collected data can only be viewed when the application is halted at a breakpoint.

![Record](../ressources/img/visual_studio_record.png)

Hit ++f5++ to run the app to the second `breakpoint`.

Now, performance data for the application is available specifically for the region of code that runs between the two breakpoints.

The profiler begins preparing thread data.

Wait for it to finish.

![Summary tab](../ressources/img/visual_studio_summary_tab.png)
![Graph](../ressources/img/visual_studio_graph.png)

It is recommended to begin analyzing the data by examining the list of functions under `CPU Usage`, identifying the functions that are doing the most work, and then taking a closer look at each one.

![Function list](../ressources/img/visual_studio_function.png)

Double-clicking a function, the `Functions view` opens in the left pane.

Select `Caller/Callee view` from the drop-down menu.

![Caller-Calee](../ressources/img/visual_studio_caller-callee.png)

This view displays the total time (in milliseconds) and the percentage of the overall application running time that the function has taken to complete.

The function Body also shows the total amount of time (and the percentage of time) spent in the function excluding time spent in calling and called functions.

???+ tip
    When trying to identify performance issues, take multiple measurements.

    Performance naturally varies from run-to-run, and code paths typically execute slower the first time they run due to one-time initialization work such as loading DLLs, and initializing caches.

    By taking multiple measurements, a better understanding of the range and median of the metric being shown is obtained. This allows for comparison between the initial time and the steady-state performance of an area of code.

### Integrated version control

With the integrated `Git` features, users can `clone`, `create`, or `open` a `repositories`.

The `Git` tool window has everything needed for `committing` and `pushing` changes to code, `managing branches`, and `resolving merge conflicts`.

`GitHub` account can be linked directly within **Visual Studio**.

With the integrated `Git` features, repositories can be cloned, created, or opened.

The `Git` tool window provides all necessary functions for committing and pushing changes to code, managing branches, and resolving merge conflicts.

For more information, these links can be referred to:

* [Git integration](https://learn.microsoft.com/en-us/visualstudio/version-control/?view=vs-2022)
* [Github integration](https://visualstudio.microsoft.com/vs/github/)

### Extension

Extensions are add-ons that enable customization and enhancement of the **Visual Studio** experience by adding new features or integrating existing tools.

Extensions can vary in complexity, but their primary aim is to boost productivity and accommodate workflow preferences.

Personalizing **Visual Studio** experience is possible today by downloading extensions.

Explore thousands of extensions available in the [Marketplace](https://marketplace.visualstudio.com/vs) to discover the various tools.

![Visual studio extension](../ressources/img/visual_studio_extension.png)

???+ info
    If the extension being sought does not exist, one can begin building their own extensions and publish them on the `Visual Studio Marketplace`.

    More information can be found at the provided [link](https://learn.microsoft.com/en-us/visualstudio/extensibility/starting-to-develop-visual-studio-extensions?view=vs-2022).

## Pros and cons
_________________

???+ success "Pros"
    - [x] Huge community
    - [x] Numerous updates
    - [x] Extended support for versions **Visual Studio** version

???+ danger "Cons"
    - [ ] Not available on Linux
    - [ ] Not adhering to the real standards of `C/C++`

## Extra
_________________

### Keyboard shortcuts

A variety of commands and windows in **Visual Studio** can be accessed by selecting the appropriate keyboard shortcut.

The shortcut for a command can be identified by opening the `Options` dialog box, expanding the `Environment` node, and then selecting `Keyboard`.

Customizing shortcuts is also possible by assigning a different shortcut to any given command.

#### Build: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`Build solution` | ++ctrl+shift+b++
`Compile` | ++ctrl+f7++
`Cancel` | ++ctrl+break++
`Run code analysis on solution` | ++alt+f11++

#### Debug: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`Start` | ++f5++
`Start without debugging` | ++ctrl+f5++
`Restart` | ++ctrl+shift+f5++
`Stop debugging` | ++shift+f5++
`Step over` | ++f10++
`Step into` | ++f11++
`Step out` | ++shift+f11++
`Toggle breakpoint` |	++f9++

#### Edit: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`Copy` | ++ctrl+c++
`Cut` | ++ctrl+x++
`Paste` | ++ctrl+v++
`Find` | ++ctrl+f++
`Find in files` | ++ctrl+shift+f++
`Replace` | ++ctrl+h++
`Select all` | ++ctrl+a++
`Comment selection` | ++ctrl+k++ ++ctrl+c++
`Uncomment selection` | ++ctrl+k++ ++ctrl+u++
`Complete word` | ++ctrl+space++
`Go to declaration` | ++ctrl+f12++
`Go to definition` | ++f12++
`Redo` | ++ctrl+y++
`Undo` | ++ctrl+z++
`Toggle bookmark` | ++ctrl+k++ ++ctrl+k++
`Next bookmark` | ++ctrl+k++ ++ctrl+n++
`Previous bookmark` | ++ctrl+k++ ++ctrl+p++

#### File: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`New file` | ++ctrl+n++
`New project` | ++ctrl+shift+n++
`Open file` | ++ctrl+o++
`Open project` | ++ctrl+shift+o++
`Save selected items` | ++ctrl+s++
`Save all` | ++ctrl+shift+s++
`Exit` | ++alt+f4++

???+ info
    A `PowerShell` inside **Visual Studio** can be opened with ++ctrl+"&ugrave;"++

### Useful plugin

Here is a list of useful **Visual Studio** plugins:

* [**SonarLint**](https://marketplace.visualstudio.com/items?itemName=SonarSource.SonarLintforVisualStudio2022) : A extension that identifies and helps the user to fix Code Quality and gain Code Security issues.

* [**Visual Assist**](https://marketplace.visualstudio.com/items?itemName=WholeTomatoSoftware.VisualAssist64) : A productivity extension for **Visual Studio** development work. It provides a set of intelligent refactoring, navigation, code highlighting and generation features for `C` and `C++` development.

* [**Visual Studio Spell Checker**](https://marketplace.visualstudio.com/items?itemName=EWoodruff.VisualStudioSpellCheckerVS2022andLater) : An editor extension that checks the spelling of comments, strings, and plain text interactively with a tool window. It can also spell check an entire solution, project, or selected items. Options are available to define multiple languages to spell check against.

* [**Qt Visual Studio Tools**](https://marketplace.visualstudio.com/items?itemName=TheQtCompany.QtVisualStudioTools2022) : Allow developers to use the standard development environment without having to worry about any Qt-related build steps or tools and `Qt` debugging classes.

* [**Clang power tools**](https://marketplace.visualstudio.com/items?itemName=caphyon.ClangPowerTools2022) : Clang Power Tools is a **Visual Studio** extension helping Visual Studio C++ developers leverage `Clang/LLVM` tools.

* [**Trailing whitespace visualizer**](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.TrailingWhitespaceVisualizer) : This extension will highlight and remove any trailing whitespace on any line in any editor in **Visual Studio**.

* [**VSDoxyHighlighter**](https://marketplace.visualstudio.com/items?itemName=Sedenion.VSDoxyHighlighter): Extension for **Visual Studio** to provide syntax highlighting, IntelliSense and quick infos for `Doxygen` style comments in C/C++.

### Code Snippets

`IntelliSense Code Snippets` are pre-authored pieces of code ready to be inserted into **Visual Studio**.

Productivity can be increased by providing code snippets that reduce the time spent typing repetitive code or searching for samples

The [**IntelliSense Code Snippet XML schema**](https://learn.microsoft.com/en-us/visualstudio/ide/code-snippets-schema-reference?view=vs-2022) can be used to create code snippets and add them to the snippets that **Visual Studio** already includes.

#### Create a Code Snippet

A code snippet can be created with only a few steps.

All that is required is to create an `XML` file, fill in the appropriate elements, and add the code to it.

The following `XML` serves as the basic snippet template.

``` xml title="example.snippet" linenums="1"
<?xml version="1.0" encoding="utf-8"?>
<CodeSnippets xmlns="http://schemas.microsoft.com/VisualStudio/2005/CodeSnippet">
  <CodeSnippet Format="1.0.0">
    <Header>
      <Title>The snippet title</Title>
      <Shortcut>The snippet shorcut</Shortcut>
      <Description>The snippet description</Description>
      <Author>The snippet author</Author>
    </Header>
    <Snippet>
      <Code Language="cpp">
        <![CDATA[]]>
      </Code>
    </Snippet>
  </CodeSnippet>
</CodeSnippets>
```

The snippet code can now be added in the `CDATA` section inside the `Code` element.

For example:

``` xml title="example.snippet"
<![CDATA[double root = std::sqrt(16);]]>
```

???+ warning
    There is no way to specify how lines of code in the `CDATA` section of a code snippet should be indented or formatted. Upon insertion, the `language service formats` the inserted code automatically.

#### Import a code snippet

1. A snippet can be imported to **Visual Studio** installation using the `Code Snippets Manager`. Open it with ++ctrl+k++, ++ctrl+b++ (`Tools > Code Snippets Manager`).

1. Select the `Import` button.

1. Navigate to the location where the code snippet was saved in the previous procedure, then select the snippet file, and choose `Open`.

1. The `Import Code Snippet dialog` opens, asking the user to choose where to add the `snippet` from the choices in the right pane. One of the choices should be `My Code Snippets`. Select it, select `Finish`, and then select `OK`.

1. The `snippet` is copied to one of the following locations: `%USERPROFILE%\Documents\Visual Studio 2022\Code Snippets\Visual C++\My Code Snippets`

1. Test the `snippet` by opening a `C++` project. With a code file open in the editor, type ++ctrl+k++, ++ctrl+b++ (`Snippets > Insert Snippet` from the right-click menu), then `My Code Snippets`. Double-click on the snippet.

#### Code snippet example

Below is an example of a code snippet creating a hello "name" application.

``` xml title="example.snippet" linenums="1"
<?xml version="1.0" encoding="utf-8"?>
<CodeSnippets xmlns="http://schemas.microsoft.com/VisualStudio/2005/CodeSnippet">
  <CodeSnippet Format="1.0.0">
    <Header>
      <Title>main Function</Title>
      <Shortcut>main</Shortcut>
      <Description>Creates a main function in C++</Description>
      <Author>Visual Studio User</Author>
      <SnippetTypes>
        <SnippetType>Expansion</SnippetType>
      </SnippetTypes>
    </Header>
    <Snippet>
      <Declarations>
        <Literal>
          <ID>Name</ID>
          <ToolTip>Display name</ToolTip>
          <Default>Unknown</Default>
        </Literal>
      </Declarations>
      <Code Language="cpp">
        <![CDATA[#include <iostream>

                  int main()
                  {
                      std::cout << "Hello $Name$" << std::endl;
                      return 0;
                  }]]>
      </Code>
    </Snippet>
  </CodeSnippet>
</CodeSnippets>
```