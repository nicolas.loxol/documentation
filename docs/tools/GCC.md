---
hide:
  - navigation
---

# **GCC** :star::star::star::star::star:
_________________

The **[GNU Compiler Collection (GCC)](https://gcc.gnu.org/)** is an optimizing compiler produced by the `GNU Project` supporting various programming languages, hardware architectures and operating systems.

The `Free Software Foundation` distributes **GCC** as free software under the `GNU General Public License`.

**GCC** is a key component of the `GNU toolchain` and the standard compiler for most projects related to `GNU` and the `Linux kernel`.

When it was first released in 1987 by **[Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman)**, `GCC 1.0` was named the `GNU C Compiler` since it only handled the `C` programming language.

It was extended to compile `C++` in December of that year.

## Installation
_________________

**GCC** can be installed using a packet manager on `Unix-like` system.

=== "C"

    ``` bash
    sudo apt install gcc
    ```

=== "C++"

    ``` bash
    sudo apt install g++
    ```

    ???+ note
        According to **GCC**'s online documentation link options and how `g++` is invoked, `g++` is equivalent to `gcc -xc++ -lstdc++ -shared-libgcc`.

        That means `C++` code can be compiled using **GCC**, but for convenience, `g++` is commonly used by most people.

???+ info
    This tutorial will cover **GCC** version 13.2

## How to use
_________________

When **GCC** is invoked, it typically does `preprocessing`, `compilation`, `assembly` and `linking`.

The “overall options” permit halting this process at an intermediate stage.

For example, the `-c` option says not to run the `linker`.

Then the output consists of `object files` output by the `assembler`.

Other options are passed on to one or more stages of processing.

Some options control the `preprocessor` and others the `compiler` itself.

Yet other options control the `assembler` and `linker`.

The **GCC** program accepts options and file names as operands.

Many options have multi-letter names; therefore multiple single-letter options may not be grouped: `-dv` is very different from `-d -v`.

It is possible to mix options and other arguments.

For the most part, the order does not matter.

The order of options becomes significant when using multiple options of the same kind.

For instance, specifying `-L` more than once, the directories are searched in the order specified.

Also, the placement of the `-l` option is significant.

Many options have long names starting with `-f` or with `-W` for example, `-fmove-loop-invariants`, `-Wformat` and so on.

Most of these have both positive and negative forms; the negative form of `-ffoo` is `-fno-foo`.

From this point, only the most used options will be covered.

The full option summary list can be found on the [official documentation page](https://gcc.gnu.org/onlinedocs/gcc/Option-Summary.html).

### Overall Options

`Compilation` can involve up to four stages: `preprocessing`, `compilation` proper, `assembly` and `linking`, always in that order.

**GCC** is capable of `preprocessing` and `compiling` several files either into several `assembler input files`, or into one assembler input file; then each assembler input file produces an `object file`, and `linking` combines all the object files (those newly compiled, and those specified as input) into an `executable file`.

- `-x language`

    Specify explicitly the language for the following input files (rather than letting the compiler choose a default based on the file name suffix).

    This option applies to all following input files until the next `-x` option.

    ``` bash
    gcc -x c++ main.cpp
    ```
    This command produce an `executable` file named `a.out`.

    <br>

- `-E`

    Stop after the `preprocessing` stage; do not run the `compiler` proper.

    The output is in the form of `preprocessed source code`, which is sent to the standard output.

    Input files that do not require `preprocessing` are ignored.

    ``` bash
    gcc -E main.c
    ```

    <br>

- `-S`

    Stop after the stage of `compilation` proper; do not `assemble`.

    The output is in the form of an `assembler code` file for each non-assembler input file specified.

    By default, the `assembler file` name for a source file is made by replacing the suffix `.c`, `.i`, etc., with `.s`.

    Input files that do not require `compilation` are ignored.

    ``` bash
    gcc -S main.c
    ```

    This command produce an `assembler file` file named `main.s`.

    <br>

- `-c`

    `Compile` or `assemble` the `source files`, but do not `link`.

    The `linking` stage simply is not done.

    The ultimate output is in the form of an `object file` for each source file.

    By default, the `object file` name for a source file is made by replacing the suffix `.c`, `.i`, `.s`, etc., with `.o`.

    Unrecognized input files, not requiring `compilation` or `assembly`, are ignored.

    ``` bash
    gcc -c main.c
    ```

    This command produce an `object file` file named `main.o`.

    <br>

- `-o file`

    Place the primary output in file `file`.

    This applies to whatever sort of output is being produced, whether it be an `executable file`, an `object file`, `an assembler file` or `preprocessed C code`.

    If `-o` is not specified, the default is to put an `executable file` in `a.out`.

    ``` bash
    gcc -o main main.c
    ```

    This command produce an `executable` file named `main`.

    <br>

- `-save-temps`

    The option `-save-temps` can do all the work done by using `-c`, `-S`, `-E` and `-o` above.

    Through this option, output at all the stages of compilation is stored in the current directory. This option produces the executable also.

    ``` bash
    gcc -save-temps main.c
    ```

    <br>

- `@file`

    Read command-line options from file.

    The options read are inserted in place of the original `@file` option.

    If file does not exist, or cannot be read, then the option will be treated literally, and not removed.

    Options in file are separated by whitespace.

    A whitespace character may be included in an option by surrounding the entire option in either single or double quotes.

    Any character (including a backslash) may be included by prefixing the character to be included with a backslash.

    The file may itself contain additional `@file` options; any such options will be processed recursively.

    ```txt title="options.txt" linenums="1"
    -x c++ -o main
    ```

    ``` bash
    gcc @options.txt main.c
    ```

### C/C++ Language Options

The following options control the dialect of `C` (or languages derived from `C`, such as `C++`) that the compiler accepts:

- `-ansi`

    In `C` mode, this is equivalent to `-std=c90`.
    In `C++` mode, it is equivalent to `-std=c++98`.

    This turns off certain features of **GCC** that are incompatible with `ISO C90` (when compiling `C` code), or of `standard C++` (when compiling `C++` code), such as the `asm` and `typeof` keywords, and predefined macros such as `unix` and `vax` that identify the type of system used.

    For the `C compiler`, it disables recognition of `C++ style ‘//’` comments as well as the `inline` keyword.

    ``` bash
    gcc -ansi main.c
    ```

    <br>

- `--std=standard`

    Determine the `language standard`.
    This option is currently only supported when compiling `C` or `C++`.

    The `compiler` can accept several `base standards`, such as `c89` or `c++98`, and `GNU dialects` of those standards, such as `gnu89` or `gnu++98`.

    Here is a list of the most famous `base standards` for `C` and `C++`:

    === "C"

        * `c90`: `ISO C90`
        * `c99`: `ISO C99`
        * `c11`: `ISO C11`
        * `c17`: `ISO C17`
        * `c23`: `ISO C23`

        ``` bash
        gcc --std=c99 main.c
        ```

    === "C++"

        * `c++98` `c++03`: The `1998 ISO C++ standard plus the 2003 technical corrigendum` and some additional defect reports
        * `c++11`: `2011 ISO C++ standard` plus amendments
        * `c++14`: `2014 ISO C++ standard` plus amendments
        * `c++17`: `2017 ISO C++ standard` plus amendments
        * `c++20: `2020 ISO C++ standard` plus amendments
        * `c++23`: `2023 ISO C++ standard` plus amendments
        * `c++26`: The next revision of the ISO C++ standard, planned for 2026

        ``` bash
        g++ --std=c++14 main.cpp
        ```

    <br>

- `-fabi-version=n`

    Use version n of the **[`C++ ABI`](https://en.wikipedia.org/wiki/Application_binary_interface)**.

    The default is version 0.

    Version 0 refers to the version conforming most closely to the `C++ ABI` specification.

    Therefore, the `ABI` obtained using version 0 will change in different versions of g++ as `ABI` bugs are fixed.

    ``` bash
    g++ -fabi-version=4 main.cpp
    ```

### Warning Options

Warnings are diagnostic messages that report constructions that are not inherently erroneous but that are risky or suggest there may have been an error.

The following language-independent options do not enable specific warnings but control the kinds of diagnostics produced by **GCC**.

- `-Wpedantic`

    Issue all the warnings demanded by strict `ISO C` and `ISO C++`; diagnose all programs that use forbidden extensions, and some other programs that do not follow `ISO C` and `ISO C++`.

    This follows the version of the `ISO C` or `C++ standard` specified by any `--std` option used.

    ``` bash
    gcc -Wpedantic main.c
    ```

    <br>

- `-w`

    Inhibit all warning messages.

    ``` bash
    gcc -w main.c
    ```

    <br>

- `-Wall`

    This enables all the warnings about constructions that some users consider questionable, and that are easy to avoid (or modify to prevent the warning), even in conjunction with macros.

    `-Wall` turns on the following warning flags:

    ```txt linenums="1"
    -Waddress
    -Warray-bounds=1 (only with -O2)
    -Warray-compare
    -Warray-parameter=2 (C and Objective-C only)
    -Wbool-compare
    -Wbool-operation
    -Wc++11-compat  -Wc++14-compat
    -Wcatch-value (C++ and Objective-C++ only)
    -Wchar-subscripts
    -Wcomment
    -Wdangling-pointer=2
    -Wduplicate-decl-specifier (C and Objective-C only)
    -Wenum-compare (in C/ObjC; this is on by default in C++)
    -Wenum-int-mismatch (C and Objective-C only)
    -Wformat
    -Wformat-overflow
    -Wformat-truncation
    -Wint-in-bool-context
    -Wimplicit (C and Objective-C only)
    -Wimplicit-int (C and Objective-C only)
    -Wimplicit-function-declaration (C and Objective-C only)
    -Winit-self (only for C++)
    -Wlogical-not-parentheses
    -Wmain (only for C/ObjC and unless -ffreestanding)
    -Wmaybe-uninitialized
    -Wmemset-elt-size
    -Wmemset-transposed-args
    -Wmisleading-indentation (only for C/C++)
    -Wmismatched-dealloc
    -Wmismatched-new-delete (only for C/C++)
    -Wmissing-attributes
    -Wmissing-braces (only for C/ObjC)
    -Wmultistatement-macros
    -Wnarrowing (only for C++)
    -Wnonnull
    -Wnonnull-compare
    -Wopenmp-simd
    -Wparentheses
    -Wpessimizing-move (only for C++)
    -Wpointer-sign
    -Wrange-loop-construct (only for C++)
    -Wreorder
    -Wrestrict
    -Wreturn-type
    -Wself-move (only for C++)
    -Wsequence-point
    -Wsign-compare (only in C++)
    -Wsizeof-array-div
    -Wsizeof-pointer-div
    -Wsizeof-pointer-memaccess
    -Wstrict-aliasing
    -Wstrict-overflow=1
    -Wswitch
    -Wtautological-compare
    -Wtrigraphs
    -Wuninitialized
    -Wunknown-pragmas
    -Wunused-function
    -Wunused-label
    -Wunused-value
    -Wunused-variable
    -Wuse-after-free=2
    -Wvla-parameter (C and Objective-C only)
    -Wvolatile-register-var
    -Wzero-length-bounds
    ```

    ``` bash
    gcc -Wall main.c
    ```

    <br>

- `-Wextra`

    This enables some extra warning flags that are not enabled by `-Wall`.

    ```txt linenums="1"
    -Wclobbered
    -Wcast-function-type
    -Wdeprecated-copy (C++ only)
    -Wempty-body
    -Wenum-conversion (C only)
    -Wignored-qualifiers
    -Wimplicit-fallthrough=3
    -Wmissing-field-initializers
    -Wmissing-parameter-type (C only)
    -Wold-style-declaration (C only)
    -Woverride-init
    -Wsign-compare (C only)
    -Wstring-compare
    -Wredundant-move (only for C++)
    -Wtype-limits
    -Wuninitialized
    -Wshift-negative-value (in C++11 to C++17 and in C99 and newer)
    -Wunused-parameter (only with -Wunused or -Wall)
    -Wunused-but-set-parameter (only with -Wunused or -Wall)
    ```

    ``` bash
    gcc -Wextra main.c
    ```

    <br>

- `-Werror`

    Make all warnings into errors.

    ``` bash
    gcc -Werror main.c
    ```

    <br>

- `-Wabi`

    Warn about code affected by `ABI` changes.

    `-Wabi` can also be used with an explicit version number to warn about `C++ ABI` compatibility with a particular `-fabi-version` level, e.g. `-Wabi=2` to warn about changes relative to `-fabi-version=2`.

    ``` bash
    g++ -Wabi=2 main.cpp
    ```

    <br>

Many specific warnings can be requested with options beginning with `-W`

- `-Wimplicit`

    Request warnings on implicit declarations.

    ``` bash
    gcc -Wimplicit main.c
    ```

    <br>

- `-Wunused-parameter`

    Warn whenever a function parameter is unused aside from its declaration.

    ``` bash
    gcc -Wunused-parameter main.c
    ```

### Static Analyser Options

- `-fanalyser`

    Performs additional checks on the code to identify potential problems that might lead to undefined behavior, security vulnerabilities, or other bugs.

    It can catch issues such as buffer overflows, null pointer dereferences, use-after-free errors, and more.

    ``` bash
    gcc -fanalyser main.c
    ```

### Debugging Options

To tell **GCC** to emit extra information for use by a `debugger`, in almost all cases it is only needed to add `-g` to the other options.

Some debug formats can co-exist (like `DWARF` with `CTF`) when each of them is enabled explicitly by adding the respective command line option to the other options.

- `-g`

    Produce `debugging information` in the operating system’s native format (`stabs`, `COFF`, `XCOFF`, or `DWARF`).

    `GDB` can work with this debugging information.

    ``` bash
    gcc -g main.c
    ```

    <br>

- `-ggdb`

    Produce debugging information for use by `GDB`. This means to use the most expressive format available (`DWARF`, `stabs`, or the native format if neither of those are supported), including `GDB` extensions if at all possible.

    ``` bash
    gcc -ggdb main.c
    ```

    <br>

- `-gdwarf`

    Produce debugging information in `DWARF` format.

    ``` bash
    gcc -gdwarf main.c
    ```

    <br>

- `-gsplit-dwarf`

    If `DWARF` debugging information is enabled, separate as much debugging information as possible into a separate output file with the extension `.dwo`.

    This option allows the build system to avoid linking files with debug information.

    To be useful, this option requires a debugger capable of reading `.dwo` files.

    ``` bash
    gcc -gdwarf -gsplit-dwarf main.c
    ```

### Optimization Options

These options control various sorts of optimizations.

Without any optimization option, the `compiler`’s goal is to reduce the cost of `compilation` and to make `debugging` produce the expected results.

Turning on `optimization flags` makes the `compiler` attempt to improve the `performance` and/or `code size` at the expense of `compilation time` and possibly the ability to `debug` the program.

The `compiler` performs optimization based on the knowledge it has of the program. Compiling multiple files at once to a single output file mode allows the `compiler` to use information gained from all of the files when compiling each of them.

Not all optimizations are controlled directly by a flag.

Only optimizations that have a flag are listed in this section.

Most optimizations are completely disabled at `-O0` or if an `-O` level is not set on the command line, even if individual optimization flags are specified.

Similarly, `-Og` suppresses many optimization passes.

- `-Ox`

    - `-O1`

        With `-O1`, the compiler tries to reduce code size and execution time, without performing any optimizations that take a great deal of compilation time.

        ```txt linenums="1"
        -fauto-inc-dec
        -fbranch-count-reg
        -fcombine-stack-adjustments
        -fcompare-elim
        -fcprop-registers
        -fdce
        -fdefer-pop
        -fdelayed-branch
        -fdse
        -fforward-propagate
        -fguess-branch-probability
        -fif-conversion
        -fif-conversion2
        -finline-functions-called-once
        -fipa-modref
        -fipa-profile
        -fipa-pure-const
        -fipa-reference
        -fipa-reference-addressable
        -fmerge-constants
        -fmove-loop-invariants
        -fmove-loop-stores
        -fomit-frame-pointer
        -freorder-blocks
        -fshrink-wrap
        -fshrink-wrap-separate
        -fsplit-wide-types
        -fssa-backprop
        -fssa-phiopt
        -ftree-bit-ccp
        -ftree-ccp
        -ftree-ch
        -ftree-coalesce-vars
        -ftree-copy-prop
        -ftree-dce
        -ftree-dominator-opts
        -ftree-dse
        -ftree-forwprop
        -ftree-fre
        -ftree-phiprop
        -ftree-pta
        -ftree-scev-cprop
        -ftree-sink
        -ftree-slsr
        -ftree-sra
        -ftree-ter
        -funit-at-a-time
        ```

        ``` bash
        gcc -O1 main.c
        ```

        <br>

    - `-02`

        Optimize even more.

        **GCC** performs nearly all supported optimizations that do not involve a space-speed tradeoff.

        As compared to `-O1`, this option increases both compilation time and the performance of the generated code.

        ```txt linenums="1"
        -falign-functions  -falign-jumps
        -falign-labels  -falign-loops
        -fcaller-saves
        -fcode-hoisting
        -fcrossjumping
        -fcse-follow-jumps  -fcse-skip-blocks
        -fdelete-null-pointer-checks
        -fdevirtualize  -fdevirtualize-speculatively
        -fexpensive-optimizations
        -ffinite-loops
        -fgcse  -fgcse-lm
        -fhoist-adjacent-loads
        -finline-functions
        -finline-small-functions
        -findirect-inlining
        -fipa-bit-cp  -fipa-cp  -fipa-icf
        -fipa-ra  -fipa-sra  -fipa-vrp
        -fisolate-erroneous-paths-dereference
        -flra-remat
        -foptimize-sibling-calls
        -foptimize-strlen
        -fpartial-inlining
        -fpeephole2
        -freorder-blocks-algorithm=stc
        -freorder-blocks-and-partition  -freorder-functions
        -frerun-cse-after-loop
        -fschedule-insns  -fschedule-insns2
        -fsched-interblock  -fsched-spec
        -fstore-merging
        -fstrict-aliasing
        -fthread-jumps
        -ftree-builtin-call-dce
        -ftree-loop-vectorize
        -ftree-pre
        -ftree-slp-vectorize
        -ftree-switch-conversion  -ftree-tail-merge
        -ftree-vrp
        -fvect-cost-model=very-cheap
        ```

        ``` bash
        gcc -O2 main.c
        ```

        <br>

    - `-03`

        Optimize yet more.

        ```txt linenums="1"
        -fgcse-after-reload
        -fipa-cp-clone
        -floop-interchange
        -floop-unroll-and-jam
        -fpeel-loops
        -fpredictive-commoning
        -fsplit-loops
        -fsplit-paths
        -ftree-loop-distribution
        -ftree-partial-pre
        -funswitch-loops
        -fvect-cost-model=dynamic
        -fversion-loops-for-strides
        ```

        ``` bash
        gcc -O3 main.c
        ```

    <br>

- `-Os`

    Optimize for size.

    `-Os` enables all `-O2` optimizations except those that often increase code size.

    ``` bash
    gcc -Os main.c
    ```

    <br>

- `-Ofast`

    Optimize for performance.

    Disregard strict standards compliance.

    `-Ofast` enables all `-O3` optimizations.

    It also enables optimizations that are not valid for all standard-compliant programs.

    ``` bash
    gcc -Ofast main.c
    ```

    <br>

- `-Og`

    Optimize debugging experience.

    ``` bash
    gcc -Og main.c
    ```

    <br>

- `-Oz`

    Optimize aggressively for size rather than speed.

    Oz behaves similarly to `-Os` including enabling most `-O2` optimizations.

    ``` bash
    gcc -Oz main.c
    ```

### Program Instrumentation Options

**GCC** supports a number of command-line options that control adding `run-time instrumentation` to the code it normally generates.

For example, one purpose of instrumentation is collect `profiling statistics` for use in finding program `hot spots`, `code coverage analysis`, or `profile-guided optimizations`.

Another class of `program instrumentation` is adding `run-time checking` to detect `programming errors` like `invalid pointer dereferences` or `out-of-bounds array accesses`, as well as `deliberately hostile attacks` such as `stack smashing` or `C++ vtable hijacking`.

- `-p` `-pg`

    Generate extra code to write profile information suitable for the analysis program `prof` (for `-p`) or `gprof` (for `-pg`).

    The option must be utilized during both compilation of the source files and during the linking process.

    === "prof"

        ``` bash
        gcc -p main.c
        ```

    === "gprof"

        ``` bash
        gcc -pg main.c
        ```

    <br>

- `-fprofile-arcs`

    Add code so that program flow arcs are instrumented.

    During execution the program records how many times each branch and call is executed and how many times it is taken or returns.

    ``` bash
    gcc -fprofile-arcs main.c
    ```

    <br>

- `-ftest-coverage`

    Produce a notes file that the `gcov` code-coverage utility can use to show program coverage.

    ``` bash
    gcc -ftest-coverage main.c
    ```

    <br>

- `--coverage`

    This option is used to compile and link code instrumented for coverage analysis.

    The option is a synonym for `-fprofile-arcs` `-ftest-coverage` (when `compiling`) and `-lgcov` (when `linking`).

    ``` bash
    gcc --coverage main.c
    ```

### Preprocessor Options

These options control the `C preprocessor`, which is run on each `C source file` before actual `compilation`.

With the option `-E`, nothing is done except `preprocessing`.

Some of these options make sense only together with `-E` because they cause the `preprocessor` output to be unsuitable for actual `compilation`.

- `-D name`

    Predefine `name` as a `macro`, with definition 1.

    ```c title="main.c" linenums="1"
    #include<stdio.h>

    int main(void)
    {
        #ifdef MY_MACRO
            printf("\n Macro defined \n");
        #endif

        return 0;
    }
    ```

    ``` bash
    gcc -DMY_MACRO main.c
    ```

    <br>

- `-U name`

    Cancel any previous definition of name, either built in or provided with a `-D` option.

    ``` bash
    gcc -UMY_MACRO main.c
    ```

### Linker Options

These options come into play when the `compiler` `links` object files into an `executable output file`.

They are meaningless if the compiler is not doing a `link` step.

- `-static`

    On systems that support dynamic linking, this prevents linking with the shared libraries.

- `-shared`

    Produce a shared object which can then be linked with other objects to form an executable.

- `-llibrary`

    Search the `library` named `library` when `linking`.

    The `-l` option is passed directly to the `linker` by **GCC**.

    The `linker` searches a standard list of directories for the library.

    The directories searched include several standard system directories plus any specified with `-L`.

    If both `static` and `shared libraries` are found, the `linker` gives preference to linking with the `shared library` unless the `-static` option is used.

    Here is an example of linkage using the `math library` and `thread library` from the standard.

    ``` bash
    gcc main.c -lm -lpthread
    ```

### Directory Options

These options specify `directories` to search for `header files` and for `libraries`.

- `-I dir`, `-iquote dir` `-isystem dir`

    Add the directory dir to the list of directories to be searched for header files during preprocessing.

    Directories specified with `-iquote` apply only to the quote form of the directive, `#!C++ #include "file"`.

    Directories specified with `-I` or `-isystem` apply to lookup for both the `#!C++ #include "file"` and `#!C++ #include <file>` directives.

    ``` bash
    gcc -I ../../include main.c
    ```

    <br>

- `-Ldir`

    Add directory `dir` to the list of directories to be searched for `-l`.

    ``` bash
    gcc -L ../../libraries main.c
    ```

### Code Generation Options

These machine-independent options control the interface conventions used in code generation.

- `fPIC`

    Generate `position-independent code` suitable for use in a `shared library`, if supported for the target machine.

    Such code accesses all constant addresses through a `global offset table`.

    ``` bash
    gcc -fPIC main.c
    ```

    <br>

- `fPIE`

    This option is similar to `-fPIC`, but the generated position-independent code can be only linked into executables.

    ``` bash
    gcc -fPIE main.c
    ```

    <br>

- `-fvisibility=[default|hidden]`

    Set the default `ELF image symbol visibility` to the specified option—all symbols are marked with this unless overridden within the code.

    Using this feature can very substantially improve `linking` and `load times` of `shared object libraries`, produce more `optimized code`, provide `near-perfect API export` and `prevent symbol clashes`.

    It is strongly recommended to use this in any distibuted `shared objects`.

    `default` means `public`; i.e., available to be linked against from outside the `shared object`.

    `hidden` means `private`; i.e., not available to be linked against from outside the `shared object`.

    The default if `-fvisibility` is not specified is `default`, i.e., make every symbol `public`.

    ``` bash
    gcc -fvisibility=hidden main.c
    ```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Free compiler
    - [x] Very comprehensive documentation

???+ danger "Cons"
    - [ ] Too many options