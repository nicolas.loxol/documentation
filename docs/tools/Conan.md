---
hide:
  - navigation
---

# **Conan** :star::star::star::star::star:
_________________

**[Conan](https://conan.io/)** is a dependency and package manager for `C` and `C++` languages.

It is free and open-source, works in several platforms including `Windows` and `Linux`, and can be used to develop various kinds of targets (embedded, mobile, etc).

It also integrates with all build systems like `CMake`, `MSBuild`, `Makefiles`, etc., including proprietary ones.

It is specifically designed and optimized for accelerating the development and Continuous Integration of `C` and `C++` projects.

Here are two of the main particularities of **Conan**:

* *Decentralized package manager*

    **Conan** is a decentralized package manager with a client-server architecture.

    This means that clients can fetch packages from, as well as upload packages to, different servers.

    At a high level, the servers are just storing packages.

    They do not build nor create the packages.

    The packages are created by the client, and if binaries are built from sources, that compilation is also done by the client application.

    ![Conan-Architecture](../ressources/img/conan_architecture.png)

    The different applications in the image above are:

    * The `Conan client` is a console/terminal command-line application, containing the heavy logic for package creation and consumption.

    `Conan client` has a local cache for package storage, and so it allows to fully create and test packages offline.

    Work offline can be done as long as no new packages are needed from remote servers.

    * [`JFrog Artifactory Community Edition (CE)`](https://jfrog.com/fr/artifactory/) is the recommended `Conan server` to host the packages privately.

    * [`ConanCenter`](https://conan.io/center) is a central public repository where the community contributes packages for popular open-source libraries like Boost, Zlib, OpenSSL, Poco, etc.

* *Binary management*

    One of the most powerful features of **Conan** is that it can create and manage pre-compiled binaries for any possible platform and configuration.

    By using pre-compiled binaries and avoiding repeated builds from source, it saves significant time for developers and Continuous Integration servers, while also improving the reproducibility and traceability of artifacts.

    A package is defined by a `conanfile.py`.

    This is a file that defines the package’s dependencies, sources, how to build the binaries from sources, etc.

    One package `conanfile.py` recipe can generate any arbitrary number of binaries, one for each different platform and configuration: operating system, architecture, compiler, build type, etc.

    These binaries can be created and uploaded to a server with the same commands in all platforms, having a single source of truth for all packages and not requiring a different solution for every different operating system.

    ![Conan-Package](../ressources/img/conan_package.png)

    Installation of packages from servers is also very efficient.

    Only the necessary binaries for the current platform and configuration are downloaded, not all of them.

    If the compatible binary is not available, the package can be built from sources in the client too.

## Installation
_________________

[Python](https://www.python.org/) is needed to install.

Open a terminal and type the following command:

``` bash
pip install conan
```

???+ info
    This tutorial will cover **Conan** version 2.3

## How to use
_________________

### Consuming packages

This section shows how to build the projects using **Conan** to manage the dependencies.

The `C++` application will perform some matrix calculation.

To do that, the [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) library will be used (3.4.0).

`CMake` version 3.29.0 will be the build system but keep in mind that **Conan** works with any build system and is not limited to using `CMake`.

Here is the basic project codebase:

``` c++ title="main.cpp" linenums="1" hl_lines="3 5 9"
#include <iostream>

#include "Eigen/Dense"

using Eigen::MatrixXd;

int main()
{
	MatrixXd m(2, 2);

	m(0, 0) = 3;
	m(1, 0) = 2.5;
	m(0, 1) = -1;
	m(1, 1) = m(1, 0) + m(0, 1);

	std::cout << m << std::endl;
}
```

``` cmake title="CMakeLists.txt" linenums="1" hl_lines="5 9"
cmake_minimum_required(VERSION 3.29)

project(Eigen)

find_package(Eigen3 REQUIRED)

add_executable(main main.cpp)

target_link_libraries(main Eigen3::Eigen)
```

#### Build a simple CMake project using Conan

The easiest way to install the [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) library and find it from the project with **Conan** is using a `conanfile.txt` file.

``` ini title="conanfile.txt" linenums="1" hl_lines="2"
[requires]
eigen/3.4.0

[generators]
CMakeDeps
CMakeToolchain
```

* `[requires]` section contains the declaration of the libraries used in the project

* `[generators]` section tells **Conan** to generate the files that the compilers or build systems will use to find the dependencies and build the project

???+ info
    `CMakeDeps` produces the necessary files for each dependency to be able to use the cmake `find_package()` function to locate the dependencies.

    `CMakeToolchain` produces the toolchain file that can be used in the command line invocation of `CMake` with the `-DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake`

Besides the `conanfile.txt`, a `Conan profile` is needed to build the project.

`Conan profiles` allow users to define a configuration set for things like the compiler, build configuration, architecture, shared or static libraries, etc.

**Conan**, by default, will not try to detect a profile automatically.

To let **Conan** try to guess the profile, based on the current operating system and installed tools, run:

``` bash
conan profile detect --force
```

This will detect the operating system, build architecture and compiler settings based on the environment.

It will also set the build configuration as Release by default.

The generated profile will be stored in the **Conan** home folder with name `default` and will be used by **Conan** in all commands by default unless another profile is specified via the command line.

The location of the default profile can be get using:

``` bash
conan profile path default
```

Here is an example of `Conan profile`:

``` ini title="default" linenums="1"
[settings]
arch=x86_64
build_type=Release
compiler=msvc
compiler.cppstd=14
compiler.runtime=dynamic
compiler.version=193
os=Windows
```

**Conan** will be used to install [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) and generate the files that `CMake` needs to find this library and build the project.

Those files will be generated in the folder build.

To do that, run:

``` bash
conan install . --output-folder=build --build=missing --profile=default
```

* `--build=missing` : Build packages from source whose binary package is not found
* `--profile=default` : Use the **Conan** default profile

Now, the app is ready to be built and executed.

=== "Windows"

    === "CMake standard"

        ``` bash
        cmake -B build -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake
        cmake --build build --config Release
        ```

    === "CMake preset"

        ``` bash
        cmake --preset conan-default
        cmake --build --preset conan-release
        ```

=== "Linux"

    === "CMake standard"

        ``` bash
        cmake -B build -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Release
        cmake --build build
        ```

    === "CMake preset"

        ``` bash
        cmake --preset conan-default
        cmake --build --preset conan-release
        ```

#### Using build tools as Conan packages

In the previous example, the `CMake` installed in the system was used already to build the application.

**Conan** can build the project with a specific build tool version, different from the one already installed system-wide.

In this case, declare this build dependency in **Conan** using a type of requirement named `tool_requires` in the `conanfile.txt`

For instance, the version 3.22.6 of `CMake` can be used to compile the project.

``` ini title="conanfile.txt" linenums="1" hl_lines="5"
[requires]
eigen/3.4.0

[tool_requires]
cmake/3.22.6

[generators]
CMakeDeps
CMakeToolchain
```

Then run:

``` bash
conan install . --output-folder=build --build=missing --profile=default
```

=== "Windows"

    On the build folder, two new file called `conanbuild.bat` and `deactivate_conanbuild.bat` have been generated

    The activation of the virtual environment can be done with:

    ``` bash
    conanbuild.bat
    ```

    With the environment enable, the application will be compiled using `CMake` 3.22.6

    In the same way, the deactivation of the virtual environment can be done with:

    ``` bash
    deactivate_conanbuild.bat
    ```

=== "Linux"

    On the build folder, two new file called `conanbuild.sh` and `deactivate_conanbuild.sh` have been generated

    The activation of the virtual environment can be done with:

    ``` bash
    source conanbuild.sh
    ```

    With the environment enable, the application will be compiled using `CMake` 3.22.6

    In the same way, the deactivation of the virtual environment can be done with:

    ``` bash
    source deactivate_conanbuild.sh
    ```

#### Building for multiple configurations: Release, Debug, Static and Shared

Modify build settings can be done in several ways:

* Create a new `Conan profile` and modify the build settings

    ``` ini title="debug" linenums="1" hl_lines="3"
    [settings]
    arch=x86_64
    build_type=Debug
    compiler=msvc
    compiler.cppstd=14
    compiler.runtime=dynamic
    compiler.version=193
    os=Windows
    ```

    ``` bash
    conan install . --output-folder=build --build=missing --profile=debug
    ```

* Specify the build setting on the `conan install` command

    ``` bash
    conan install . --output-folder=build --build=missing --settings=build_type=Debug
    ```

Modify build options can be done by:

* Specifying the build option on the `conan install` command

    ``` bash
    conan install . --output-folder=build --build=missing --options=eigen/3.4.0:shared=True
    ```

Now, try to run the compiled executable.

An error is triggered because the executable cannot find the [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page)'s shared libraries.

This is because shared libraries are loaded at runtime.

That means that the application executable needs to know where are the required shared libraries when it runs.

**Conan** provides a mechanism to define those variables and make it possible, for executables, to find and load these shared libraries.

For that, activate the environnement (see above).

???+ warning

    Noticed that for changing between Debug and Release configuration a `Conan setting` was used, but when setting the shared mode for the executable a `Conan option` was used.

    Please, note the difference between `settings` and `options`:

    * `settings` are typically a project-wide configuration defined by the client machine.
    * `options` are intended for package-specific configuration that can be set to a default value in the recipe.

#### Understanding the flexibility of using conanfile.py vs conanfile.txt

Using a `conanfile.txt` to build the projects using **Conan** it is enough for simple cases, but more flexibility can be get using a `conanfile.py` file where `Python` code is used to make things such as adding requirements dynamically, changing options depending on other options or setting options for the requirements.

The equivalent of the `conanfile.txt` in form of **Conan** recipe could look like this:

``` py title="conanfile.py" linenums="1"
from conan import ConanFile

class CompressorRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("eigen/3.4.0")

    def build_requirements(self):
        self.tool_requires("cmake/3.22.6")
```

This file is what is typically called a `Conan recipe`.

It can be used for consuming packages, like in this case, and also to create packages.

In the previous examples, every time the `conan install` command was executed, the `--output-folder` argument was used to define the location of the generated **Conan**'s files.

There is a neater way to decide where **Conan** will generate these files.

Define this directly in the `conanfile.py` inside the `layout()` method and make it work for every platform without adding more changes.

``` py title="conanfile.py" linenums="1" hl_lines="15-22"
import os

from conan import ConanFile

class CompressorRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("eigen/3.4.0")

    def build_requirements(self):
        self.tool_requires("cmake/3.22.6")

    def layout(self):
        multi = True if self.settings.get_safe("compiler") == "msvc" else False
        if multi:
            self.folders.generators = os.path.join("build", "generators")
            self.folders.build = "build"
        else:
            self.folders.generators = os.path.join("build", str(self.settings.build_type), "generators")
            self.folders.build = os.path.join("build", str(self.settings.build_type))
```

???+ info
    There are some pre-defined layouts that can be imported and directly use in the recipe.

    For example, for the `CMake` case, there is a `cmake_layout()` already defined in **Conan**:

    ``` py title="conanfile.py" linenums="1"
    from conan.tools.cmake import cmake_layout

    def layout(self):
        cmake_layout(self)
    ```

The `validate()` method is evaluated when Conan loads the `conanfile.py` and it can be used to perform checks of the input settings.

``` py title="conanfile.py" linenums="1" hl_lines="24-26"
import os

from conan import ConanFile

class CompressorRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("eigen/3.4.0")

    def build_requirements(self):
        self.tool_requires("cmake/3.22.6")

    def layout(self):
        multi = True if self.settings.get_safe("compiler") == "msvc" else False
        if multi:
            self.folders.generators = os.path.join("build", "generators")
            self.folders.build = "build"
        else:
            self.folders.generators = os.path.join("build", str(self.settings.build_type), "generators")
            self.folders.build = os.path.join("build", str(self.settings.build_type))

    def validate(self):
        if self.settings.os == "Macos":
            raise ConanInvalidConfiguration("Macos is not supported")
```


#### How to cross-compile the applications using Conan: host and build contexts

In the last chapter, the same platform was used for building and running the application.

But, what if the application is built on the machine running Ubuntu Linux and then run it on another platform like a Raspberry Pi ?

**Conan** can model that case using two different profiles, one for the machine that builds the application and another for the machine that runs the application.

Even with specifying only one `--profile` argument when invoking **Conan**, **Conan** will internally use two profiles.

One for the machine that builds the binaries (called the `build profile`) and another for the machine that runs those binaries (called the `host profile`).

Calling this command:

``` bash
conan install . --build=missing --profile=someprofile
```

Is equivalent to:

``` bash
conan install . --build=missing --profile:host=someprofile --profile:build=default
```

* **profile:host**: This is the profile that defines the platform where the built binaries will run.
* **profile:build**: This is the profile that defines the platform where the binaries will be built.

#### Introduction to versioning

So far the requires argument were fixed versions.

But sometimes dependencies evolve, new versions are released and consumers want to update to those versions as easy as possible.

It is always possible to edit the conanfiles and explicitly update the versions to the new ones, but there are mechanisms in **Conan** to allow such updates without even modifying the recipes.

``` py title="conanfile.py" linenums="1" hl_lines="8 11"

from conan import ConanFile

class CompressorRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("/[~3.0]")

    def build_requirements(self):
        self.tool_requires("cmake/[>3.10]")
```

* `[~3.0]` : Any 3.X version
* `[>3.10]` : Minimum version 3.11
* `eigen/3.4.0#87a7211557b6690ef5bf7fc599dd8349` : A special revision

The usage of version ranges, and the possibility of creating new revisions of a given package without bumping the version allows to do automatic faster and more convenient updates, without need to edit recipes.

But in some occasions, there is also a need to provide an immutable and reproducible set of dependencies.

This process is known as locking, and the mechanism to allow it is `lockfile` files.

A `lockfile` is a file that contains a fixed list of dependencies, specifying the exact version and exact revision.

``` py title="conanfile.py"
def requirements(self):
    self.requires("eigen/3.4.0")
```

``` bash
conan lock create .
```

``` json title="conan.lock"
{
    "version": "0.5",
    "requires": [
        "eigen/3.4.0#2e192482a8acff96fe34766adca2b24c%1680436083.8"
    ],
    "build_requires": [],
    "python_requires": []
}
```

Now, restore the original requires version range:

``` py title="conanfile.py" linenums="1"
def requirements(self):
        self.requires("/[~3.0]")
```

And run:

``` bash
conan install .
```

Which by default will find the `conan.lock`, and run the equivalent:

``` bash
conan install . --lockfile=conan.lock
```

Now, the version range is no longer resolved.

### Creating packages

This section shows how to create **Conan** packages using a **Conan** recipe.

A basic maths library using `CMake` will be created.

#### Create a Conan package

Use the `conan new` command to create `C/C++` package:

``` bash
conan new cmake_lib -d name=maths -d version=1.0
```

???+ info
    Different type of template are available including `basic`, `cmake_lib`, `cmake_exe`, etc

This will create a `Conan package` project with the following structure.

Add the source code of this `Conan package`.

``` bash
.
├── CMakeLists.txt
├── conanfile.py
├── include
│   └── addition.hpp
│   └── substraction.hpp
│   └── maths_export.hpp
├── src
│   └── addition.cpp
│   └── substraction.cpp
└── test_package
    ├── CMakeLists.txt
    ├── conanfile.py
    └── src
        └── example.cpp
```

The `Conan package` is composed of:

* **conanfile.py**: A `conanfile.py` which is the main recipe file, responsible for defining how the package is built and consumed.

* **CMakeLists.txt**: A simple generic `CMakeLists.txt`, with nothing specific about **Conan** in it.

* **src**: A `src` folder that contains the source files of the maths library.

* **include**: An `include` folder that contains the include files of the maths library.

* **test_package**: A folder that contains an example application that will require and link with the created package.

Here is the `conanfile.py`:

``` py title="conanfile.py" linenums="1"
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps

class mathsRecipe(ConanFile):
    name = "maths"
    version = "1.0"
    package_type = "library"

    # Optional metadata
    license = "MIT"
    author = "N.LOXOL my.mail@gmail.com"
    url = "https://gitlab.com/nicolasloxol"
    description = "A tiny maths library"
    topics = ("Maths")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    generators = "CMakeDeps", "CMakeToolchain"

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self)

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["maths"]
```

Several methods are declared:

* The `config_options()` method allows to fine-tune the binary configuration model, for example, in Windows, there is no `fPIC` option, so it can be removed.
* The `layout()` method is used to specify the locations where the source files are expected to be found, as well as the directories where the generated files should be saved during the build process.
* The `build()` will configure the project and build it from source.
* The `package()` method copies artifacts (headers, libs) from the build folder to the final package folder.
* The `package_info()` method defines that consumers must link with a “maths” library when using this package.

And the `CMakeLists.txt`:

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(Maths CXX)

file(GLOB INC_FILES include/*.hpp)
file(GLOB SRC_FILES src/*.cpp)

add_library(maths ${SRC_FILES})

target_include_directories(maths PUBLIC include)

set_target_properties(maths PROPERTIES PUBLIC_HEADER "include/addition.hpp;include/substraction.hpp;include/maths_export.hpp")

install(TARGETS maths)
```

Let’s build the package from sources with the current default configuration, and then let the test_package folder test the package

``` bash
conan create .
```

Validate that the recipe and the package binary are in the cache:

``` bash
conan list maths
conan list maths/1.0#:*
```

The `conan create` command receives the same parameters as `conan install`, so it is possible to pass to it the same settings and options.

``` bash
conan create . --setting=build_type=Debug --options=maths/1.0:shared=True
```

#### Handle sources in packages

In the previous example, `exports_sources` attribute of the `Conanfile` were used to declare the location of the sources for the library.

``` py title="conanfile.py" linenums="1"
exports_sources = "CMakeLists.txt", "src/*", "include/*"
```

However, sometimes the source files are stored in a repository or a file in a remote server, and not in the same location as the `Conanfile`.

Then use:

=== "Zip file"

    Retrieve the sources from a zip file stored in a remote repository.

    ``` py title="conanfile.py" linenums="1"
    def source(self):
        get(self, "https://******/maths.zip", strip_root=True)
    ```

    A `conandata.yml` file can be written in the same folder of the `conanfile.py`.

    This file will be automatically exported and parsed by **Conan** to extract the URLs of the external sources repositories, zip files etc.

    ``` yaml title="conandata.yml" linenums="1"
    sources:
    "1.0":
        url: "https://******/maths.zip"
        sha256: "7bc71c682895758a996ccf33b70b91611f51252832b01ef3b4675371510ee466"
        strip_root: true
    "1.1":
        url: ...
        sha256: ...
    ```

    The recipe does not need to be modified for each version of the code

    ``` py title="conanfile.py" linenums="1"
    def source(self):
        get(self, **self.conan_data["sources"][self.version])
    ```

=== "Git repository"

    Retrieve the sources from a branch of a git repository.

    ``` py title="conanfile.py" linenums="1"
    from conan.tools.scm import Git

    def source(self):
        git = Git(self)
        git.clone(url="https://gitlab.com/******/maths.git", target=".")
        # git.checkout("<tag> or <commit hash>") ## Switch to a git refs
    ```

#### Add dependencies to packages

A dependency can be added to the package in a manner similar to what was done in the consuming packages section.

``` py title="conanfile.py" linenums="1"
generators = "CMakeDeps"

def requirements(self):
    self.requires("eigen/3.4.0")
```

#### Use conan.tools.files.copy() in the package() method and packaging licenses

For the cases that the user does not want to rely on `CMake’s install` functionality or that he is using another build-system, **Conan** provides the tools to copy the selected files to the package_folder.

``` py title="conanfile.py" linenums="1"
def package(self):
    copy(self, "LICENSE", src=self.source_folder, dst=os.path.join(self.package_folder, "licenses"))
    copy(self, pattern="*.h", src=os.path.join(self.source_folder, "include"), dst=os.path.join(self.package_folder, "include"))
    copy(self, pattern="*.hpp", src=os.path.join(self.source_folder, "include"), dst=os.path.join(self.package_folder, "include"))
    copy(self, pattern="*.a", src=self.build_folder, dst=os.path.join(self.package_folder, "lib"), keep_path=False)
    copy(self, pattern="*.so", src=self.build_folder, dst=os.path.join(self.package_folder, "lib"), keep_path=False)
    copy(self, pattern="*.lib", src=self.build_folder, dst=os.path.join(self.package_folder, "lib"), keep_path=False)
    copy(self, pattern="*.dll", src=self.build_folder, dst=os.path.join(self.package_folder, "bin"), keep_path=False)
    copy(self, pattern="*.dylib", src=self.build_folder, dst=os.path.join(self.package_folder, "lib"), keep_path=False)
```

#### Define information for consumers: the package_info() method

Consumers that depend on that package will reuse those files, but some additional information have to be provided so that **Conan** can pass that to the build system and consumers can use the package.

For instance, consumers that want to link against this library will need some information:

* The location of the include folder in the Conan local cache to search for the header files.
* The name of the library file to link against it
* The location of the lib folder in the Conan local cache to search for the library file.

``` py title="conanfile.py" linenums="1"
def package_info(self):
        self.cpp_info.libs = ["maths"]
        self.cpp_info.libdirs = ["lib"] # conan sets libdirs = ["lib"] by default
        self.cpp_info.includedirs = ["include"] # conan sets includedirs = ["include"] by default
```

#### Testing Conan packages

Although it is understood that the `test_package` is invoked when conan create is called, the `test_package` can also be created independently if the `maths/1.0` package has already been created in the `Conan cache`.

This is done with the `conan test` command:

``` bash
conan test test_package maths/1.0
```

#### Header-only package

A header-only library is composed only of header files.

That means a consumer does not link with any library but includes headers, so only one binary configuration is needed for a header-only library.

As only one binary package is required, there is no need to declare the settings attribute.

This is a basic recipe for a header-only recipe:

``` py title="conanfile.py" linenums="1"
from conan import ConanFile
from conan.tools.files import copy

class mathsConan(ConanFile):
    name = "maths"
    version = "1.0"
    # No settings/options are necessary, this is header only
    exports_sources = "include/*"
    # Avoid copying the sources to the build folder in the cache
    no_copy_source = True

    def package(self):
        # This will also copy the "include" folder
        copy(self, "*.h", self.source_folder, self.package_folder)
        copy(self, "*.hpp", self.source_folder, self.package_folder)

    def package_info(self):
        # For header-only packages, libdirs and bindirs are not used
        # so it is necessary to set those as empty.
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []
```

#### Package prebuilt binaries

There are specific scenarios in which it is necessary to create packages from existing binaries, for example from 3rd parties or binaries previously built by another process or team that is not using **Conan**.

=== "Packaging already Pre-built Binaries"

    If there exists a local folder containing binaries for different configurations, packaging them can be accomplished using the following approach.

    These are the files of the example:

    ``` bash
    ├── conanfile.py
    └── vendor_maths_library
        ├── linux
        │   ├── x86
        │   │   ├── include
        │   │   │   └── maths.h
        │   │   └── libmaths.a
        │   └── x64
        │       ├── include
        │       │   └── maths.h
        │       └── libmaths.a
        └── windows
            ├── x86
            │   ├── maths.lib
            │   └── include
            │       └── maths.h
            └── x64
                ├── maths.lib
                └── include
                    └── maths.h
    ```

    ``` py title="conanfile.py" linenums="1"
    import os
    from conan import ConanFile
    from conan.tools.files import copy

    class mathsRecipe(ConanFile):
        name = "maths"
        version = "1.0"
        settings = "os", "arch"

        def layout(self):
            _os = str(self.settings.os).lower()
            _arch = str(self.settings.arch).lower()
            self.folders.build = os.path.join("vendor_maths_library", _os, _arch)
            self.folders.source = self.folders.build
            self.cpp.source.includedirs = ["include"]
            self.cpp.build.libdirs = ["."]

        def package(self):
            local_include_folder = os.path.join(self.source_folder, self.cpp.source.includedirs[0])
            local_lib_folder = os.path.join(self.build_folder, self.cpp.build.libdirs[0])
            copy(self, "*.h", local_include_folder, os.path.join(self.package_folder, "include"), keep_path=False)
            copy(self, "*.hpp", local_include_folder, os.path.join(self.package_folder, "include"), keep_path=False)
            copy(self, "*.lib", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)
            copy(self, "*.a", local_lib_folder, os.path.join(self.package_folder, "lib"), keep_path=False)

        def package_info(self):
            self.cpp_info.libs = ["maths"]
    ```

    ``` bash
    conan export-pkg . --settings=os="Linux" --settings=arch="x86"
    conan export-pkg . --settings=os="Linux" --settings=arch="x64"
    conan export-pkg . --settings=os="Windows" --settings=arch="x86"
    conan export-pkg . --settings=os="Windows" --settings=arch="x64"
    ```

=== "Downloading and Packaging Pre-built Binaries"

    If the libraries are not being built, it is probable that they are stored somewhere in a remote repository.

    In this case, creating a complete `Conan recipe`, with the detailed retrieval of the binaries could be the preferred method, because it is reproducible, and the original binaries might be traced.

    ``` py title="conanfile.py" linenums="1"
    import os
    from conan.tools.files import get, copy
    from conan import ConanFile

    class mathsConan(ConanFile):
        name = "maths"
        version = "1.0"
        settings = "os", "arch"

        def build(self):
            base_url = "https://github.com/****"

            _os = {"Windows": "win", "Linux": "linux", "Macos": "macos"}.get(str(self.settings.os))
            _arch = {"x86": "x86", "x64": "x64"}.get(str(self.settings.arch))
            url = "{}/{}_{}.tgz".format(base_url, _os, _arch)
            get(self, url)

        def package(self):
            copy(self, "*.h", self.build_folder, os.path.join(self.package_folder, "include"))
            copy(self, "*.hpp", self.build_folder, os.path.join(self.package_folder, "include"))
            copy(self, "*.lib", self.build_folder, os.path.join(self.package_folder, "lib"))
            copy(self, "*.a", self.build_folder, os.path.join(self.package_folder, "lib"))

        def package_info(self):
            self.cpp_info.libs = ["maths"]
    ```

    ``` bash
    conan export-pkg . --settings=os="Linux" --settings=arch="x86"
    conan export-pkg . --settings=os="Linux" --settings=arch="x64"
    conan export-pkg . --settings=os="Windows" --settings=arch="x86"
    conan export-pkg . --settings=os="Windows" --settings=arch="x64"
    ```

???+ note
    The `conan export-pkg` command creates a package binary directly from pre-compiled binaries in a user folder

### Working with Conan repositories

The previous section shows how to download and use packages from `Conan Center` (the official repository for open source `Conan packages`).

The creation of packages and the storage in the `Conan local cache` for reusing later was introduced on the previous chapter too.

This section covers how to use the `Conan repositories` to upload the recipes and binaries and store them for later use on another machine, project, or for sharing purposes.

#### Artifactory Community Edition for C/C++

[Artifactory Community Edition (CE)](https://jfrog.com/) for `C/C++` is the recommended server for development and hosting private packages for a team or company.

It is completely free, and it features a WebUI, advanced authentication and permissions, great performance and scalability, a REST API, a generic CLI tool and generic repositories to host any kind of source or binary artifact.

Run `Artifactory CE` using [docker](https://www.docker.com/)

``` bash
docker run --name artifactory -d -p 8081:8081 -p 8082:8082 docker.bintray.io/jfrog/artifactory-cpp-ce:latest
```

Once `Artifactory` has started, navigate to the default URL `http://localhost:8081`, where the Web UI should be running.

The default user is `admin` and password is `password`.

Navigate to `Administration -> Repositories -> Repositories`, then click on the `Add Repositories` button and select `Local Repository`.

A dialog for selecting the package type will appear, select **Conan**, then type a `Repository Key` (the name of the repository to create), for example “conan-local” and click on `Create Local Repository`.

Now, configure the `Conan client` to connect with the “conan-local” repository.

First add the remote to the `Conan remote registry`:

``` bash
conan remote add artifactory http://localhost:8081/artifactory/api/conan/conan-local
```

Then configure the credentials for the remote:

``` bash
conan remote login artifactory admin -p password
```

From now, the artifactory repos can be uploaded, downloaded and searched similarly to the other repo types.

Upload the maths packages on `artifactory`:

``` bash
conan upload maths -r=artifactory
conan search "*" -r=artifactory
```

#### Setting-up a Conan Server

The `Conan Server` is a free and open source server that implements `Conan remote repositories`.

It is a very simple application, used for testing inside the `Conan client` and distributed as a separate pip package.

Install the `Conan Server` using pip:

``` bash
pip install conan-server
```

Then run the server:

``` bash
conan_server
```

First add the remote to the `Conan remote registry`:

``` bash
conan remote add conan-server http://localhost:9300/v2
```

From now, the remote repos can be uploaded, downloaded and searched similarly to the other repo types.

Upload the maths packages

``` bash
conan upload maths -r=conan-server
conan search "*" -r=conan-server
```

???+ note
    In this example, the default `Conan server` configuration was used.

    Check the [official documentation page](https://docs.conan.io/2/reference/conan_server.html#reference-conan-server) to create a configuration file.

#### Uploading Packages

Now, take a look at the process of uploading both recipes and binaries to this remote and store them for later use on another machine, project, or for sharing purposes.

First, check if the remote intended for upload is already included in the current list of remotes.

``` bash
conan remote list
```

Now, upload the package recipe and all the packages to the remote.

In this example, `Artifactory` is utilized as a remote repository, though it could be replaced with any other similar service.

``` bash
conan upload maths -r=artifactory
```

Now try again to search the package from the remote.

``` bash
conan search maths -r=artifactory
```

And install the uploaded package from the `Conan repository`:

``` bash
conan install --requires=maths/1.0 -r=artifactory
```

### Other important Conan features

#### python_requires

It is possible to reuse code from other recipes using the `python_requires` feature.

If many recipes for various packages are maintained, sharing common logic without redundancy is achieved by placing the shared code in a `conanfile.py`, uploading it to the server.

Subsequently, other recipe `conanfiles` can depend on it via `python_requires = "mypythoncode/version"`, facilitating code reuse.

``` py title="conanfile.py" linenums="1" hl_lines="11"
from conan import ConanFile

myvar = 123

def myfunct():
    return 456

class Pkg(ConanFile):
    name = "pyrequire"
    version = "0.1"
    package_type = "python-require"
```

``` py title="conanfile.py" linenums="1" hl_lines="6 9-10"
from conan import ConanFile

class Pkg(ConanFile):
    name = "pkg"
    version = "0.1"
    python_requires = "pyrequire/0.1"

    def build(self):
        v = self.python_requires["pyrequire"].module.myvar  # v will be 123
        f = self.python_requires["pyrequire"].module.myfunct()  # f will be 234
```

#### Packages lists

It is possible to manage a list of packages, recipes and binaries together with the `packages-list` feature.

Several commands like `upload`, `download`, and `remove` allow receiving a list of packages file as an input, and they can do their operations over that list.

For example, removing everything from the cache can be done with:

``` bash
conan list *#* --format=json > pkglist.json
conan remove --list=pkglist.json  -c
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Tons of settings and options
    - [x] Various kind of build system integration
???+ danger "Cons"
    - [ ] Duplicate scripts (.bat and .sh)
    - [ ] CMake preset integration does not use cmake workflow steps

## Extra
_________________

### Cheat sheet

Here is a [cheat sheet](../ressources/conan_cheatsheet.pdf){:download="Conan2.0-CheatSheet"} for basic **Conan** commands and concepts.