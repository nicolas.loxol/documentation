---
hide:
  - navigation
---

# **Include What You Use** :star::star::star::star:
_________________

**[Include What You Use](https://github.com/include-what-you-use/include-what-you-use)** is a tool and a coding guideline for `C/C++` development.

It is designed to help `C/C++` developers manage and improve their code's inclusion of header files.

The goal of **IWYU** is to encourage more efficient and maintainable code by making sure that `C/C++` source files include only the necessary header files and not more than what they need.

This can result in faster compilation times, better code maintainability, and fewer dependencies.

## Installation
_________________

**Include What You Use** can be installed with the following commands:

=== "MSVC"

	**Include what you use** needs to be build from source for `MSVC` usage.

	* [Build standalone](https://github.com/include-what-you-use/include-what-you-use#how-to-build-standalone) : This build mode assumes `LLVM` and `Clang` libraries are already compiled on the system

	* [Build as part of LLVM](https://github.com/include-what-you-use/include-what-you-use#how-to-build-as-part-of-llvm)

=== "Unix-like system"

	``` bash
	sudo apt install iwyu
	```

???+ info
    This tutorial will cover **Include What You Use** version 0.22, which ensures compatibility with `Clang 18`.

## How to use
_________________

### What is Include What You Use and Why use it ?

**Include What You Use** is composed of 2 main elements:

* `IWYU Tool`: This is a command-line tool that runs on `C/C++` source code.
It analyzes the `#include` directives in the code and suggests which header files should be included or removed to adhere to the **IWYU** guidelines.
The tool is meant to be used as part of the build process, ideally as a static analysis step to check for compliance.

* `IWYU Coding Guidelines`: These are a set of rules and best practices that recommend which header files should be included and which should not.
The guidelines aim to promote the principle of minimal inclusion, where each source file should include only the headers required for its functionality.

Here are the main assets of using **IWYU**:

* *Faster compiles*: Every `.h` file brings when compiling a source file lengthens the time to compile, as the bytes have to be read, preprocessed, and parsed.
If the `.h` file is nor used, that cost is removed.
With template code, where entire instantiations have to be in `.h` files, this can be hundreds of thousands of bytes of code.

* *Fewer recompiles*: Many build tools, such as `make`, provide a mechanism for automatically figuring out what `.h` files a `.cc` file depends on.
These mechanisms typically look at `#include` lines.
When unnecessary `#includes` are listed, the build system is more likely to recompile in cases where it is not necessary.

* *Allow refactoring*: Suppose a developer want to refactor `foo.h` so it no longer uses vectors.
He would like to remove `#include <vector>` from `foo.h`, to reduce compile time.
Template class files such as vector can include a lot of code.
But can he?
In theory yes, but in practice maybe not: some other file may be `#including foo.h` and using vectors, and depending (probably unknowingly) on the `#include <vector>` to compile. The refactor could break code far away from him.
Here, it is the actual **Include What You Use** policy that saves the day.
If everyone who uses vector is `#including <vector>` themselves, then he can remove `<vector>` without fear of breaking anything.

* *Self-documentation*: When the `#include` lines can be trusted to accurately reflect what is used in the file, they can be used to help understand the code.
Looking at them, in itself, can help understand what this file needs in order to do its work.
If the optional `commenting` feature of `fix_includes.py` is used, what symbols, functions, and classes are used by this code can be seen.
It is like a pared-down version of `doxygen` markup, but totally automated and present where the code is.

* *Dependency cutting*: Suppose the binaries are larger than expected, and upon closer examination use symbols that seem totally irrelevant.
With **Include What You Use**, it can be easily determined by seeing who includes the files that define these symbols: those includers, and those alone, are responsible for the use.

* *Forward-declare*: **Include What You Use** tries very hard to figure out when a forward-declare can be used instead of an `#include`.
The reason for this is simple: if you an `#include` statement can be replaced by a forward-declare, the code size is reduced.

To demonstrate the possibilities of **Include What You Use**, the following codebase will be used:

``` c++ title="main.cpp" linenums="1"
#include <string>
#include <algorithm>

#include "Object.h"

int main()
{
	Object* o;
	std::string str;
}
```

``` c++ title="Object.cpp" linenums="1"
#include "Object.h"

#include <iostream>

Object::Object()
{
	std::cout << "OBJECT CLASS";
}
```

``` c++ title="Object.h" linenums="1"
#ifndef _OBJECT_
#define _OBJECT_

class Object
{
	public:
		Object();
};

#endif
```

### Running on single source file

The simplest way to use **IWYU** is by running it against a single source file

``` bash
include-what-you-use $CXXFLAGS SOURCES
```

* `$CXXFLAGS`: flags passed to the compiler
* `SOURCES`: sources files

To run **IWYU** on the `main.cpp` file:

``` bash
include-what-you-use main.cpp
```

Here is the command output:

``` bash
main.cpp should add these lines:
class object;

main.cpp should remove these lines:
- #include <algorithm>  // lines 2-2
- #include "object.h"  // lines 4-4

The full include-list for main.cpp:
#include <string>  // for string
class object;
---
```

As expected, the unused header `<algorithm>` should be removed and a forward declaration for the `Object` class must be used.

### Using with CMake

`CMake` has grown native support for **IWYU** as of version 3.3

The `<LANG>_INCLUDE_WHAT_YOU_USE` target property enables a mode where `CMake` first compiles a source file, and then runs **IWYU** on it where `<LANG>` is the language to enable.

The target property is initialized from the `CMAKE_<LANG>_INCLUDE_WHAT_YOU_USE` variable when a target is created.

=== "C"

	``` cmake title="CMakeLists.txt" linenums="1" hl_lines="5 7"
	cmake_minimum_required(VERSION 3.29)

	project(INCLUDE_WHAT_YOU_USE)

	find_program(IWYU NAME include-what-you-use REQUIRED)

	set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU})
	```

=== "C++"

	``` cmake title="CMakeLists.txt" linenums="1" hl_lines="5 7"
	cmake_minimum_required(VERSION 3.29)

	project(INCLUDE_WHAT_YOU_USE)

	find_program(IWYU NAME include-what-you-use REQUIRED)

	set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${IWYU})
	```

### Applying fixes

**IWYU** also include a tool that automatically fixes up the source files based on the `IWYU recommendations`.

???+ info
    This feature requires  `python3` and `IWYU source code`.


The `main.cpp` file can be corrected with the following command:

``` bash
include-what-you-use main.cpp 2> /tmp/iwyu.out
python3 fix_includes.py --comments < /tmp/iwyu.out
```

The `--comments` option puts comments after the `#include` lines.

Here is the resulting file:

``` c++ title="main.cpp" linenums="1"
#include <string>  // for string

class Object;

int main()
{
	Object* o;
	std::string str;
}
```

### IWYU Pragmas

[`IWYU pragmas`](https://github.com/include-what-you-use/include-what-you-use/blob/master/docs/IWYUPragmas.md) are used to give **IWYU** information that is not obvious from the source code, such as how different files relate to each other and which includes to never remove or include.

All pragmas start with `// IWYU pragma:` or `/* IWYU pragma:`.

They are case-sensitive and spaces are significant.

Here is a list of the most useful pragma:

* `IWYU pragma: keep`

	This pragma applies to a single #include directive or forward declaration. It forces IWYU to keep an inclusion even if it is deemed unnecessary.

	``` c++ linenums="1"
	#include <vector> // IWYU pragma: keep

	class ForwardDeclaration; // IWYU pragma: keep
	```

* `IWYU pragma: begin_keep/end_keep`

	This pragma applies to a set of #include directives and forward declarations. It declares that the headers and forward declarations in between are to be left alone by IWYU.

	``` c++ linenums="1"
	// IWYU pragma: begin_keep
	#include <vector>
	#include <iostream>

	class MyClass;
	// IWYU pragma: end_keep
	```

### IWYU Mapping

#### What is mapping ?

One of the difficult problems for **IWYU** is distinguishing between which header contains a symbol definition and which header is the actual documented header to include for that symbol.

For example, in `GCC`'s `libstdc++`, `std::unique_ptr<T>` is defined in `<bits/unique_ptr.h>`, but the documented way to get it is to `#include <memory>`.

To simplify **IWYU** deployment and command-line interface, many of these mappings are compiled into the executable.

These constitute the default mappings.

However, many mappings are toolchain and version-dependent.

Symbol homes and `#include` dependencies change between releases of `GCC` and are dramatically different for the standard libraries shipped with `Microsoft Visual C++`.

Also, mappings such as these are usually necessary for third-party libraries (e.g. `Boost`, `Qt`) or even project-local symbols and headers as well.

Any mappings outside of the default set can therefore be specified as external mapping files.

**IWYU**'s default mappings are hard-coded in `iwyu_include_picker.cc`, and are very `GCC`-centric.

There are both symbol and include mappings for `GNU libstdc++` and `libc`.

#### Create a mapping file

**IWYU** allow to create mapping files.

The mapping files conventionally use the `.imp` file extension.

They use a `JSON` meta-format with the following general form:

``` json linenums="1"
[
  { <directive>: <data> },
  { <directive>: <data> }
]
```

Directives can be one of the literal strings:

* *include*:

	The include directive specifies a mapping between two include names.

	This is typically used to map from a private implementation detail header to a public facade header, such as the `<bits/unique_ptr.h>` to `<memory>` example above.

	Data for this directive is a list of four strings containing:

	* The include name to map from
	* The visibility of the include name to map from
	* The include name to map to
	* The visibility of the include name to map to

	``` json linenums="1"
	{ include: ["<bits/unique_ptr.h>", "private", "<memory>", "public"] }
	```

* *symbol*:

	The symbol directive maps from a qualified symbol name to its authoritative header.

	Data for this directive is a list of four strings containing:

	* The symbol name to map from
	* The visibility of the symbol
	* The include name to map to
	* The visibility of the include name to map to

	``` json linenums="1"
	{ symbol: ["NULL", "private", "<cstddef>", "public"] }
	```

* *ref*:

	The last kind of directive, ref, is used to pull in another mapping file, much like the `C` preprocessor's `#include` directive.

	Data for this directive is a single string: the filename to include.

	``` json linenums="1"
	{ ref: "more.symbols.imp" },
	{ ref: "/usr/lib/other.includes.imp" }
	```

#### Specifying mapping files

Mapping files are specified on the command-line using the `--mapping_file` option:

``` bash
include-what-you-use -Xiwyu --mapping_file=foo.imp some_file.cc
```

### How to correct IWYU mistakes

Considering **IWYU** is still under development, sometimes, **IWYU** can be wrong about the include file or the forward-declare.

In that case, the following should be used:

* If `fix_includes.py` has removed an `#include` actually needed, add it back in with the comment `// IWYU pragma: keep` at the end of the `#include line`.
* If `fix_includes.py` has added an `#include` not needed, just take it out.
* If `fix_includes.py` has wrongly added or removed a forward-declare, just fix it up manually.
* If `fix_includes.py` has suggested a private header file instead of the proper public header file, `IWYU Mapping` can be used to fix it.

## Pros and cons
_________________

???+ success "Pros"
    - [x] Ease to use
    - [x] Direct integration with CMake
???+ danger "Cons"
    - [ ] Requires Clang tools
    - [ ] Command-line utility is limited to a single file