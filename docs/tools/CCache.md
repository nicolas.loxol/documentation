---
hide:
  - navigation
---

# **Ccache** :star::star::star::star:
_________________

**[Ccache](https://ccache.dev/)** is a software development tool used to speed up recompilation of `C/C++` code by caching previous compilations.

It acts as a compiler cache, storing compiled object files in a cache directory so that they can be reused if the same compilation is requested again.

This can significantly reduce the time it takes to rebuild a project, especially in scenarios where developers frequently make small changes to source code and recompile.

When a compilation is requested, **Ccache** checks if it has already compiled the same source code with the same compiler options before.

If it finds a match in its cache, it will reuse the previously compiled object file instead of invoking the compiler again, which can dramatically speed up the compilation process.

If there is no match, **Ccache** will invoke the compiler as usual and cache the resulting object file for future use.

## Installation
_________________

To install **Ccache**, run the following commands:

=== "MSVC"

    Download the latest release on [the official website](https://ccache.dev/download.html)

=== "Unix-like system"

    ``` bash
    sudo apt install ccache
    ```

???+ info
    This tutorial will use **Ccache** version 4.9.1

## How to use
_________________

## Using Ccache with a compiler

To use **Ccache** with a `compiler`, prefix the compilation command with the `ccache` command.

``` bash
ccache g++ main.cpp -o main -Wall -g -O2
```

This command tells **Ccache** to use the `g++` compiler to compile `main.cpp` into a `main` executable.

If **Ccache** has already compiled `main.cpp` with the same compiler options before, it will reuse the cached object file instead of invoking the compiler again, which can significantly speed up the compilation process.

If **Ccache** has not compiled `main.cpp` with the same compiler options before, it will invoke `g++` to compile the source code as usual and cache the resulting object file for future use.

Subsequent compilations of `main.cpp` using the same `g++` compiler will be faster because **Ccache** will reuse the cached object file.

By default, **Ccache** stores its cache files in a directory specified by the `CCACHE_DIR` environment variable or in a default location (such as `~/.ccache` on `Unix-like` systems).

Please note that modifying or deleting cache files manually is not recommended and may cause unexpected behavior with **Ccache**.

Managing the cache can be done using **Ccache**'s built-in options.

Here are the two main built-in commands designed to delete the cache:

* `--cleanup`

    Clean up the cache by removing not recently used cached files until the specified file number and cache size limits are not exceeded.

* `--clear`

    Clear the entire cache, removing all cached files, but keeping the configuration file.

## Using CMake build system

### Command line option

Using **Ccache** with `CMake` can be done by setting `CMAKE_<LANG>_COMPILER_LAUNCHER` when running the `CMake` configuration step.

=== "C"

    Using command line:

    ``` bash
    cmake -S . -B build -DCMAKE_C_COMPILER_LAUNCHER=ccache
    ```

    Using CMakeLists:

    ``` cmake title="CMakeLists.txt" linenums="1"
    find_program(CCACHE_EXECUTABLE ccache)

    if(CCACHE_EXECUTABLE)
        set(CMAKE_C_COMPILER_LAUNCHER ${CCACHE_EXECUTABLE})
    endif()
    ```

=== "C++"

    Using command line:

    ``` bash
    cmake -S . -B build -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
    ```

    Using CMakeLists:

    ``` cmake title="CMakeLists.txt" linenums="1"
    find_program(CCACHE_EXECUTABLE ccache)

    if(CCACHE_EXECUTABLE)
        set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE_EXECUTABLE})
    endif()
    ```

### MSVC special feature

**Ccache** supports caching the output of the `Microsoft Visual Studio Compiler (MSVC)` since version 4.6.

Here is an example of CMake integration of **Ccache** using `MSVC` generator:

``` cmake title="CMakeLists.txt" linenums="1"
find_program(ccache_exe ccache)

if(ccache_exe)
  file(COPY_FILE ${ccache_exe} ${CMAKE_CURRENT_BINARY_DIR}/cl.exe ONLY_IF_DIFFERENT)

  set(CMAKE_MSVC_DEBUG_INFORMATION_FORMAT "$<$<CONFIG:Debug,RelWithDebInfo>:Embedded>")

  set(CMAKE_VS_GLOBALS
    "CLToolExe=cl.exe"
    "CLToolPath=${CMAKE_CURRENT_BINARY_DIR}"
    "TrackFileAccess=false"
    "UseMultiToolTask=true"
    "DebugInformationFormat=OldStyle"
  )
endif()
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Easy to use
    - [x] Multi-platform

???+ danger "Cons"
    - [ ] Longer first time compilation