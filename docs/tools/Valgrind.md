---
hide:
  - navigation
---

# **Valgrind** :star::star::star::star:
_________________

**[Valgrind](https://valgrind.org/)** is a programming tool for `memory debugging`, `memory leak detection`, and `profiling` `C\C++` languages.

Originally, **Valgrind** was designed to be a free memory debugging tool for `Unix based` system on `x86`, but it has since evolved to become a generic framework for creating `dynamic analysis` tools such as `checkers` and `profilers`.

More generally, **Valgrind** is based on a generic architecture that allows the supervision of programs using three main tools:

- `Memcheck`: a memory leak detector (default option)
- `Cachegrind`: a cache simulator
- `Callgrind`: a profiler

## Installation
_________________

**Valgrind** can be installed using the following command:

``` bash
sudo apt install valgrind
```

???+ info
    This tutorial will cover **Valgrind** version 3.23.0

## How to use
_________________

For the following examples, a code that producing a `Segmentation fault (core dumped)` will be used.

``` C++ title="main.cpp" linenums="1"
#include <iostream>

int main(int, char**)
{
    int* p = nullptr;

    *p = 5;

    return 0;
}
```

???+ warning
    Compile the program with `-g` to include debugging information so that `Memcheck`'s error messages include exact line numbers.

    Additionally, using `-O0` is recommended as optimization can lead to inaccurate behavior

    === "C"

        ``` bash
        gcc -g -O0 main.c -o main
        ```

    === "C++"

        ``` bash
        g++ -g -O0 main.cpp -o main
        ```

### Memory leak detection

The memory leak detection tool used is `memcheck`.

Run the following command to check for memory leaks in the program:

``` bash
valgrind --tool=memcheck --leak-check=yes --leak-resolution=high --show-reachable=yes --track-origins=yes ./main
```

The program will run much slower (eg. 20 to 30 times) than normal and will use a lot more memory.

`Memcheck` will issue messages about `memory errors and leaks` that it detects.

* The `--leak-check` option turns on the detailed memory leak detector.

* The `--show-reachable` option extends the search to areas that are still pointed to but not deallocated.

* The `--leak-resolution` ooption indicates the level of detail for memory leak searches.

`Memcheck` also reports uses of `uninitialised values`, most commonly with the message `Conditional jump or move depends on uninitialised value(s)`.

It can be difficult to determine the root cause of these errors.

Use the `--track-origins=yes` to get extra information.

Here is the output of the command:

``` txt linenums="1" hl_lines="6-8"
==8664== Memcheck, a memory error detector
==8664== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==8664== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
==8664== Command: ./main
==8664==
==8664== Invalid write of size 4
==8664==    at 0x109184: main (main.cpp:7)
==8664==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
==8664==
==8664==
==8664== Process terminating with default action of signal 11 (SIGSEGV)
==8664==  Access not within mapped region at address 0x0
==8664==    at 0x109184: main (main.cpp:7)
==8664==  If you believe this happened as a result of a stack
==8664==  overflow in your program's main thread (unlikely but
==8664==  possible), you can try to increase the size of the
==8664==  main thread stack using the --main-stacksize= flag.
==8664==  The main thread stack size used in this run was 8388608.
==8664==
==8664== HEAP SUMMARY:
==8664==     in use at exit: 72,704 bytes in 1 blocks
==8664==   total heap usage: 1 allocs, 0 frees, 72,704 bytes allocated
==8664==
==8664== 72,704 bytes in 1 blocks are still reachable in loss record 1 of 1
==8664==    at 0x4848899: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
==8664==    by 0x4913979: ??? (in /usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.30)
==8664==    by 0x400647D: call_init.part.0 (dl-init.c:70)
==8664==    by 0x4006567: call_init (dl-init.c:33)
==8664==    by 0x4006567: _dl_init (dl-init.c:117)
==8664==    by 0x40202E9: ??? (in /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2)
==8664==
==8664== LEAK SUMMARY:
==8664==    definitely lost: 0 bytes in 0 blocks
==8664==    indirectly lost: 0 bytes in 0 blocks
==8664==      possibly lost: 0 bytes in 0 blocks
==8664==    still reachable: 72,704 bytes in 1 blocks
==8664==         suppressed: 0 bytes in 0 blocks
==8664==
==8664== For lists of detected and suppressed errors, rerun with: -s
==8664== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
```

### Cache simulation

The `cachegrind` tool is a `cache simulator`.

It is possible to manually specify the configuration of the cache to be simulated.

For each cache, indicate its size, associativity and line size.

Sizes must be given in bytes.

For example, type the following command to launch the cache simulation on the `main` executable:

``` bash
valgrind --tool=cachegrind ./main
```

Here is the output of the command:

``` txt linenums="1" hl_lines="8-10"
==9501== Cachegrind, a cache and branch-prediction profiler
==9501== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==9501== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
==9501== Command: ./main
==9501==
--9501-- warning: L3 cache found, using its data for the LL simulation.
==9501==
==9501== Process terminating with default action of signal 11 (SIGSEGV)
==9501==  Access not within mapped region at address 0x0
==9501==    at 0x109169: main (main.cpp:4)
==9501==  If you believe this happened as a result of a stack
==9501==  overflow in your program's main thread (unlikely but
==9501==  possible), you can try to increase the size of the
==9501==  main thread stack using the --main-stacksize= flag.
==9501==  The main thread stack size used in this run was 8388608.
==9501==
==9501== I   refs:      2,279,042
==9501== I1  misses:        1,753
==9501== LLi misses:        1,741
==9501== I1  miss rate:      0.08%
==9501== LLi miss rate:      0.08%
==9501==
==9501== D   refs:        745,207  (547,991 rd   + 197,216 wr)
==9501== D1  misses:       15,559  ( 13,119 rd   +   2,440 wr)
==9501== LLd misses:        9,133  (  7,551 rd   +   1,582 wr)
==9501== D1  miss rate:       2.1% (    2.4%     +     1.2%  )
==9501== LLd miss rate:       1.2% (    1.4%     +     0.8%  )
==9501==
==9501== LL refs:          17,312  ( 14,872 rd   +   2,440 wr)
==9501== LL misses:        10,874  (  9,292 rd   +   1,582 wr)
==9501== LL miss rate:        0.4% (    0.3%     +     0.8%  )
Segmentation fault (core dumped)
```

### Profiling

The `callgrind` tool allows advanced `profiling` of a program by counting the number of calls and their associated costs.

For example, type the following command to start profiling the `main` executable:

``` bash
valgrind --tool=callgrind ./main
```

Here is the output of the command:

``` txt linenums="1" hl_lines="8-10"
==9958== Callgrind, a call-graph generating cache profiler
==9958== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==9958== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
==9958== Command: ./main
==9958==
==9958== For interactive control, run 'callgrind_control -h'.
==9958==
==9958== Process terminating with default action of signal 11 (SIGSEGV)
==9958==  Access not within mapped region at address 0x0
==9958==    at 0x109169: main (main.cpp:4)
==9958==  If you believe this happened as a result of a stack
==9958==  overflow in your program's main thread (unlikely but
==9958==  possible), you can try to increase the size of the
==9958==  main thread stack using the --main-stacksize= flag.
==9958==  The main thread stack size used in this run was 8388608.
==9958==
==9958== Events    : Ir
==9958== Collected : 2277579
==9958==
==9958== I   refs:      2,277,579
Segmentation fault (core dumped)
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Various tools
    - [x] Comprehensive and complete documentation
???+ danger "Cons"
    - [ ] Not multi-platform

## Extra
_________________

### Kcachegrind

**[Kcachegrind](https://kcachegrind.github.io/html/Home.html)** is profile data visualization tool that allow to visualize **Valgrind** report on graphs using the profiling data generated by `Cachegrind`.

`Cachegrind` generates a file named `cachegrind.out.XXXX` where `XXXX` is a identifier.

``` bash
kcachegrind cachegrind.out.9501
```