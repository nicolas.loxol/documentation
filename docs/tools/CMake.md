---
hide:
  - navigation
---

# **CMake** :star::star::star::star::star:

**[CMake](https://cmake.org/)**, short for `Cross-Platform Make`, is an open-source build system that facilitates the management and compilation of software projects across various platforms and build environments.

It provides a platform-independent way to describe the build process of a project using a simple scripting language, allowing developers to generate build configurations for different operating systems, compilers, and build tools.

## Installation

=== "Windows"

    Download the latest version of **CMake** on the [official website](https://cmake.org/download/)

=== "Unix-like system"

    ``` bash
    sudo apt install cmake
    ```

???+ info
    This tutorial will cover `CMake` version 3.29.0

## How to use

This section references the most basic aspects of **CMake**.

It will equip a developer who has never used **CMake** before with the necessary skills to be able to create, build, test, install, and package a simple executable or library.

### CMake development workflow

When working with **CMake**, it helps to understand its development workflow.

Loosely speaking, the start to end process according to **CMake** looks something like this:

![CMake-dev-workflow](../ressources/img/cmake_dev_workflow.png)

The first stage takes a generic project description and generates platform-specific project files suitable for use with the developer’s regular build tool of choice (`make`, `Xcode`, `Visual Studio` etc.).

While this setup stage is what **CMake** is best known for, the **CMake** suite of tools also includes `CTest` and `CPack`.

These manage the testing and packaging stages respectively.

The entire process from start to finish can be driven from **CMake** itself, and each stage can also be executed individually through **CMake**.

This abstracts away platform differences, making some tasks much simpler.

### Minimal project

Without a build system, a project is just a collection of files.

**CMake** brings some order to this, starting with a human-readable file called `CMakeLists.txt` that defines what should be built and how, what tests to run, and what packages to create.

This file is a platform-independent description of the whole project, which **CMake** then turns into platform-specific build tool project files.

As its name suggests, it is just an ordinary text file which developers edit in their favorite text editor or development environment.

Here is an example of a basic `CMakeLists.txt` that create an executable based on a source file:

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29) # Require 3.29 as the minimum version of cmake

project(MyProject C CXX) # Set the name of the project and test the C/C++ compiler and linker

add_executable(MyExecutable main.cpp) # Add an executable to the project using the specified source files
```

The most basic way to run **CMake** is via the **cmake** command line utility.

The simplest way to invoke it is to pass options to **cmake** for the location of the source tree (`-S`), the output folder for the build files (`-B`) and the generator type (`-G`).

The next step is to build the application using the `cmake --build` command with the folder containing the build files.

Once the application is compiled, the last step is to execute it.

For example:

=== "MSVC"

    ``` bash
    cmake -S . -B build -G "Visual Studio 17 2022"
    cmake --build build/ --config Release
    build\\Release\\MyExecutable.exe
    ```

=== "Unix-like system"

    ``` bash
    cmake -S . -B build -G "Unix Makefiles" --config Release
    cmake --build build/
    build/MyExecutable
    ```

???+ note
    Some generators produce projects which support multiple configurations (`Debug`, `Release`, and so on).

    These allow the developer to choose between different build configurations without having to re-run **CMake**.

    This is more convenient for generators that create projects for use in `IDE` environments like `Xcode` and `Visual Studio`.

    For generators which do not support multiple configurations, the developer has to re-run **CMake** to switch the build between `Debug`, `Release`, etc.

### Building simple targets

#### Executables

It is relatively straightforward to define a simple executable in **CMake**.

The simple example given previously required defining a target name for the executable and listing the source files to be compiled.

``` cmake title="CMakeLists.txt" linenums="1"
add_executable(MyApp main.cpp a.cpp b.cpp)
```

#### Libraries

**CMake** supports building a variety of different kinds of libraries, taking care of many of the platform differences, but still supporting the native idiosyncrasies of each.

=== "Static"

    ``` cmake title="CMakeLists.txt" linenums="1"
    add_library(MyLib STATIC a.cpp b.cpp)
    ```

=== "Shared"

    ``` cmake title="CMakeLists.txt" linenums="1"
    add_library(MyLib SHARED a.cpp b.cpp)
    ```

=== "Header-only"

    ``` cmake title="CMakeLists.txt" linenums="1"
    add_library(MyLib INTERFACE)
    ```

=== "Module"

    ``` cmake title="CMakeLists.txt" linenums="1"
    add_library(MyLib MODULE a.cpp b.cpp)
    ```

???+ info
    It is possible to omit the keyword defining what type of library to build.

    Unless the project specifically requires a particular type of library, the preferred practice is to not specify it and leave the choice up to the developer when building the project.

    In such cases, the library will be either `STATIC` or `SHARED`, with the choice determined by the value of a **CMake** variable called `BUILD_SHARED_LIBS`.

#### Linking targets

When considering the targets that make up a project, developers are typically used to thinking in terms of library A needing library B, so A is linked to B.

This is the traditional way of looking at library handling, where the idea of one library needing another is very simplistic.

In reality, there are a few different types of dependency relationships that can exist between libraries:

* Private : Private dependencies specify that library A uses library B in its own internal implementation.

Anything else that links to library A does not need to know about B because it is an internal implementation detail of A.

* Public : Public dependencies specify that not only does library A use library B internally, it also uses B in its interface.

This means that A cannot be used without B, so anything that uses A will also have a direct dependency on B.

* Interface : Interface dependencies specify that in order to use library A, parts of library B must also be used.

This differs from a public dependency in that library A does not require B internally, it only uses B in its interface.

**CMake** captures this richer set of dependency relationships with its `target_link_libraries()` command, not just the simplistic idea of needing to link.

Here is a basic example of the `target_link_libraries()` command:

``` cmake title="CMakeLists.txt" linenums="1"
target_link_libraries(MyLib
  PUBLIC A
  PRIVATE B C
)
```

#### Linking non-targets

In the preceding section, all the items being linked to were existing **CMake** targets, but the `target_link_libraries()` command is more flexible than that.

In addition to **CMake** targets, the following things can also be specified as items in a `target_link_libraries()` command:

* Full path to a library file : **CMake** will add the library file to the linker command

* Plain library name : If just the name of the library is given with no path, the linker command will search for that library (e.g. `foo` becomes `-lfoo` or `foo.lib`, depending on the platform).

* Link flag : As a special case, items starting with a hyphen other than `-l` or `-framework` will be treated as flags to be added to the linker command.

### Testing

**CMake** provides a separate command-line tool called `ctest`.

It can be thought of as a test scheduling and reporting tool, offering close integration with **CMake** for defining tests in a convenient and flexible way.

Typically, **CMake** will generate the input file necessary for `ctest` based on details provided by the project.

The following minimal example shows how to define a project with a couple of simple test cases:

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(MyProj VERSION 1.0.2)

enable_testing() # instruct CMake to produce an input file for `ctest`

add_executable(MyExecutable main.cpp)

add_test(NAME MyTest1 COMMAND MyExecutable) # Test case 1
add_test(NAME MyTest2 COMMAND MyExecutable 5 "Hello") # Test case 2
```

By default, a test is deemed to pass if it returns an exit code of 0.

The tests can be executed using the command-line tool `ctest`:

``` bash
ctest
```

???+ note
    As a convenience, **CMake** also defines a test build target, which runs `ctest` with a default set of options.

    It always runs all tests, and it provides only very limited control over the test output and the way tests are run.

### Installing

While things built by a project can often be used directly from the build directory, that is often just a stepping stone on the way to deploying the project.

Deployment can take different forms, but a common element to most of them is an install step.

A convenient way of handling headers is to use file sets.

A file set can associate headers with a target, and the headers can be installed along with the target in an `install(TARGETS)` call.

No separate `install(FILES)` or `install(DIRECTORY)` call is needed.

In addition, a file set provides information about whether a header is private or public, and also about the header search paths a consumer of the target must use.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(MyProj VERSION 1.0.2)

add_library(MyLib main.cpp a.cpp b.cpp)

target_sources(MyLib
  PUBLIC
  FILE_SET api
  TYPE HEADERS
  BASE_DIRS headers
  FILES
  headers/a.h
  headers/b.h
)

install(TARGETS MyLib FILE_SET api)
```

During installation, files are copied from the build directory (and possibly the source directory) to the install location.

???+ info
    **CMake** will use default locations that correspond to the convention used on most `Unix-like` systems.

    The same layout also works on `Windows`

The following command shows how to install a project:

``` bash
cmake --install . --prefix /install_path
```

???+ info
    If `--prefix` is not given, the install location is taken from the platform-dependent value `CMAKE_INSTALL_PREFIX`.

As was the case for testing, multi-configuration generators are also supported for installs too.

The configuration must be specified as part of the build and install steps:

``` bash
cmake --install . --config Debug
```

### Packaging

**CMake** provides the `cpack` tool, which can produce binary packages in a variety of formats.

These include simple archives like `.zip`, `.tar.gz` and `.7z`, packages for platform-specific packaging systems like `RPM`, `DEB`, and `MSI`, and even standalone graphical installers.

These are all based on installing a project using the information provided through `install()` commands, and others.

Internally, `cpack` effectively does one or more `cmake --install` commands with `--prefix` set to a temporary staging area.

The contents of that staging area are then used to create a package in the relevant format.

Basic packaging is implemented by setting some relevant **CMake** variables, then including a **CMake** module called `CPack`, which writes out an input file for the `cpack` tool.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(MyProj VERSION 1.0.2)

add_library(MyLib main.cpp a.cpp b.cpp)

target_sources(MyLib
  PUBLIC
  FILE_SET api
  TYPE HEADERS
  BASE_DIRS headers
  FILES
  headers/a.h
  headers/b.h
)

install(TARGETS MyLib FILE_SET api)

set(CPACK_PACKAGE_NAME MyLib)
set(CPACK_PACKAGE_VENDOR MyCompany)
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A library project")
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_PACKAGE_NAME})
set(CPACK_VERBATIM_VARIABLES TRUE)

include(CPack)
```

The following command demonstrates how to package a project:

``` bash
cpack -G "ZIP;WIX"
```

As was the case for installing, multi-configuration generators are also supported for packaging too.

The configuration must be specified as part of the build, install and package steps:

``` bash
cpack -G "ZIP;WIX" --config Release
```

## Pros and cons

???+ success "Pros"
    - [x] Cross-Platform support
    - [x] Wide Adoption
    - [x] Customization

???+ danger "Cons"
    - [ ] Learning curve
    - [ ] Verbose syntax
    - [ ] Documentation gaps