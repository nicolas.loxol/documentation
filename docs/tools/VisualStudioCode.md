---
hide:
  - navigation
---

# **Visual Studio Code** :star::star::star::star::star:
_________________

**[Visual Studio Code](https://code.visualstudio.com/)**, also commonly referred to as `VS Code`, is a source code editor made by `Microsoft` using the `Electron Framework`.

It is a lightweight yet powerful source code editor that runs on desktop and is available for `Windows` and `Linux`.

It boasts a rich ecosystem of extensions for other languages such as `C` and `C++`.

## Installation
_________________

=== "Windows"

    Simply download the installer on the [official website](https://code.visualstudio.com/download)

=== "Unix-like system"

    === ".deb package"

        The easiest way to install **Visual Studio Code** for `Debian/Ubuntu` based distributions is to download the `.deb` package on the [official website](https://code.visualstudio.com/download) and install it, either through the `graphical software center` if it is available, or via the `command line` with:

        ``` bash
        sudo apt install ./<file>.deb
        ```
        Installing the `.deb` package will automatically install the apt repository and signing key to enable auto-updating using the system's package manager.

    === "Command line"

        Alternatively, the repository and key can also be installed manually with the following script:

        ``` bash
        sudo apt install wget gpg
        wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
        sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
        sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
        rm -f packages.microsoft.gpg
        ```

        Then update the package cache and install the package using:

        ```bash
        sudo apt install apt-transport-https
        sudo apt update
        sudo apt install code
        ```

???+ info
    This tutorial will cover **Visual Studio Code** version 1.88.1

## How to use
_________________

### Add C/C++ support

The `C/C++ extension` adds language support for `C/C++` to **Visual Studio Code**, including editing features (`IntelliSense`) and debugging capabilities.

To install this extension:

* Search for the `C++ extension` on the `Extensions` menu
* Launch `VS Code Quick Open` (++ctrl+p++), paste `ext install ms-vscode.cpptools`, and press ++enter++.

![Visual Studio Code C++ extension install](../ressources/img/visual_studio_code_install_cpp_extension.png)

`C++` is a `compiled language`, which means that a program's source code must be translated (compiled) before it can be run on a computer.

**Visual Studio Code** is primarily an editor and relies on command-line tools for much of the development workflow.

The `C/C++ extension` does not include a `C++` `compiler` or `debugger`.

As a result, it is mandatory to install these tools or utilize those already installed on computer and add them to the environment `PATH`.

Now, consider the following `C++` code:

``` cpp title="helloworld.cpp" linenums="1"
#include <iostream>

int main(int, char**)
{
    std::cout << "Hello World" << std::endl;

    return 0;
}
```

And build it.

Select the `Terminal > Run Build Task` command (++ctrl+shift+b++) from the main menu.

![Visual Studio Code C++ compile](../ressources/img/visual_studio_code_compile.png)

This will display a dropdown with various compiler found on the machine.

A compiler should be picked on the list.

![Visual Studio Code C++ compiler](../ressources/img/visual_studio_code_compiler.png)

This will compile `helloworld.cpp` and create an executable file called `helloworld.exe`, which will appear in the `File Explorer`.

![Visual Studio Code C++ executable](../ressources/img/visual_studio_code_executable.png)

From a `command prompt` or a `VS Code Integrated Terminal`, the program can now be run by typing `.\helloworld`.

![Visual Studio Code C++ run executable](../ressources/img/visual_studio_code_run.png)

### IntelliSense

`IntelliSense` is a general term encompassing various code editing features including: code completion, parameter info, quick info, and member lists.

By default, `Visual Studio Code IntelliSense` is provided for `JavaScript`, `TypeScript`, `JSON`, `HTML` and `CSS` languages.

While `VS Code` supports word-based completions for any programming language, enhancing `IntelliSense` further is possible by installing a language extension to support a preferred language

???+ info
    `IntelliSense` for `C/C++` is a part of the [C/C++ extentions](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

`VS Code IntelliSense` features are powered by a language service.

A language service offers intelligent code completions based on language semantics and an analysis of the source code.

If a language service is aware of possible completions, `IntelliSense` suggestions will appear as typing occurs.

As typing continues, the list of members (variables, methods, etc.) is filtered to include only those containing the typed characters.

Pressing ++tab++ or ++enter++ will insert the selected member.

`IntelliSense` can be trigger in any editor window by typing ++ctrl+space++.

???+ info
    `IntelliSense` experience can be customized in settings and key bindings.

    These settings can be adjusted in the `Preferences` tab using the shortcut ++ctrl+comma+plus++

### Run and Debug

To bring up the `Run and Debug` view, select the `Run and Debug` icon in the `Activity Bar` on the side of `VS Code`.

Alternatively, the keyboard shortcut ++ctrl+shift+d++ can be used.

![Visual Studio Code Run and Debug icon](../ressources/img/visual_studio_code_debug_window.png)

The `Run and Debug` view showcases all information related to running and debugging, featuring a top bar with debugging commands and configuration settings.

If `Run and Debug` is not yet configured (no debugger specified), `VS Code` shows the `Run start view`.

![Visual Studio Code Run and Debug Debugger](../ressources/img/visual_studio_code_debugger.png)

The application can be debugged in the usual way, with breakpoints being employed and the variable, watch, and call stack windows being utilized.

![Visual Studio Code Run and Debug Window](../ressources/img/visual_studio_code_debugger_windows.png)

Once a debug session starts, the `Debug toolbar` will appear on the top of the editor.

![Visual Studio Code Run and Debug toolbar](../ressources/img/visual_studio_code_debug_toolbar.png)

Otherwise, here is a list a the basic debug shortcuts:

Action | Explanation |
:------------: | :-------------:
`Continue / Pause` | ++f5++ Continue: Resume normal program/script execution (up to the next breakpoint). Pause: Inspect code executing at the current line and debug line-by-line.
`Step Over` | ++f10++ Execute the next method as a single command without inspecting or following its component steps.
`Step Into` | ++f11++ Enter the next method to follow its execution line-by-line.
`Step Out` | ++shift+f11++ When inside a method or subroutine, return to the earlier execution context by completing remaining lines of the current method as though it were a single command.
`Restart` | ++ctrl+shift+f5++ Terminate the current program execution and start debugging again using the current run configuration.
`Stop` | ++shift+f5++ Terminate the current program execution.

### Using Git source control in VS Code

**Visual Studio Code** integrates source control management (SCM) and includes `Git` support out-of-the-box.

Additionally, many other source control providers are available through extensions on the `VS Code Marketplace`.

???+ warning
    `VS Code` will utilize the machine's `Git` installation (requiring at least version 2.0.0).

These links can be referred to for more information:

* [Git integration](https://code.visualstudio.com/docs/sourcecontrol/overview)
* [Github integration](https://code.visualstudio.com/docs/sourcecontrol/github)

### Extension Marketplace

The features included in **Visual Studio Code** out-of-the-box are just the beginning.

With `VS Code extensions`, languages, debuggers, and tools can be added to the installation to support the development workflow

`VS Code's` extensibility model enables extension authors to plug directly into the `VS Code UI` and contribute functionality through the same APIs used by `VS Code`.

Extensions can be browsed and installed from within `VS Code`.

Simply bring up the `Extensions` view by clicking on the `Extensions` icon in the `Activity Bar` or by using the ++ctrl+shift+x++ command.

![Visual Studio Code extension logo](../ressources/img/visual_studio_code_extension_logo.png)

This will display a list of the most popular `VS Code extensions` on the [VS Code Marketplace](https://marketplace.visualstudio.com/VSCode).

![Visual Studio Code extension window](../ressources/img/visual_studio_code_extension_window.png)

Each `extension` in the list includes a brief description, the publisher, the download count, and a five-star rating.

Selecting the extension item will display the extension's details page, providing an opportunity to learn more.

## Pros and cons
_________________

???+ success "Pros"
    - [x] Huge community
    - [x] Multi-platform
    - [x] A la carte IDE

???+ danger "Cons"
    - [ ] No default debugger
    - [ ] No default compiler

## Extra
_________________

### Keys

A variety of commands and windows in **Visual Studio Code** can be accessed via the appropriate keyboard shortcut.

This part lists the most used default command shortcuts.

Additionally, shortcuts can be customized by assigning different shortcuts to specific commands.

#### Debug: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`Start` | ++f5++
`Start without debugging` | ++ctrl+f5++
`Restart` | ++ctrl+shift+f5++
`Stop debugging` | ++shift+f5++
`Step over` | ++f10++
`Step into` | ++f11++
`Step out` | ++shift+f11++
`Toggle breakpoint` |	++f9++

#### Edit: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`Copy` | ++ctrl+c++
`Cut` | ++ctrl+x++
`Paste` | ++ctrl+v++
`Find` | ++ctrl+f++
`Find in files` | ++ctrl+shift+f++
`Replace` | ++ctrl+h++
`Select all` | ++ctrl+a++
`Comment selection` | ++ctrl+k++ ++ctrl+c++
`Uncomment selection` | ++ctrl+k++ ++ctrl+u++
`Complete word` | ++ctrl+space++
`Go to definition` | ++f12++
`Redo` | ++ctrl+y++
`Undo` | ++ctrl+z++

#### File: popular shortcuts

Commands | Keyboard shortcuts |
:------------: | :-------------:
`New file` | ++ctrl+n++
`Open file` | ++ctrl+o++
`Save selected items` | ++ctrl+s++
`Save all` | `unassigned`
`Exit` | ++alt+f4++

???+ info
    A terminal can be open inside **Visual Studio Code** with ++ctrl+shift+c++

### Useful plugin

Here is a list of useful `Visual Studio Code extension` for `C/C++`:

* [**SonarLint**](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarlint-vscode): A extension that identifies and helps the user to fix Code Quality and gain Code Security issues.

* [**Code Spell Checker**](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker): An editor extension that checks the spelling of comments, strings, and plain text interactively with a tool window.

* [**Trailing Spaces**](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces): Automate detection and deletion of trailing spaces.

* [**CMake**](https://marketplace.visualstudio.com/items?itemName=twxs.cmake): CMake language support for Visual Studio Code

* [**Clang-Format**](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format): A tool to format C/C++ code.

* [**Doxygen Documentation Generator**](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen): A tool that generates automatic Doxygen Documentation generation.

### Create a code snippet

`Code snippets` are templates that facilitate entering repeating code patterns, such as loops or conditional statements.

In **Visual Studio Code**, `snippets` appear in `IntelliSense` (++ctrl+space++) mixed with other suggestions, as well as in a dedicated snippet picker (`Insert Snippet` in the `Command Palette`).

There is also support for `tab-completion`: Enable it with `editor.tabCompletion: on`, type a `snippet prefix`, and press `Tab` to insert a `snippet`.

Many extensions on the `VS Code Marketplace` include snippets.

This is the case of the `C/C++ extension` which include `C/C++ snippets` with `IntelliSense`.

Defining a snippet without any extension is straightforward.

To create or edit `snippets`, select `Configure User Snippets` under `File > Preferences`, and then select the language for which the `snippets` should appear, or the `New Global Snippets file` option if they should appear for all languages.

`VS Code` manages the creation and refreshing of the underlying `snippets files`.

![Visual Studio Code Snippet menu](../ressources/img/visual_studio_code_snippet_menu.png)

`Snippets files` are written in `JSON`, support `C-style comments`, and can define an unlimited number of `snippets`.

Below is an example of a for loop snippet for `C++` using iterator:

``` json title="cpp.json" linenums="1"
{
	"for_loop_iterator":
	{
		"prefix": ["for iterator", "for it"],
		"body": ["for (${1:container_type}::iterator it = ${2:container}.begin(); it != ${2:container}.end(); ++it),
        "{",
        "\t${0:/* code */}",
        "}",
        ],
		"description": "A basic for loop using iterator"
	}
}
```
In the example above:

* "for_loop_iterator" is the `snippet name`. It is displayed via `IntelliSense` if no description is provided.

* `prefix` defines one or more trigger words that display the `snippet` in `IntelliSense`. Substring matching is performed on prefixes, so in this case, "fi" could match "for iterator".

* `body` is one or more lines of content, which will be joined as multiple lines upon insertion. Newlines and embedded tabs will be formatted   according to the context in which the snippet is inserted.

* `description` is an optional description of the snippet displayed by `IntelliSense`.

Additionally, the body of the example above has three placeholders (listed in order of traversal): `${1:container_type}`, `${2:container}`, and `$0`.

The next placeholder can be swiftly accessed with ++tab++, allowing the user to edit it or move on to the subsequent one.

The string after the colon `:` (if any) is the default text, for example container in `${2:container}`.

Placeholder traversal order is ascending by number, starting from one; zero is an optional special case that always comes last, and exits `snippet` mode with the cursor at the specified position.