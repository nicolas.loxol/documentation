---
hide:
  - navigation
---

# **Doxygen** :star::star::star::star:
_________________

**[Doxygen](https://www.doxygen.nl/)** is the de facto standard tool for generating documentation from annotated `C/C++` sources.

It offers several powerful features to streamline and enhance documentation's process.

* It can generate an online or official documentation in various format including amongst other `HTML`, `LaTeX`, `PDF`, from a set of documented source files.

* **Doxygen** can be configured to extract the code structure from undocumented source files.

* **Doxygen** can also visualize the relations between the various elements by means of include dependency graphs, inheritance diagrams, and collaboration diagrams, which are all generated automatically.

* **Doxygen** is not limited to specialized documentation; it can also be used for creating regular, comprehensive documentation for any project.

## Installation
_________________

To install **Doxygen**, run the following command:

=== "Windows"

    Download the latest release on the [official website](https://www.doxygen.nl/download.html)

=== "Unix-like system"

    ``` bash
    sudo apt install doxygen
    ```

???+ info
    This tutorial will cover **Doxygen** version 1.10.0 and `Graphviz` version 11.0.0

## How to use
_________________

### Create a configuration file

**Doxygen** uses a configuration file to determine all of its settings.

Each project should get its own configuration file.

A project can consist of a single source file, but can also be an entire source tree that is recursively scanned.

To simplify the creation of a configuration file, **Doxygen** can create a template configuration file.

To do this call **Doxygen** from the command line:

```bash
doxygen -g
```

This create a `Doxyfile` that contains all of the settings with the default values.

The different settings can be then manually modified on the `Doxyfile`:

``` bash title="Doxyfile" linenums="1"
# The PROJECT_NAME tag is a single word (or a sequence of words surrounded by
# double-quotes, unless you are using Doxywizard) that should identify the
# project for which the documentation is generated. This name is used in the
# title of most generated pages and in a few other places.
# The default value is: My Project.

PROJECT_NAME           = "My Project"

# The INPUT tag is used to specify the files and/or directories that contain
# documented source files. You may enter file names like myfile.cpp or
# directories like /usr/src/myproject. Separate the files or directories with
# spaces. See also FILE_PATTERNS and EXTENSION_MAPPING
# Note: If this tag is empty the current directory is searched.

INPUT                  = src/

# The OUTPUT_DIRECTORY tag is used to specify the (relative or absolute) path
# into which the generated documentation will be written. If a relative path is
# entered, it will be relative to the location where doxygen was started. If
# left blank the current directory will be used.

OUTPUT_DIRECTORY       = doc/

# If the GENERATE_HTML tag is set to YES, doxygen will generate HTML output
# The default value is: YES.

GENERATE_HTML          = YES

# The HTML_OUTPUT tag is used to specify where the HTML docs will be put. If a
# relative path is entered the value of OUTPUT_DIRECTORY will be put in front of
# it.
# The default directory is: html.
# This tag requires that the tag GENERATE_HTML is set to YES.

HTML_OUTPUT            = html

# If the GENERATE_LATEX tag is set to YES, doxygen will generate LaTeX output.
# The default value is: YES.

GENERATE_LATEX         = YES

# The LATEX_OUTPUT tag is used to specify where the LaTeX docs will be put. If a
# relative path is entered the value of OUTPUT_DIRECTORY will be put in front of
# it.
# The default directory is: latex.
# This tag requires that the tag GENERATE_LATEX is set to YES.

LATEX_OUTPUT           = latex

# If you set the HAVE_DOT tag to YES then doxygen will assume the dot tool is
# available from the path. This tool is part of Graphviz (see:
# https://www.graphviz.org/), a graph visualization toolkit from AT&T and Lucent
# Bell Labs. The other options in this section have no effect if this option is
# set to NO
# The default value is: NO.

HAVE_DOT               = NO

.
.
.
```

### Documenting the code

A special comment block is a `C++` style comment block with some additional markings, so **Doxygen** knows it is a piece of structured text that needs to end up in the generated documentation.

``` c
/**
* ... documentation ...
*/
```

Here is a list of widely used **Doxygen** tags:

``` txt
\class to document a class.
\struct to document a C-struct.
\union to document a union.
\enum to document an enumeration type.
\fn to document a function.
\param to document a function parameter.
\return to document the return type or value of a function.
\var to document a variable or typedef or enum value.
\def to document a #define.
\typedef to document a type definition.
\file to document a file.
\namespace to document a namespace.
```

???+ info
    The official documentation listing the different **Doxygen** tags can be found [here](https://www.doxygen.nl/manual/commands.html).

Below is an example of commented `C/C++` source file.

=== "C"

    ``` c title="main.c" linenums="1"
    /**
    * \file main.c
    * \brief Test program
    * \author Author.Name
    * \version 1.0
    * \date september 24, 2023
    */

    #include <stdio.h>

    /**
    * \fn int addition(int a, int b)
    * \brief Addition of two integers
    *
    * \param a The first number to add
    * \param b The second number to add
    * \return The result of the addition a + b
    */
    int addition(int a, int b)
    {
        return a + b;
    }

    /**
    * \fn int main ()
    * \brief Entry point, printf the result of the addition 5 + 3 using the method addition
    *
    * \return 0 - Normal terminaison
    */
    int main()
    {
        printf("%d", addition(5, 3));

        return 0;
    }
    ```

=== "C++"

    ``` c++ title="main.cpp" linenums="1"
    /**
    * \file main.cpp
    * \brief Test program
    * \author Author.Name
    * \version 1.0
    * \date september 24, 2023
    */

    #include <iostream>

    using namespace std;

    /**
    * \fn int addition(int a, int b)
    * \brief Addition of two integers
    *
    * \param a The first number to add
    * \param b The second number to add
    * \return The result of the addition a + b
    */
    int addition(int a, int b)
    {
        return a + b;
    }

    /**
    * \fn int main ()
    * \brief Entry point, printf the result of the addition 5 + 3 using the method addition
    *
    * \return 0 - Normal terminaison
    */
    int main(int, char**)
    {
        std::cout << addition(5, 3) << std::endl;

        return 0;
    }
    ```

### Generate the documentation

The documentation can be generated with the command:

```bash
doxygen Doxyfile
```

Depending on the settings, **Doxygen** will create the various format of documentation inside the output directory.

Below is an example of the generated documentation in `HTML` format.

![Generated HMTL documentation](../ressources/img/doxygen_HTML.png)

### Linking to external documentation

If the project depends on `external libraries` or `tools`, there are several reasons to not include all sources for these with every run of **Doxygen**:

* *Disk space*: Some documentation may be available outside of the output directory of **Doxygen** already, for instance somewhere on the web.
Link to these pages instead of generating the documentation in the local output directory can be preferred.

* *Compilation speed*: `External projects` typically have a different update frequency from the project.
It does not make much sense to let **Doxygen** parse the sources for these external project over and over again, even if nothing has changed.

* *Memory*: For very large source trees, letting **Doxygen** parse all sources may simply take too much of the system's memory.
By dividing the sources into several packages, the sources of one package can be parsed by **Doxygen**, while all other packages that this package depends on, are linked in externally. This saves a lot of memory.

* *Availability*: For some projects that are documented with **Doxygen**, the sources may just not be available.

* *Copyright issues*: If the external package and its documentation are copyright someone else, it may be better or even necessary to reference it rather than include a copy of it with the project's documentation.

If any of the above apply, the `tag file` mechanism can be used.

A `tag file` is basically a compact representation of the entities found in the external sources.

**Doxygen** can both generate and read `tag files`.

To generate a `tag file` for a project, simply put the name of the `tag file` after the `GENERATE_TAGFILE` option in the `configuration file`.

To combine the output of one or more external projects, it is necessary to specify the name of the `tag files` after the `TAGFILES` option in the `configuration file`.

???+ note
    A tag file typically only contains a relative location of the documentation from the point where **Doxygen** was run.

    So when a tag file is included in other project, the external documentation is location need to be specified.

    It can be done on the configuration file by assigning the (relative) location to the tag files specified after the `TAGFILES` configuration option.

    A relative path should be relative with respect to the directory where the output of the project is generated; so a relative path from the output directory of a project to the output of the other project that is linked to.

Here is a basic `Doxyfile` creating its own `Tag file` and using an already defined `Tag file` from an other project

``` bash title="Doxyfile" linenums="1" hl_lines="18 24 51"
#---------------------------------------------------------------------------
# Configuration options related to external references
#---------------------------------------------------------------------------

# The TAGFILES tag can be used to specify one or more tag files. For each tag
# file the location of the external documentation should be added. The format of
# a tag file without this location is as follows:
# TAGFILES = file1 file2 ...
# Adding location for the tag files is done as follows:
# TAGFILES = file1=loc1 "file2 = loc2" ...
# where loc1 and loc2 can be relative or absolute paths or URLs. See the
# section "Linking to external documentation" for more information about the use
# of tag files.
# Note: Each tag file must have a unique name (where the name does NOT include
# the path). If a tag file is not located in the directory in which doxygen is
# run, you must also specify the path to the tagfile here.

TAGFILES               =  ../MyOtherProject/MyOtherProject.tag=../../MyOtherProject/html

# When a file name is specified after GENERATE_TAGFILE, doxygen will create a
# tag file that is based on the input files it reads. See section "Linking to
# external documentation" for more information about the usage of tag files.

GENERATE_TAGFILE       =  MyTag.tag

# If the ALLEXTERNALS tag is set to YES, all external class will be listed in
# the class index. If set to NO, only the inherited external classes will be
# listed.
# The default value is: NO.

ALLEXTERNALS           = NO

# If the EXTERNAL_GROUPS tag is set to YES, all external groups will be listed
# in the modules index. If set to NO, only the current project's groups will be
# listed.
# The default value is: YES.

EXTERNAL_GROUPS        = YES

# If the EXTERNAL_PAGES tag is set to YES, all external pages will be listed in
# the related pages index. If set to NO, only the current project's pages will
# be listed.
# The default value is: YES.

EXTERNAL_PAGES         = YES

# If the REFERENCES_RELATION tag is set to YES then for each documented function
# all documented entities called/used by that function will be listed.
# The default value is: NO.

REFERENCES_RELATION    = YES
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Numerous types of output formats
    - [x] Able to generate graphs and diagrams

???+ danger "Cons"
    - [ ] Lack of new features
    - [ ] Old style rendering

## Extra
_________________

### Doxywizard

[Doxywizard](https://www.doxygen.nl/manual/doxywizard_usage.html) is a GUI front-end for configuring and running **Doxygen**.

It is automatically install with the **Doxygen** package.

Using `Doxywizard` allow to edit graphically some parameters that usually configured on the `doxyfile` (source code directory, destination folder, graphs usage, ...).

![Doxywizard](../ressources/img/doxywizard.png)

### Graphiz

[Graphviz](https://graphviz.org/) is open source graph visualization software.

Graph visualization is a way of representing structural information as diagrams of abstract graphs and networks.

`Graphviz` can be used in addition to **Doxygen** to generate different types of graphs.

To install `Graphviz`, run the following command:

=== "Windows"

    Download the latest release on the [official website](https://graphviz.org/download/)

=== "Unix-like system"

    ``` bash
    sudo apt install graphviz
    ```

By default **Doxygen** sets the `HAVE_DOT` setting to `NO`.

Set the value to `YES` to generate graphs using `Graphviz`.

![Graphviz](../ressources/img/graphviz.svg)

### CMake and Doxygen

The **Doxygen** documentation can be directly generated with the [CMake](https://cmake.org/) build system.

=== "Doxygen"

    ``` cmake title="CMakeLists.txt" linenums="1"
    cmake_minimum_required(VERSION 3.29)

    project(DoxygenDocumentation)

    add_executable(main main.cpp)

    # Find Doxygen
    find_package(Doxygen REQUIRED doxygen)

    if(DOXYGEN_FOUND)
        # Set doxygen configuration information
        set(DOXYGEN_GENERATE_HTML YES)
        set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc")

        # Add a custom target to generate Doxygen documentation
        doxygen_add_docs (
            doxygen
            "${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile"
            COMMENT "Generate HTML pages"
        )
    else()
        message("Doxygen not found. Unable to generate documentation.")
    endif()
    ```

=== "Graphviz"

    ``` cmake title="CMakeLists.txt" linenums="1" hl_lines="8 14"
    cmake_minimum_required(VERSION 3.29)

    project(DoxygenDocumentation)

    add_executable(main main.cpp)

    # Find Doxygen and Graphviz
    find_package(Doxygen REQUIRED doxygen dot)

    if(DOXYGEN_FOUND)
        # Set doxygen configuration information
        set(DOXYGEN_GENERATE_HTML YES)
        set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc")
        set(DOXYGEN_HAVE_DOT YES)

        # Add a custom target to generate Doxygen documentation
        doxygen_add_docs (
            doxygen
            "${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile"
            COMMENT "Generate HTML pages"
        )
    else()
        message("Doxygen not found. Unable to generate documentation.")
    endif()
    ```

???+ info
    For more information, refer to this [link](https://cmake.org/cmake/help/latest/module/FindDoxygen.html)