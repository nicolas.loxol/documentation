---
hide:
  - navigation
---

# **OpenCppCoverage** :star::star::star::star::star:
_________________

**[OpenCppCoverage](https://github.com/OpenCppCoverage/OpenCppCoverage)** is an open source code coverage tool for `C/C++` under `Windows` platform.

The main usage is for unit testing coverage, but it can also be used to know the executed lines in a program for debugging purpose.

## Installation
_________________

To install **OpenCppCoverage**, download the latest available release on the [official github](https://github.com/OpenCppCoverage/OpenCppCoverage/releases).

## How to use
_________________

**OpenCppCoverage** must be called using the command line.

=== "Classic"

    ``` bash
    OpenCppCoverage.exe --sources SourcePath* -- Program.exe
    ```

=== "With args"

    ``` bash
    OpenCppCoverage.exe --sources SourcePath* -- Program.exe arg1 arg2
    ```

???+ info
    This tutorial will cover **OpenCppCoverage** version 0.9.9.0

In release build, the code can be optimized aggressively, from this point, **OpenCppCoverage** should be used with a debug build.

By default `HTML` is the default format or the generated coverage report.

It can be overridden with the value of the `--export_type` option.

![Code coverage graph](../ressources/img/coverage1.png)

![Code coverage code](../ressources/img/coverage2.png)

## Pros and cons
_________________

???+ success "Pros"
    - [x] Recompilation of the application is unnecessary
    - [x] Multiple types of output formats are available
    - [x] Detailed reports are provided
???+ danger "Cons"
    - [ ] Not multi-platform

## Extra
_________________

### Visual Studio extension

**OpenCppCoverage** can be used within `Visual Studio` thank to his [Visual Studio extension](https://marketplace.visualstudio.com/items?itemName=OpenCppCoverage.OpenCppCoveragePlugin) available on the market place.

++right-button++ on the solution and `run open cpp coverage`.

![Code coverage extension](../ressources/img/coverage_extension.png)