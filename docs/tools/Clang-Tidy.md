---
hide:
  - navigation
---

# **Clang-Tidy** :star::star::star::star:
_________________

**[Clang-Tidy](https://clang.llvm.org/extra/clang-tidy/)** is a widely used `static code analysis` tool that is part of the `Clang` compiler infrastructure.

Its purpose is to provide an extensible framework for diagnosing and fixing typical programming errors, like style violations, interface misuse, or bugs that can be deduced via static analysis.

**Clang-tidy** is modular and provides a convenient interface for writing new checks.

## Installation
_________________

The easiest way to install **Clang-Tidy** is by installing `Clang` packages

To install `Clang` and its dependencies, follow these instructions:

=== "Windows"

    Go to the last tag on the official [github page](https://github.com/llvm/llvm-project/releases/) and download `LLVM-X.Y.Z-win64.exe`

=== "Unix-like system"

    ???+ warning
        The `<version number>` should be replace with the version of `Clang` the user want to install

    ``` bash
    wget https://apt.llvm.org/llvm.sh
    chmod +x llvm.sh
    sudo ./llvm.sh <version number>

    sudo apt install clang-<version number> clang-tools-<version number> clang-<version number>-doc libclang-common-<version number>-dev libclang-<version number>-dev libclang1-<version number> clang-format-<version number> python3-clang-<version number> clangd-<version number> clang-tidy-<version number>
    ```

???+ info
    This tutorial will cover `clang 18`

## How to use
_________________

### Command line

The easiest way to work with the **Clang-tidy** utility is to use a `compile command database`.

Otherwise, `compilation options` can be specified on the command line after `--`:

``` cpp
clang-tidy main.cpp -- -Imy_project/include -DMY_DEFINES
```

**Clang-tidy** has its own checks and can also run `Clang Static Analyzer checks`.

Each check has a name and the checks to run can be chosen using the `-checks=` option

The `-list-checks` option lists all the enabled checks.

``` cpp
clang-tidy -list-checks
```

There are currently the following groups of checks:

``` txt
* abseil: Checks related to Abseil library
* altera: Checks related to OpenCL programming for FPGAs
* android: Checks related to Android
* boost: Checks related to Boost library
* bugprone: Checks that target bug:prone code constructs
* cert: Checks related to CERT Secure Coding Guidelines
* clang:analyzer: Clang Static Analyzer checks
* concurrency: Checks related to concurrent programming (including threads, fibers, coroutines, etc)
* cppcoreguidelines: Checks related to C++ Core Guidelines
* darwin: Checks related to Darwin coding conventions
* fuchsia: Checks related to Fuchsia coding conventions
* google: Checks related to Google coding conventions
* hicpp: Checks related to High Integrity C++ Coding Standard
* linuxkernel: Checks related to the Linux Kernel coding conventions
* llvm: Checks related to the LLVM coding conventions
* llvmlibc: Checks related to the LLVM:libc coding standards
* misc: Checks that we did not have a better category for
* modernize: Checks that advocate usage of modern (currently “modern” means “C++11”) language constructs
* mpi: Checks related to MPI (Message Passing Interface)
* objc: Checks related to Objective:C coding conventions
* openmp: Checks related to OpenMP API
* performance: Checks that target performance:related issues
* portability: Checks that target portability:related issues that do not relate to any particular coding style
* readability: Checks that target readability:related issues that do not relate to any particular coding style
* zircon: Checks related to Zircon kernel coding conventions
```

`Clang diagnostics` are treated in a similar way as check diagnostics.

`Clang diagnostics` are displayed by **Clang-tidy** and can be filtered out using the `-checks=` option.

However, the `-checks=` option does not affect `compilation arguments`, so it cannot turn on `Clang warnings` which are not already turned on in the build configuration.

The `-warnings-as-errors=` option upgrades any warnings emitted under the `-checks=` flag to errors (but it does not enable any checks itself).

`Clang diagnostics` have check names starting with `clang-diagnostic-`.

Diagnostics which have a corresponding warning option, are named `clang-diagnostic-<warning-option>`.

For instance, `Clang warning` controlled by `-Wliteral-conversion` will be reported with check name `clang-diagnostic-literal-conversion`.

Lets try the utility on a basic `main.cpp` example:

``` cpp title="main.cpp" linenums="1"
int main(int, char**)
{
	double a = 5.1;
	int* b = nullptr;
	int c = a;
	char d;

    return 0;
}
```

Then, the checks for the `cppcoreguidelines` can be run:

``` bash
clang-tidy main.cpp --checks=-*,cppcoreguidelines* --
```

???+ warning
    The `--checks=-*` options will disable all default checks

``` bash
main.cpp:3:13: warning: 5.1 is a magic number; consider replacing it with a named constant [cppcoreguidelines-avoid-magic-numbers]
        double a = 5.1;
                   ^
main.cpp:5:10: warning: narrowing conversion from 'double' to 'int' [cppcoreguidelines-narrowing-conversions]
        int c = a;
                ^
main.cpp:6:7: warning: variable 'd' is not initialized [cppcoreguidelines-init-variables]
        char d;
             ^
               = 0
```

???+ note
    The full **Clang-Tidy** checks summary list can be found on the [official documentation](https://clang.llvm.org/extra/clang-tidy/checks/list.html)

Some `Clang-Tidy Checks` are able to correct the generated warnings or errors.

* `--fix` flag instructs **Clang-tidy** to apply suggested fixes.

* `--fix-errors` flag instructs **Clang-tidy** to apply suggested fixes even if compilation errors were found.

``` cpp title="main_with_fix.cpp" linenums="1"
int main(int, char**)
{
	double a = 5.1;
	int* b = nullptr;
	int c = a;
	char d = 0;

    return 0;
}
```

The style of the generated fix code can be formatted using the `--format-style=` flag.

It can even take a `.clang-format` to apply a specific coding style.

Creating a set of specific options can be done with a file named `.clang-tidy`:

``` json title=".clang-tidy" linenums="1"
Checks: "-*,cppcoreguidelines-*"
WarningsAsErrors: false
FormatStyle: none
```

The `--config-file=` use a `.clang-tidy` file in order to analyze the code.

``` bash
clang-tidy main.cpp --config-file=".clang-tidy" --
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Cross-platform
    - [x] Large number of checks available

???+ danger "Cons"
    - [ ] Documentation may be unclear

## Extra
_________________

### Visual Studio Code integration

**Clang-tidy** now comes bundled with the `C++` extension.

But if **Clang-tidy** is already installed (and it is on the environment’s path), the `C++` extension will use that one instead.

The `C++` extension can point to any **Clang-tidy** binary by editing the `C++ extension’s Clang Tidy: Path setting`.

To manually run **Clang-tidy**, open the Command Palette (++ctrl+shift+p++) and type `Run Code Analysis`.

![Run Code Analysis](../ressources/img/run-code-analysis.png)

If a `.clang-tidy` configuration file is present in the project directory, the `C++` extension will honor the checks and options defined in that file.

If multiple `.clang-tidy` configuration files are available in the workspace, **Clang-tidy** will use the configuration file closest to the source file by searching up the path in its parent directories.

Alternatively, a **Clang-tidy** configuration can be specified with the `Clang Tidy: Config setting`.

It is also possible to set **Clang-tidy** to run automatically whenever a file is opened or saved.

To turn on automatic code analysis, open the project settings by selecting `Preferences: Open Settings (UI)` from the `Command Palette` (++ctrl+shift+p++).

![Open settings UI](../ressources/img/open-settings-ui.png)

Then search for `code analysis` to find all the **Clang-tidy** settings and set `Clang Tidy: Enabled` to true.

![Enable Clang-tidy](../ressources/img/clang-tidy-enable-setting.png)

**Clang-tidy** is now integrated on `Visual Studio Code`.

![Clang-tidy VSCode](../ressources/img/clang-tidy-vscode.png)

### Visual Studio integration

`Visual Studio Code Analysis` natively supports **Clang-tidy** for `MSBuild`, whether using `Clang` or `MSVC` toolsets.

**Clang-tidy** checks can run as part of background code analysis.

They appear as in-editor warnings, and display in the `Error List`.

**Clang-tidy** is the default analysis tool when using the `LLVM/clang-cl toolset`.

It can be configured when using an `MSVC` toolset to run alongside, or to replace, the standard `Code Analysis experience`.

Using the `clang-cl` toolset make the `Microsoft Code Analysis` unavailable.

**Clang-tidy** can be configured to be run as part of both `Code Analysis` and `build` under the `Code Analysis > General page` in the `Project Properties window`.

![Clang-tidy Visual Studio](../ressources/img/clang-tidy-visual-studio.png)

Options to configure the tool can be found under the **Clang-tidy** submenu.

![Clang-tidy Visual Studio](../ressources/img/clang-tidy-visual-studio-file.png)

**Clang-tidy** is now integrated on `Visual Studio Code Analysis` (`Build > Run Code Analysis on Solution`).

![Clang-tidy Visual Studio](../ressources/img/clang-tidy-visual-studio-example.png)

### CMake integration

`CMake` has direct support for running **Clang-tidy** as part of the build when using one of the `Makefiles` or `Ninja` generators.

It is enabled by setting the `<LANG>_CLANG_TIDY` target property to the path of the **clang-tidy** executable, where `<LANG>` is the language to enable the checks for.

The property can also hold a list, where the first item is the path to the **clang-tidy** executable, and the rest are command-line arguments for that tool.

The `<LANG>_CLANG_TIDY` target property is initialized from the `CMAKE_<LANG>_CLANG_TIDY` variable when a target is created.

#### Command line

=== "C"

    ``` bash
    cmake -S . -B build -DCMAKE_C_CLANG_TIDY="clang-tidy;-config=."
    ```

=== "C++"

    ``` bash
    cmake -S . -B build -DCMAKE_CXX_CLANG_TIDY="clang-tidy;-config=."
    ```

#### Custom target

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(CLANG-TIDY LANGUAGES C CXX)

find_program(CLANG_TIDY_EXECUTABLE NAME clang-tidy REQUIRED)
find_program(RUN_CLANG_TIDY_EXECUTABLE NAME run-clang-tidy REQUIRED)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)
set(CLANG_TIDY_COMMAND ${RUN_CLANG_TIDY_EXECUTABLE} -clang-tidy-binary ${CLANG_TIDY_EXECUTABLE} -p ${CMAKE_CURRENT_BINARY_DIR} -config-file .)

add_custom_target(clang-tidy
  COMMAND ${CLANG_TIDY_COMMAND}
)
```

``` bash
cmake --build build --target clang-tidy
```