---
hide:
  - navigation
---

# **vcpkg** :star::star::star::star:
_________________

**[vcpkg](https://vcpkg.io/en/index.html)** is a free `C/C++ package manager` maintained by the `Microsoft C++ team` and open source contributors for acquiring and managing libraries.

## Installation
_________________

Installing **vcpkg** is a two-step process.

First, clone the official repository, then run the `bootstrapping script` to produce the **vcpkg** binary.

=== "Windows"

    ``` bash
    git clone https://github.com/Microsoft/vcpkg.git
    ./vcpkg/bootstrap-vcpkg.bat
    ```

=== "Unix-like system"

    ``` bash
    git clone https://github.com/Microsoft/vcpkg.git
    .\vcpkg\bootstrap-vcpkg.sh
    ```

    ???+ warning
        The following packets are required to use **vcpkg**

        ``` bash
        sudo apt install curl zip unzip tar pkg-config
        ```

???+ info
    This tutorial will cover **vcpkg** tag 2024.04.26

## How to use
_________________

This section will utilize the **[SQLite](https://sqlite.org/index.html)** and **[fmt](https://github.com/fmtlib/fmt)** libraries.

### Install libraries for a project

First, search for the `SQLite` library on the packages center.

``` bash
vcpkg search sqlite
```

A library named `sqlite3` corresponding to `SQLite` is found on the list.

``` txt
sqlite3 3.42.0 SQLite is a software library that implements a self-contained, serverless,...
```

???+ info
    The `search` command can be run without arguments to see the full list of packages

Installing is then as simple as using the `install` command.

``` bash
vcpkg install sqlite3
```

???+ info
    === "Windows"

        By default **vcpkg** will install the `x86-windows`.

        To install the `x64` version use:

        ``` bash
        vcpkg install sqlite3:x64-windows
        ```

    === "Unix-like system"

        By default **vcpkg** will install the `x64-linux`.

        To install the `x86` version use:

        ``` bash
        vcpkg install sqlite3:x86-linux
        ```

    The `VCPKG_DEFAULT_TRIPLET` environment variable can be set to get a default architecture-platform behavior.


The installed packaged can be listed using the list command.

``` bash
vcpkg list
```

### Using vcpkg with MSBuild / Visual Studio (Windows)

The recommended and most productive way to use **vcpkg** is via user-wide integration, making the system available for all projects.

The user-wide integration will prompt for administrator access the first time it is used on a given machine, but afterwards is no longer required and the integration is configured on a per-user basis.

``` bash
vcpkg integrate install
```

`SQLite` is now available for `Visual Studio` project.

???+ warning
    `Visual Studio` need to be restarted to update `Intellisense` with the changes.

To remove the integration use:

``` bash
vcpkg integrate remove
```

### Using vcpkg with CMake

First, create a manifest file name `vcpkg.json` in the project's directory by running the `vcpkg new` command:

``` bash
vcpkg new --application
```

Next add the `fmt` library to the dependencies:

``` bash
vcpkg add port fmt
```

The manifest file should resemble this:

``` json title="vcpkg.json" linenums="1"
{
    "dependencies": [
        "fmt"
    ]
}
```

**vcpkg** reads the manifest file to learn what dependencies to install and integrates with `CMake` to provide the dependencies required by the project.

Here is the `CMakeList.txt` of the application:

``` cmake title="CMakeLists.txt" linenums="1" hl_lines="5 9"
cmake_minimum_required(VERSION 3.29)

project(Example)

find_package(fmt CONFIG REQUIRED)

add_executable(Exe main.cpp)

target_link_libraries(Exe PRIVATE fmt::fmt)
```

And the source code:

``` cpp title="main.cpp" linenums="1" hl_lines="1 5"
#include <fmt/core.h>

int main()
{
    fmt::print("Hello World!\n");

    return 0;
}
```

To allow the `CMake` project system to recognize `C/C++` libraries provided by **vcpkg**, it is needed to provide the `vcpkg.cmake` toolchain file.

``` bash
cmake -S . -B build "-DCMAKE_TOOLCHAIN_FILE=[path to vcpkg]/scripts/buildsystems/vcpkg.cmake"
```

``` json title="CMakePresets.json" linenums="1" hl_lines="11"
{
  "version": 8,
  "configurePresets":
  [
    {
      "name": "default",
      "generator": "Ninja",
      "binaryDir": "${sourceDir}/build",
      "cacheVariables":
      {
        "CMAKE_TOOLCHAIN_FILE": "$env{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake"
      }
    }
  ]
}
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Easy to set up
    - [x] Lots of libraries
    - [x] Multi-platform
???+ danger "Cons"
    - [ ] Specific configuration library are missing

## Extra
_________________

### Tab-Completion / Auto-Completion

**vcpkg** supports `auto-completion` of commands, package names, and options in both `powershell` and `bash`.

To enable `tab-completion`, run:

=== "Windows"

    ``` bash
    vcpkg integrate powershell
    ```

=== "Unix-like system"

    ``` bash
    vcpkg integrate bash # or zsh ...
    ```