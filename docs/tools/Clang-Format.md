---
hide:
  - navigation
---

# **Clang-Format** :star::star::star::star:
_________________

**[ClangFormat](https://clang.llvm.org/docs/ClangFormat.html)** describes a set of tools that are built on top of **[LibFormat](https://clang.llvm.org/docs/LibFormat.html)**.

It can support different workflows in a variety of ways including a `standalone tool` and `editor integrations`.

**Clang-format** can be used to format `C/C++` code.

## Installation
_________________

The easiest way to install **Clang-Format** is by installing `Clang` packages

To install `Clang` and its dependencies, follow these instructions:

=== "Windows"

    Go to the last tag on the official [github page](https://github.com/llvm/llvm-project/releases/) and download `LLVM-X.Y.Z-win64.exe`

=== "Unix-like system"

    ???+ warning
        The `<version number>` should be replace with the version of `Clang` the user want to install

    ``` bash
    wget https://apt.llvm.org/llvm.sh
    chmod +x llvm.sh
    sudo ./llvm.sh <version number>

    sudo apt install clang-<version number> clang-tools-<version number> clang-<version number>-doc libclang-common-<version number>-dev libclang-<version number>-dev libclang1-<version number> clang-format-<version number> python3-clang-<version number> clangd-<version number> clang-tidy-<version number>
    ```

???+ info
    This tutorial will cover `clang 20`

## How to use
_________________

### Command line

The **Clang-format** utility is called from the command line:

``` bash
clang-format [options] [<file> ...]
```

* `options`: flags
* `file`: file to format

The `--style=<string>` option set the coding style.

`<string>` can be:

* A `preset`:

    * [LLVM](https://llvm.org/docs/CodingStandards.html)
    * [GNU](https://www.gnu.org/prep/standards/standards.html)
    * [Google](https://google.github.io/styleguide/)
    * [Chromium](https://chromium.googlesource.com/chromium/src/+/refs/heads/main/styleguide/styleguide.md)
    * [Microsoft](https://docs.microsoft.com/en-us/visualstudio/ide/editorconfig-code-style-settings-reference)
    * [Mozilla](https://firefox-source-docs.mozilla.org/code-quality/coding-style/index.html)
    * [WebKit](https://webkit.org/code-style-guidelines/)

* A `file` to load style configuration from a `.clang-format file` in one of the parent directories of the source file

* A `file:<format_file_path>` to explicitly specify the configuration file.

* `"{key: value, ...}"` to set specific parameters, e.g.: `--style="{BasedOnStyle: llvm, IndentWidth: 8}"`

The option `--Werror` changes the formatting warnings to errors.

The `-i` option stands for "in-place" editing, this means that when applied, it modifies the file direcly, instead of just displaying the formatted output in the terminal.

``` bash
clang-format -style=llvm -i main.cpp
```

When the desired code formatting style is different from the available options, the style can be customized using the `-style="{key: value, ...}"` option or by putting the style configuration in the `.clang-format` in the project’s directory and using clang-format `-style=file`.

An easy way to create the `.clang-format` file is:

``` bash
clang-format -style=llvm -dump-config > .clang-format
```

The full option summary list can be found on the [official documentation](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)

Here is an example of a basic `.clang-format`

``` bash title=".clang-format" linenums="1"
UseTab: Never
IndentWidth: 4
BreakBeforeBraces: Allman
AllowShortIfStatementsOnASingleLine: false
IndentCaseLabels: false
ColumnLimit: 0
```

## Pros and cons
_________________

???+ success "Pros"
    - [x] Cross-platform
    - [x] Large number of formatting rules

???+ danger "Cons"
    - [ ] Documentation may be unclear

## Extra
_________________

### Visual Studio Code integration

**Clang-format** can be used in `Visual Studio Code` using the [Clang-format extension](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format).

To manually run **Clang-format**, open the Command Palette (++ctrl+shift+p++) and type `Format Document`.

![Run Format Document](../ressources/img/clang-format-vscode.png)

If a `.clang-format` configuration file is present in the project directory, the extension will use the options specified in that file.

Alternatively, a **Clang-format** configuration can be specified with the `Clang Format: Config setting`.

**Clang-format** runs automatically whenever a file is saved.

### Visual Studio integration

`Visual Studio` natively supports **Clang-format** for formatting document.

There is only one rule, the `.clang-format` file need to be next to the source on the project.

**Clang-format** settings can be specified on the `Formatting options page`.

To access this page, in the Options dialog box, in the left pane, expand `Text Editor`, expand `C/C++`, and then `click Formatting`.

Format document can be run manually on document (++ctrl+k++, ++ctrl+d++) or selection (++ctrl+k++, ++ctrl+f++).

To enable automatic formatting on save, the [Format document on Save](https://marketplace.visualstudio.com/items?itemName=mynkow.FormatdocumentonSave) is needed.

### CMake integration

`CMake` has no direct support for running **Clang-format**.

Nevertheless, a custom target can be created to format the code.

#### Custom target

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(CLANG-FORMAT LANGUAGES C CXX)

find_program(CLANG_FORMAT_EXECUTABLE NAME clang-format REQUIRED)

set(CLANG_FORMAT_COMMAND ${CLANG_FORMAT_EXECUTABLE} -style=file ${SOURCE_FILES} ${HEADER_FILES})

add_custom_target(clang-format
  COMMAND ${CLANG_FORMAT_COMMAND}
)
```

``` bash
cmake --build build --target clang-format
```