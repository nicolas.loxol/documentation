---
hide:
  - navigation
  - toc
---

# **Machine configuration**

The documentation of the different tools was conducted on two distinct **operating systems**:

* `Windows 10 Professional version 22H2`
* `Ubuntu-24.04 running on a virtual machine`

<br>

???+ info
    The `LTS (Long-Term Support)` versions of **Ubuntu** can be downloaded on the [Ubuntu official website](https://ubuntu.com/download)

???+ tip
    It is strongly recommend updating both the tools and the operating system to the latest `LTS` version for optimal performance and security.