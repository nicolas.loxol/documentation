---
hide:
  - navigation
  - toc
---

# **C/C++**

Here is the complete list of the **C/C++ tutorials** cover in the documentation:

## Libraries
_________________

* [Static library](C++/StaticLibrary.md) :material-microsoft-windows: :simple-linux:
* [Shared library](C++/SharedLibrary.md) :material-microsoft-windows: :simple-linux:
* [Plugin library](C++/PluginLibrary.md) :material-microsoft-windows: :simple-linux:
* [Header-only library](C++/HeaderOnlyLibrary.md) :material-microsoft-windows: :simple-linux:
* [Libraries Interoperability](C++/Interoperability.md) :material-microsoft-windows: :simple-linux:

## Concepts
_________________

* [Error Handling](C++/ErrorHandling.md) :material-microsoft-windows: :simple-linux:
* [String Library](C++/StringsLibrary.md) :material-microsoft-windows: :simple-linux:
* [Regular Expression Library](C++/RegularExpressionsLibrary.md) :material-microsoft-windows: :simple-linux:

## Resources
_________________

* [A list of open source C libraries](https://en.cppreference.com/w/c/links/libs) :material-microsoft-windows: :simple-linux:
* [A list of open source C++ libraries](https://en.cppreference.com/w/cpp/links/libs) :material-microsoft-windows: :simple-linux: