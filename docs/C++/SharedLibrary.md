---
hide:
  - navigation
---

# **Shared library**
_________________

A **shared library** or dynamic library contains code designed to be shared by multiple programs.

The content in the library is loaded into memory at runtime.

Each executable does not maintain its own copy of the library.

A **shared library** on `Unix-like` systems is called a `dynamically linked shared object`, and has the file extension `.so`.

The `Windows` equivalent is the `dynamic link library`, usually with file extension `.dll`.

## Basic C/C++ code
_________________

Subsequently, a **shared library** composed of a header file `addition.h` and a source file `addition.c` will be built.

The library will then be used in a `main.c` file.

The codebase is defined below:

```bash
.
├── addition.c
├── addition.h
├── main.c
└── CMakeLists.txt

0 directories, 4 files
```

``` c title="addition.h" linenums="1"
int addition(int x, int y);
```

``` c title="addition.c" linenums="1"
#include "addition.h"

int addition(int x, int y)
{
    return x + y;
}
```

``` c title="main.c" linenums="1"
#include "addition.h"

int main(int, char**)
{
    int res = addition(5, 3);

    return 0;
}
```

## Creation of a static library

_________________

### Compilation and assemblage

First, the library code needs to be compiled with specific options:

=== "MSVC"

    ``` bash
    cl /LD /Fe:Addition.dll addition.c
    ```

    * `LD` : Creates a `.dll`
    * `/Fe` : Specify a name for the `.dll`

    In addition to creating the `.dll`, this process will generate the `import library (.lib)` and the `export file (.exp)` required to use the `.dll` from another executable.

=== "Unix-like"

    ``` bash
    gcc addition.c -o libAddition.so -fPIC -shared
    ```

    The command above compiles `addition.c` to a shared library (`-shared`) with position independent code ([`-fPIC`](https://en.wikipedia.org/wiki/Position-independent_code)).

    ???+ info
        In `Unix-like` systems, **static libraries** conventionally use the `lib` prefix in their filenames.

        This prefix distinguishes them from other files and indicates their purpose.

### Symbols export

Here is a quick look at the `external symbols` of the library.

=== "Windows"

    By default, the `Microsoft Visual Studio Compiler` does not export any symbols.

    Symbols are exported by explicitly declaring them with `__declspec(dllexport)`.

    A common approach is to use an export file`:

    ``` c title="addition_EXPORTS.h" linenums="1"
    #ifndef _ADDITION_EXPORTS_H__
    #define _ADDITION_EXPORTS_H__

    #ifdef _WIN32
        #ifdef ADDITION_EXPORTS
            #define ADDITION_EXPORT __declspec(dllexport)
        #else
            #define ADDITION_EXPORT __declspec(dllimport)
        #endif
    #else
        #define ADDITION_EXPORT
    #endif

    #endif /* _ADDITION_EXPORTS_H__ */
    ```

    When utilizing the library, if `ADDITION_EXPORTS` is not defined, it is automatically set to __declspec(dllimport).

    This signifies that during the library's build, functions are exported, and when employing the library, functions are imported from the `DLL`.

    Ensure that export symbols are also specified in the source code:

    ``` c title="addition.h" linenums="1"
    #include "addition_EXPORTS.h"

    int ADDITION_EXPORT addition(int a, int b);
    ```

    Symbols in DLL can be inspected using the dumpbin utility.

    ``` bash
    dumpbin /EXPORTS Addition.dll
    ```

    The dump result displays the mangled name of the `addition` function.

    ``` bash
    Dump of file .\Addition.dll

    File Type: DLL

    Section contains the following exports for Addition.dll

        00000000 characteristics
        FFFFFFFF time date stamp
            0.00 version
            1 ordinal base
            1 number of functions
            1 number of names

        ordinal hint RVA      name

            1    0 000010FA ?addition@@YAHHH@Z = @ILT+245(?addition@@YAHHH@Z)
    ```

=== "Unix-like"

    ???+ info
        On `Unix-like` systems, all symbols are exported by default.

        However, symbol export can be controlled with the [`-fvisibility`](https://gcc.gnu.org/onlinedocs/gcc/Code-Gen-Options.html) flag.

    Symbols in `Shared object` can be inspected using the `nm` utility.

    ``` bash
    nm -gC libAddition.so
    ```

    The `-g` option searches for external symbols.

    With `-C`, low-level symbol names are translated to human-readable symbols.

    The dump result displays the mangled name of the `addition` function.

    ``` bash
    00000000000011aa T addition(int, int)
    ```

### Linking to a shared library

The **shared library** previously created can be specified in the `main` program during the linking phase.

=== "Windows"

    ``` bash
    cl main.c /link Addition.lib
    ```

    The above command compiles the `main.c` file and links it with the `Addition.lib` shared library during the compilation process.

=== "Unix-like"

    ``` bash
    gcc main.c -o main -L. -lAddition
    ```

    The path and name of the **static library** are manually specified using the `-L` and `-l` options, respectively.

    Before executing the program, `ldd` can be used to check which libraries are linked with the executable

    ``` bash
    ldd main
    ```

    As expected, `libAddition.so` is among the linked libraries, but it cannot be found.

    ``` bash
    libAddition.so => not found
    ```

    An error is displayed when attempting to execute the program.

    ``` bash
    ./main: error while loading shared libraries: libAddition.so: cannot open shared object file: No such file or directory
    ```

    To solve this problem, it is necessary to somehow specify where the library can be found.

    The environment variable `LD_LIBRARY_PATH` can be used to specify directories where libraries are searched.

    ``` bash
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
    ```

    ???+ info
        The `-rpath` flag can also be used to specify the shared library path when building the executable.

        In computing, `rpath` designates the run-time search path hard-coded in an executable file or library.

        Dynamic linking loaders use the `rpath` to find required libraries.

        ``` bash
        gcc main.c -o main -L. -lAddition -Wl,-rpath,.
        ```

## Extra

_________________

### CMake

The following `CMakeLists.txt` defines a project named `DYNAMIC_LIBRARY`, creating a **shared library** named `Addition` from addition source files, and links it to an executable named `main` compiled from `main.c`.

It also creates the `ADDITION_EXPORTS` compile definition, allowing the symbols to be exported.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(DYNAMIC_LIBRARY LANGUAGES C CXX)

add_library(Addition SHARED addition.c addition.h addition_EXPORTS.h)

add_executable(main main.c)

target_compile_definitions(Addition PRIVATE "ADDITION_EXPORTS")

target_link_libraries(main PRIVATE Addition)
```

### Portable export file

``` c title="addition_EXPORTS.h"
#ifndef ADDITION_EXPORT_H
#define ADDITION_EXPORT_H

#ifdef ADDITION_STATIC_DEFINE
    #define ADDITION_EXPORT
    #define ADDITION_NO_EXPORT
#else
    #ifndef ADDITION_EXPORT
        #ifdef Addition_EXPORTS
            #ifdef _WIN32
                /* We are building this library */
                #define ADDITION_EXPORT __declspec(dllexport)
            #else //__linux__
                #define ADDITION_EXPORT __attribute__((visibility("default")))
            #endif
        #else
            #ifdef _WIN32
                /* We are using this library */
                #define ADDITION_EXPORT __declspec(dllimport)
            #else //__linux__
                #define ADDITION_EXPORT __attribute__((visibility("default")))
            #endif
        #endif
    #endif
    #ifndef ADDITION_NO_EXPORT
        #ifdef _WIN32
            #define ADDITION_NO_EXPORT
        #else //__linux__
            #define ADDITION_NO_EXPORT __attribute__((visibility("hidden")))
        #endif
    #endif
#endif

#ifndef ADDITION_DEPRECATED
    #ifdef _WIN32
        #define ADDITION_DEPRECATED __declspec(deprecated)
    #else //__linux__
        #define ADDITION_DEPRECATED __attribute__ ((__deprecated__))
    #endif
#endif

#ifndef ADDITION_DEPRECATED_EXPORT
    #define ADDITION_DEPRECATED_EXPORT ADDITION_EXPORT ADDITION_DEPRECATED
#endif

#ifndef ADDITION_DEPRECATED_NO_EXPORT
    #define ADDITION_DEPRECATED_NO_EXPORT ADDITION_NO_EXPORT ADDITION_DEPRECATED
#endif

#endif /* ADDITION_EXPORT_H */
```