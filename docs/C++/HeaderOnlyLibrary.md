---
hide:
  - navigation
---

# **Header-Only library**
_________________

A **header-only** library is a complete `C/C++` library that is implemented solely using header files.

Here are the main benefits and drawbacks of **header-only libraries**:

???+ success "Benefits"
    - [x] Simplifies the build process as there is no need to build separately.
    - [x] Provides a single version, eliminating the need for build settings or options.
    - [x] Easy to include in the project without additional configuration.

???+ danger "Drawbacks"
    - [ ] Longer compilation times for consumers due to the inclusion of entire library code in every translation unit.
    - [ ] Results in larger object files, which can impact compilation and linking times.
    - [ ] Code can be harder to read and maintain due to the lack of separation between interface and implementation.

## Basic C/C++ code
_________________

Subsequently, a **header-only library** composed of a single header file `addition.h` will be utilized.

The library will then be used on a `main.c` file.

The codebase is defined below:

```bash
.
├── CMakeLists.txt
├── main.c
└── Addition
    └── include
        └── addition.h

2 directories, 3 files
```

``` c title="addition.h" linenums="1"
#pragma once

namespace maths
{
    inline int addition(int x, int y)
    {
        return x + y;
    }
} // namespace maths
```

``` c title="main.c" linenums="1"
#include "addition.h"

int main(int, char**)
{
    int res = maths::addition(5, 3);

    return 0;
}
```

## CMake
_________________

The following `CMakeLists.txt` sets up a project named `HEADER_ONLY_LIBRARY`, defining a **header-only** library named `addition` with `include directories` pointing to `${CMAKE_CURRENT_SOURCE_DIR}/Addition/include`, and links it to an executable named `main` compiled from `main.c`.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(HEADER_ONLY_LIBRARY LANGUAGES C CXX)

add_library(Addition INTERFACE)

target_include_directories(Addition INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/Addition/include)

add_executable(main main.c)

target_link_libraries(main PRIVATE Addition)
```

???+ info
    An `INTERFACE` library target does not compile sources and does not produce a library artifact on disk.