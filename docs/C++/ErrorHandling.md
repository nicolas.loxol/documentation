---
hide:
  - navigation
---

# **Error handling**

## Exception handling

**Error handling** in `C++` is an essential aspect of writing robust and reliable software.

`C++` provides a structured way to detect and manage errors through its exception handling mechanism.

By using `try-catch` blocks and throwing exceptions, developers can separate error detection from error handling, making code more readable and maintainable.

### std::exception

`std::exception` is the base class for all standard exception types in `C++`.

It provides a consistent interface for handling errors and unexpected conditions within a program.

The `std::exception` class includes the `what()` member function, which returns a descriptive C-string of the error, allowing developers to diagnose issues more effectively.

Here is the `std::exception` basic implementation since `C++11`

``` cpp linenums="1" hl_lines="4 8"
exception() noexcept;
exception(const exception& other) noexcept;

virtual ~exception();

exception& operator=(const exception& other) noexcept;

virtual const char* what() const noexcept;
```

???+ info
    The `noexcept` specifier indicates that a function does not throw any exceptions, enabling potential optimizations and improving code safety.

    Conversely, a `throw(std::bad_alloc)` declaration specifies that a function might throw a `std::bad_alloc` exception.

    ``` cpp
    void myMethod() noexcept;
    void myMethod1() throw(std::bad_alloc);
    ```

All custom exception classes should inherit from `std::exception` to ensure they integrate seamlessly with the standard exception-handling mechanisms, promoting robust and maintainable error management throughout the application.

``` cpp title="MyException.hpp" linenums="1" hl_lines="13 17"
#pragma once

#include <exception>
#include <string>

class MyException : public std::exception
{
	public:
		MyException() noexcept;
		MyException(const int pCode, const std::string& pMessage) noexcept;
		MyException(const MyException& pOther) noexcept;

		virtual ~MyException() override;

		MyException& operator=(const MyException& pOther) noexcept;

		virtual const char* what() const noexcept override;

	private:
		int mCode = 0;
		std::string mMessage;
};
```

``` cpp title="MyException.cpp" linenums="1"
#include "MyException.hpp"

MyException::MyException() noexcept
{

}

MyException::MyException(const int pCode, const std::string& pMessage) noexcept : mCode(pCode), mMessage(pMessage)
{

}

MyException::MyException(const MyException& pOther) noexcept : mCode(pOther.mCode), mMessage(pOther.mMessage)
{

}

MyException::~MyException()
{

}

MyException& MyException::operator=(const MyException& pOther) noexcept
{
    if (this != &pOther)
    {
        mCode = pOther.mCode;
        mMessage = pOther.mMessage;
    }

    return *this;
}

const char* MyException::what() const noexcept
{
    return mMessage.c_str();
}
```

``` cpp title="main.cpp" linenums="1" hl_lines="7 9 11"
#include "MyException.hpp"

int main()
{
    try
    {
        throw MyException(2, "Illegal operation");
    }
    catch (const MyException& lEx)
    {
        std::cerr << lEx.what() << std::endl;
    }
    catch (const std::exception& lEx)
    {
        //...
    }
    catch (...)
    {
        //...
    }

    return 0;
}
```

## Exception categories

Derived classes, such as `std::runtime_error` and `std::logic_error`, represent specific categories of exceptions, enabling more granular control over error handling.

Below is the complete list of the standard exceptions:

* [logic_error](https://en.cppreference.com/w/cpp/error/logic_error)
    * [invalid_argument](https://en.cppreference.com/w/cpp/error/invalid_argument)
    * [domain_error](https://en.cppreference.com/w/cpp/error/domain_error)
    * [length_error](https://en.cppreference.com/w/cpp/error/length_error)
    * [out_of_range](https://en.cppreference.com/w/cpp/error/out_of_range)
    * [future_error](https://en.cppreference.com/w/cpp/thread/future_error) (since `C++11`)
* [runtime_error](https://en.cppreference.com/w/cpp/error/runtime_error)
* [range_error](https://en.cppreference.com/w/cpp/error/range_error)
    * [overflow_error](https://en.cppreference.com/w/cpp/error/overflow_error)
    * [underflow_error](https://en.cppreference.com/w/cpp/error/underflow_error)
    * [regex_error](https://en.cppreference.com/w/cpp/regex/regex_error) (since `C++11`)
    * [system_error](https://en.cppreference.com/w/cpp/error/system_error) (since `C++11`)
        * [ios_base::failure](https://en.cppreference.com/w/cpp/io/ios_base/failure) (since `C++11`)
        * [filesystem::filesystem_error](https://en.cppreference.com/w/cpp/filesystem/filesystem_error) (since `C++17`)
    * [nonexistent_local_time](https://en.cppreference.com/w/cpp/chrono/nonexistent_local_time) (since `C++20`)
    * [ambiguous_local_time](https://en.cppreference.com/w/cpp/chrono/ambiguous_local_time) (since `C++20`)
    * [format_error](https://en.cppreference.com/w/cpp/utility/format/format_error) (since `C++20`)
* [bad_typeid](https://en.cppreference.com/w/cpp/types/bad_typeid)
* [bad_cast](https://en.cppreference.com/w/cpp/types/bad_cast)
    * [bad_any_cast](https://en.cppreference.com/w/cpp/utility/any/bad_any_cast) (since `C++17`)
* [bad_optional_access](https://en.cppreference.com/w/cpp/utility/optional/bad_optional_access) (since `C++17`)
* [bad_expected_access](https://en.cppreference.com/w/cpp/utility/expected/bad_expected_access) (since `C++23`)
* [bad_weak_ptr](https://en.cppreference.com/w/cpp/memory/bad_weak_ptr) (since `C++11`)
* [bad_function_call](https://en.cppreference.com/w/cpp/utility/functional/bad_function_call) (since `C++11`)
* [bad_alloc](https://en.cppreference.com/w/cpp/memory/new/bad_alloc)
    * [bad_array_new_length](https://en.cppreference.com/w/cpp/memory/new/bad_array_new_length) (since `C++11`)
* [bad_exception](https://en.cppreference.com/w/cpp/error/bad_exception)
* [bad_variant_access](https://en.cppreference.com/w/cpp/utility/variant/bad_variant_access) (since `C++17`)

## Program termination

In `C++`, managing program termination and ensuring proper cleanup are essential aspects of robust application development.

The `C++ Standard Library` provides a suite of functions designed to handle different termination scenarios, from normal exits to handling unexpected failures.

The following functions offer developers the tools needed to control the termination process, register cleanup routines, handle abnormal terminations, and customize the behavior during unexpected exceptions.

### std::exit

The [std::exit](https://en.cppreference.com/w/cpp/utility/program/exit) function terminates the calling process after performing cleanup tasks.

Specifically, it calls the destructors for all static and thread-local objects and invokes any functions registered with `std::atexit`.

This function allows for a controlled shutdown of the program, ensuring that resources are properly released and any necessary finalization is performed.

It accepts an integer argument, `exit_code`, which is returned to the host environment to indicate the termination status.

???+ info
    Returning from the main function, either by a return statement or by reaching the end of the function performs the normal function termination (calls the destructors of the variables with automatic storage durations) and then executes `std::exit`, passing the argument of the return statement (or ​0​ if implicit return was used) as `exit_code`.

### std::atexit

The [std::atexit](https://en.cppreference.com/w/cpp/utility/program/atexit) function registers a function to be called upon normal program termination.

This allows for custom cleanup code to be executed before the program exits, such as releasing resources, saving state, or logging information.

Up to 32 functions can be registered, and they will be called in the reverse order of their registration.

Functions registered with `std::atexit` are not called if the program terminates via `std::abort` or an unhandled exception.

### std::abort

The [std::abort](https://en.cppreference.com/w/cpp/utility/program/abort) function causes the program to terminate immediately and abnormally.

It does not perform any cleanup, such as calling destructors for static or thread-local objects, or invoking functions registered with `std::atexit`.

Instead, it generates a `SIGABRT` signal, which typically results in the program producing a core dump for debugging purposes.

This function is useful in situations where continuing execution is unsafe or undesirable.

### std::terminate

The [std::terminate](https://en.cppreference.com/w/cpp/error/terminate) function is called when the `C++` runtime detects an unhandled exception or when the `noexcept` specification is violated.

It leads to immediate program termination without any stack unwinding or cleanup.

The default behavior of `std::terminate` is to call `std::abort`, but this can be customized by setting a different terminate handler using `std::set_terminate`.

This function ensures that the program halts in a controlled manner when exception handling fails.

### std::set_terminate

The [std::set_terminate](https://en.cppreference.com/w/cpp/error/set_terminate) function allows the user to set a custom termination handler that will be called by `std::terminate`.

This handler can be used to define specific actions to be taken when an unhandled exception occurs or when `noexcept` is violated, such as logging error information, performing diagnostics, or attempting a controlled shutdown.

The function returns the previously set terminate handler, allowing for the restoration of the default behavior if needed.

## Assertions

The [`assert`](https://en.cppreference.com/w/cpp/error/assert) macro in `C++` is used for debugging purposes to verify assumptions made by the program.

When an assert condition evaluates to false, the program displays an error message and invokes `std::abort` to terminate immediately.

``` cpp linenums="1"
#include <cassert>

assert(2 + 2 == 4);
assert(2 + 2 == 5, "Failed"); // assertion fails
```

???+ info
    [`static_assert`](https://en.cppreference.com/w/cpp/language/static_assert) is a compile-time assertion in `C++` that checks conditions during compilation rather than at runtime.

    It is used to enforce constraints and validate assumptions within the code, ensuring they are met before the program is even run.

    If the condition in a static_assert statement evaluates to false, the compiler generates an error, stopping the compilation process and providing a custom error message.

## Stacktrace

Since `C++23`, the `C++ Standard Library` introduces [`std::basic_stacktrace`](https://en.cppreference.com/w/cpp/utility/basic_stacktrace) and [`std::stacktrace_entry`](https://en.cppreference.com/w/cpp/utility/stacktrace_entry) to facilitate debugging and error diagnosis by capturing and representing stack traces.

`std::basic_stacktrace` is a container that holds a sequence of `std::stacktrace_entry` objects, each representing a single frame in the stack trace.

This allows developers to inspect the call stack at the point where an error occurred or an exception was thrown.

`std::stacktrace_entry` provides detailed information about each stack frame, such as the function name, source file, and line number, enabling precise identification of the code location involved in the issue.

``` cpp title="main.cpp" linenums="1" hl_lines="1 14"
#include <stacktrace>

#include "MyException.hpp"

int main()
{
    try
    {
        // ...
    }
    catch (const MyException& lEx)
    {
        std::cerr << lEx.what() << std::endl;
        std::cerr << std::to_string(std::stacktrace::current()) << std::endl;
    }
    catch (const std::exception& lEx)
    {
        //...
    }
    catch (...)
    {
        //...
    }

    return 0;
}
```