---
hide:
  - navigation
---

# **Plugin library**
_________________

A **plugin library** is a collection of `dynamically loaded modules`, typically in the form of `dynamic-link libraries (DLL)` on `Windows` or `shared objects (SO)` on `Unix-like` systems.

These modules, known as plugins, extend the functionality of a software application.

They can be loaded and unloaded at runtime, allowing the application to be augmented with additional features or capabilities without requiring recompilation.

**Plugin libraries** provide a flexible and modular approach to software development, enabling developers to enhance their applications with custom functionality while maintaining a separation between core functionality and optional features.

## Basic example
_________________

Subsequently, a `shared library` containing a basic addition function will be dynamically loaded into a `main` executable.

???+ note
    For convenience, the following code will be written in `C++`.

    Nevertheless, the following code would also work in `C` with a few changes.

The codebase is defined below:

=== "Windows"

    ```bash
    .
    ├── addition.cpp
    ├── addition.hpp
    ├── addition.dll
    └── main.cpp

    0 directories, 3 files
    ```

    ``` cpp title="addition.hpp" linenums="1"
    extern "C" int __declspec(dllexport) addition(int x, int y);
    ```

    ``` cpp title="addition.cpp" linenums="1"
    #include "addition.hpp"

    int addition(int x, int y)
    {
        return x + y;
    }
    ```

    ``` cpp title="main.cpp" linenums="1"
    #include <iostream>

    #include <windows.h>

    typedef int (*PluginFunction)(int, int);

    int main(int, char**)
    {
        HINSTANCE plugin = LoadLibrary(TEXT("addition.dll"));

        if (!plugin)
        {
            std::cerr << "Failed to load the plugin: " << GetLastError() << std::endl;

            return 1;
        }

        PluginFunction pluginFunction = reinterpret_cast<PluginFunction>(GetProcAddress(plugin, "addition"));

        if(!pluginFunction)
        {
            std::cerr << "Failed to find the plugin function: " << GetLastError() << std::endl;

            FreeLibrary(plugin);

            return 1;
        }

        std::cout << pluginFunction(5, 3);

        FreeLibrary(plugin);

        return 0;
    }
    ```

    The provided `C++` code demonstrates the dynamic loading of a shared library (`plugin`) at runtime to extend program functionality.

    It begins by including `windows.h`, the necessary header for dynamic library loading.

    The program defines a type alias for a function pointer representing the desired plugin function's signature.

    In the main function, it attempts to load the shared library `addition.dll` using `LoadLibrary`, retrieving a handle to it.

    Then, it looks up the symbol `addition` within the library using `GetProcAddress`, casting it to the appropriate function pointer type.

    If successful, it invokes the function with specific arguments and prints the result.

    Finally, the program closes the library handle using `FreeLibrary` before exiting.

=== "Unix-like"

    ```bash
    .
    ├── addition.cpp
    ├── addition.hpp
    ├── libAddition.so
    └── main.cpp

    0 directories, 3 files
    ```

    ``` cpp title="addition.hpp" linenums="1"
    extern "C" int addition(int x, int y);
    ```

    ``` cpp title="addition.cpp" linenums="1"
    #include "addition.hpp"

    int addition(int x, int y)
    {
        return x + y;
    }
    ```

    ``` cpp title="main.cpp" linenums="1"
    #include <iostream>

    #include <dlfcn.h>

    typedef int (*PluginFunction)(int, int);

    int main(int, char**)
    {
        void* pluginHandle = dlopen("./libAddition.so", RTLD_LAZY);

        if (!pluginHandle)
        {
            std::cerr << "Failed to load the plugin: " << dlerror() << std::endl;

            return 1;
        }

        PluginFunction pluginFunction = reinterpret_cast<PluginFunction>(dlsym(pluginHandle, "addition"));

        if(!pluginFunction)
        {
            std::cerr << "Failed to find the plugin function: " << dlerror() << std::endl;

            dlclose(pluginHandle);

            return 1;
        }

        std::cout << pluginFunction(5, 3);

        dlclose(pluginHandle);

        return 0;
    }
    ```

    The provided `C++` code demonstrates the dynamic loading of a shared library (`plugin`) at runtime to extend program functionality.

    It begins by including `dlfcn.h`, the necessary header for dynamic library loading.

    The program defines a type alias for a function pointer representing the desired plugin function's signature.

    In the main function, it attempts to load the shared library `addition.dll` using `dlopen`, retrieving a handle to it.

    Then, it looks up the symbol `addition` within the library using `dlsym`, casting it to the appropriate function pointer type.

    If successful, it invokes the function with specific arguments and prints the result.

    Finally, the program closes the library handle using `dlclose` before exiting.

???+ info
    Plugins are often dynamically linked libraries (`DLLs` or `shared objects`).

    When a `C++` compiler generates these libraries, it might mangle the function names.

    However, declaring these functions with `#!C++ extern "C"` ensures that the symbols exported by the library maintain a consistent, unmangled naming convention, making it easier for other programs to dynamically load and use the functions.

## CMake
_________________

The following `CMakeLists.txt` defines a project named `PLUGIN_LIBRARY`, creating a **plugin library** named `Addition` from source files `addition.cpp` and `addition.h^^`, and links it to an executable named `main` compiled from `main.c`.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(PLUGIN_LIBRARY LANGUAGES C CXX)

add_library(Addition MODULE addition.cpp addition.hpp)

add_executable(main main.cpp)
```

???+ info
    A `MODULE` library is a plugin that may not be linked by other targets, but may be dynamically loaded at runtime using dlopen-like functionality.