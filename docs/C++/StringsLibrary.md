---
hide:
  - navigation
---

# **String Library**

In the `C++ Standard Library`, a character is an object which, when treated sequentially, can represent text.

## Library components

The `C++` strings library includes the following components:

### Character traits

[`std::char_traits`](https://en.cppreference.com/w/cpp/string/char_traits) is a template class in the `C++ Standard Library` that defines various properties and operations for character types used by the `std::basic_string` and `std::basic_string_view` classes.

It provides a set of static member functions for character manipulation, including comparison, assignment, length calculation, and character search.

The `char_traits` class template serves as a basis for explicit instantiations.

The user can provide a specialization for any custom character types.

Several explicit specializations are provided for the standard character types:

| Character trait                        | Type       |
| :------------------------------------- | :--------- |
| `std::char_traits<char>`               | `char`     |
| `std::char_traits<wchar_t>`            | `wchar_t`  |
| `std::char_traits<char8_t>` (`C++20`)  | `char8_t`  |
| `std::char_traits<char16_t>` (`C++11`) | `char16_t` |
| `std::char_traits<char32_t>` (`C++11`) | `char32_t` |

### String classes

The class template [`std::basic_string`](https://en.cppreference.com/w/cpp/string/basic_string) generalizes how sequences of characters are manipulated and stored.

String creation, manipulation, and destruction are all handled by a convenient set of class methods and related functions.

Several specializations of `std::basic_string` are provided for commonly-used types:

| Type                       | Definition                    |
| :------------------------- | :---------------------------- |
| `std::string`              | `std::basic_string<char>`     |
| `std::wstring`             | `std::basic_string<wchar_t>`  |
| `std::u8string` (`C++20`)  | `std::basic_string<char8_t>`  |
| `std::u16string` (`C++11`) | `std::basic_string<char16_t>` |
| `std::u32string` (`C++11`) | `std::basic_string<char32_t>` |

### String view classes

The class template [`std::basic_string_view`](https://en.cppreference.com/w/cpp/string/basic_string_view) provides a lightweight object that offers **read-only** access to a string or a part of a string using an interface similar to the interface of `std::basic_string`.

Several specializations of `std::basic_string_view` are provided for commonly-used types:

| Type                            | Definition                         |
| :------------------------------ | :--------------------------------- |
| `std::string_view` (`C++17`)    | `std::basic_string_view<char>`     |
| `std::wstring_view` (`C++17`)   | `std::basic_string_view<wchar_t>`  |
| `std::u8string_view` (`C++20`)  | `std::basic_string_view<char8_t>`  |
| `std::u16string_view` (`C++17`) | `std::basic_string_view<char16_t>` |
| `std::u32string_view` (`C++17`) | `std::basic_string_view<char32_t>` |

Compared to `std::basic_string` classes, `std::string_view` is a non-owning, lightweight reference to a sequence of characters, designed for efficient read-only access.

It does not manage memory or modify the string content, making it ideal for passing substrings or immutable string data without the overhead of copying.

### Null-terminated sequence utilities

`Null-terminated character sequences (NTCTS)` are sequences of characters that are terminated by a null character.

The strings library provides functions to create, inspect, and modify such sequences:

* [Null-terminated byte strings](https://en.cppreference.com/w/cpp/string/byte)
* [Null-terminated multibyte strings](https://en.cppreference.com/w/cpp/string/multibyte)
* [Null-terminated wide strings](https://en.cppreference.com/w/cpp/string/wide)