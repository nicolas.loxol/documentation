---
hide:
  - navigation
---

# **Interoperability of Libraries**

Libraries written in `C++`, compiled with different `compilers`, or even just with different releases of the same compiler, often cannot be linked together.

Sometimes, there exists basic `ABI (Application Binary Interface)` compatibility (e.g., `GCC` and `Clang`), but it is likely that both the library and the application sourced must be recompiled with the same compiler to link them successfully.

Nevertheless, libraries written in `C` can be linked even with applications written in `C++` due to language inheritance.

Libraries written in `C++` work too, as long as its symbols are exposed through a `C interface` declared with `extern "C"`.

## `#!C++ extern "C"`

`extern "C"` is a linkage specification that makes a function-name in `C++` have `C` linkage (compiler does not mangle the name) so that client `C` code can link to a function using a `C` compatible header file that contains just the declaration of the function.

The function definition is contained in a binary format (that was compiled by the `C++` compiler) that the client `C` linker will then link to using the `C` name.

Since `C++` has overloading of function names and `C` does not, the `C++` compiler cannot just use the function name as a unique id to link to, so it mangles the name by adding information about the arguments.

A `C` compiler does not need to mangle the name since function names in `C` cannot be overloaded.

State that a function has `extern "C"` linkage in `C++` mean that the `C++` compiler does not add argument/parameter type information to the name used for linkage.

`extern "C"` linkage can be specified to each individual declaration/definition explicitly or use a block to group a sequence of declarations/definitions to have a certain linkage:

=== "Static library"

    ``` cpp
    extern "C" void foo(int);
    extern "C"
    {
      void f(int a);
      int i;
    }
    ```

=== "Dynamic/Shared library"

    ``` cpp
    extern "C" void EXPORT foo(int);
    extern "C"
    {
      void EXPORT f(int a);
      int i;
    }
    ```

## `#!C++ __cplusplus`

`__cplusplus` is a predefined macro in `C++` compilers that evaluates to an integer representing the version of the `C++` standard being used.

This example of code is very common:

``` cpp
#ifdef __cplusplus
extern "C" {
#endif

// all of the legacy C code here

#ifdef __cplusplus
}
#endif
```

What this accomplishes is that it allows to use that `C` header file with the `C++` code, because the macro [`__cplusplus`](https://en.cppreference.com/w/cpp/preprocessor/replace#Predefined_macros) will be defined.

But it can also still be used with the legacy `C` code, where the macro is not defined, so the uniquely `C++` construct would not be seen.

## `#!C++ _WIN32` and `#!C++ __unix__`

`_WIN32` and `__unix__` are predefined macro by compilers to indicate the target platform or operating system during compilation.

Using conditional compilation based on these macros, portable code across different platforms can be written.

``` cpp
#ifdef _WIN32
  // WINDOWS CODE
#elifdef __unix__
  // UNIX CODE
#endif
```