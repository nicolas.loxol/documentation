---
hide:
  - navigation
---

# **Static library**
_________________

A **static library**, also known as an `archive` on `Unix-like` systems is a collection of object files that are linked into the executable at compile time.

It provides a way to package functions and data so that they can be used by multiple programs.

**Static libraries** have a `.lib` extension on `Windows` and `.a` on `Unix-like` systems.

## Basic C/C++ code
_________________

Subsequently, a **static library** composed of a header file `addition.h` and a source file `addition.c` will be built.

The library will then be used in a `main.c` file.

The codebase is defined below:

```bash
.
├── addition.c
├── addition.h
├── main.c
└── CMakeLists.txt

0 directories, 4 files
```

``` c title="addition.h" linenums="1"
int addition(int x, int y);
```

``` c title="addition.c" linenums="1"
#include "addition.h"

int addition(int x, int y)
{
    return x + y;
}
```

``` c title="main.c" linenums="1"
#include "addition.h"

int main(int, char**)
{
    int res = addition(5, 3);

    return 0;
}
```

## Creation of a static library

_________________

### Compilation and assemblage

First, the library code needs to be converted to object files:

=== "MSVC"

    ``` bash
    cl /c addition.c
    ```

    The compile options `/c` `compiles` or `assembles` the source files, but does not `link` them.

    The linking stage is simply not performed.

    The ultimate output is an `object file` for each source file.

    As a result, the above command produces the object file `addition.obj`.

=== "Unix-like"

    ``` bash
    gcc -c addition.c
    ```

    The compile options `-c` `compiles` or `assembles` the source files, but does not `link` them.

    The linking stage is simply not performed.

    The ultimate output is an `object file` for each source file.

    As a result, the above command produces the object file `addition.o`.

### Creation

The next step is to create an archive containing the object file:

=== "MSVC"

    ``` bash
    lib /OUT:Addition.lib addition.obj
    ```

    The above command creates a **static library** named `Addition.lib` from the object file `addition.obj` using the `Microsoft Library Manager tool`.

=== "Unix-like"

    ``` bash
    ar rcs libAddition.a addition.o
    ```

    `ar` is a utility used to create, modify and extract archive files.

    The `rcs` options mean:

    * r: Replace existing code by inserting object files
    * c: Create a new archive file if it does not exist.
    * s: Write an index for the archive file.

    ???+ info
        In `Unix-like` systems, **static libraries** conventionally use the `lib` prefix in their filenames.

        This prefix distinguishes them from other files and indicates their purpose.


## Linking to a static library

_________________

The **static library** previously created can be specified in the `main` program during the linking phase.

The following command compiles the `main.c` source file and links it with the `Addition static library` to produce an executable.

=== "MSVC"

    ``` bash
    cl main.c Addition.lib
    ```

=== "Unix-like"

    ``` bash
    gcc main.c -L. -lAddition
    ```

    The path and name of the **static library** are manually specified using the `-L` and `-l` options, respectively.

    ???+ warning
        During linking phase, the linker does not require the `lib` prefix.

## Extra

_________________

### CMake

The following `CMakeLists.txt` defines a project named `STATIC_LIBRARY`, creating a **static library** named `Addition` from source files `addition.c` and `addition.h`, and links it to an executable named `main` compiled from `main.c`.

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.29)

project(STATIC_LIBRARY LANGUAGES C CXX)

add_library(Addition STATIC addition.c addition.h)

add_executable(main main.c)

target_link_libraries(main PRIVATE Addition)
```