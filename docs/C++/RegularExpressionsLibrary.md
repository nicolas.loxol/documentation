---
hide:
  - navigation
---

# **Regular Expression Library (`C++11`)**

The **regular expressions library** provides a class that represents [regular expressions](https://en.wikipedia.org/wiki/Regular_expression), which are a kind of mini-language used to perform pattern matching within strings.

The **regular expressions library** is defined on the system header `<regex>`.

The library support the following regular expression grammars:

* [Modified ECMAScript regular expression grammar](https://en.cppreference.com/w/cpp/regex/ecmascript) (This is the default grammar)
* [Basic POSIX regular expression grammar](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03)
* [Extended POSIX regular expression grammar](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04)
* The regular expression grammar used by the `awk` utility in `POSIX`
* The regular expression grammar used by the `grep` utility in `POSIX`
* The regular expression grammar used by the `grep` utility, with the `-E` option, in `POSIX`

## std::basic_regex

The class template `std::basic_regex` provides a general framework for holding regular expressions.

Several `typedefs` for common character types are provided:

| Type          | Definition                           |
| :------------ | :----------------------------------- |
| `std::regex`  | `std::basic_regex<char>`             |
| `std::wregex` | `std::basic_regex<wchar_t>`          |

The constructors enable the creation of regex patterns from various string sources and offer different [customization options](https://en.cppreference.com/w/cpp/regex/syntax_option_type) through flags.

``` cpp
std::regex("ab*");
std::regex("ab*", std::regex_constants::grep);
std::regex("ab*", std::regex::icase|std::regex_constants::multiline);
```

## std::match_results

[`std::match_results`](https://en.cppreference.com/w/cpp/regex/match_results) is a template class in the `C++ Standard Library` that holds the results of a match performed by regular expression algorithms like `std::regex_match` and `std::regex_search`.

It is essentially a collection of sub-matches, each representing a portion of the target sequence that matched a part of the regular expression, including the entire match and any capturing groups.

`std::match_results` provides methods to query the match details, such as the matched string, positions of matches, and the number of capturing groups.

Several specializations for common character sequence types are provided:

| Type          | Definition                                          |
| :------------- | :------------------------------------------------- |
| `std::cmatch`  | `std::match_results<const char*>`                  |
| `std::wcmatch` | `std::match_results<const wchar_t*>`               |
| `std::smatch`  | `std::match_results<std::string::const_iterator>`  |
| `std::wsmatch` | `std::match_results<std::wstring::const_iterator>` |

## std::regex_iterator

[`std::regex_iterator`](https://en.cppreference.com/w/cpp/regex/regex_iterator) is a template class in the `C++ Standard Library` used to iterate over all matches of a regular expression within a target sequence.

It provides a way to traverse multiple matches in a string, allowing for comprehensive pattern-based text processing.

Several specializations for common character sequence types are defined:

| Type                    | Definition                                          |
| :---------------------- | :-------------------------------------------------- |
| `std::cregex_iterator`  | `std::regex_iterator<const char*>`                  |
| `std::wcregex_iterator` | `std::regex_iterator<const wchar_t*>`               |
| `std::sregex_iterator`  | `std::regex_iterator<std::string::const_iterator>`  |
| `std::wsregex_iterator` | `std::regex_iterator<std::wstring::const_iterator>` |

## Algorithms

### std::regex_match

[`std::regex_match`](https://en.cppreference.com/w/cpp/regex/regex_match) is a function in the `C++ Standard Library` used to match an entire target character sequence, which may be specified as `std::string`, a `C-string`, or an iterator pair against a regular expression.

It takes a target string and a `std::basic_regex` object as input, along with optional match results and flags, to determine if the entire string conforms to the specified pattern.

The function returns true if a match exists, false otherwise.

Unlike `std::regex_search`, which can find patterns anywhere within the string, `std::regex_match` requires the pattern to match the whole string from beginning to end.

This function is essential for validating input formats, such as email addresses, URLs, or other structured data, ensuring that the input meets the exact criteria defined by the regular expression.

``` cpp linenums="1"
std::regex emailPattern(R"((\w+)(\.\w+)*@(\w+)(\.\w+)+)");

std::string valid_email = "example.email@example.com";
std::string invalid_email = "invalid-email.com";

if (std::regex_match(valid_email, emailPattern))
{
    std::cout << valid_email << " is a valid email address." << std::endl;
}

if (std::regex_match(invalid_email, emailPattern))
{
    std::cout << invalid_email << " is a valid email address." << std::endl;
}
```

???+ info
    Raw string literals are string literals with a prefix containing `R`.

    They do not escape any character, which means anything between the delimiters becomes part of the string.

### std::regex_search

[`std::regex_search`](https://en.cppreference.com/w/cpp/regex/regex_search) is a function in the `C++ Standard Library` used to search for occurrences of a regular expression pattern within a string.

Unlike `std::regex_match`, which requires the entire string to match the pattern, `std::regex_search` can find the pattern anywhere within the string.

This makes it suitable for tasks such as searching for substrings, extracting parts of a string, or finding multiple occurrences of a pattern.

It takes the target string and a `std::basic_regex` object as input, and it can optionally accept match results and flags to customize the search behavior.

The function returns true if a match exists, false otherwise

``` cpp linenums="1"
std::regex wordPattern(R"(\b\w+\b)");

std::string text = "This is a simple example.";

std::smatch match;

if (std::regex_search(text, match, wordPattern))
{
  std::cout << "Found a word: " << match.str() << std::endl;
}

std::sregex_iterator begin(text.begin(), text.end(), wordPattern);
std::sregex_iterator end;

for (std::sregex_iterator i = begin; i != end; ++i)
{
  std::cout << i->str() << std::endl;
}
```

### std::regex_replace

[`std::regex_replace`](https://en.cppreference.com/w/cpp/regex/regex_replace) is a function in the `C++ Standard Library` used to search for patterns within a string and replace them with a specified replacement.

It utilizes regular expressions to identify parts of the string that match the pattern and substitutes them with the replacement text.

This function is particularly useful for tasks such as formatting strings, sanitizing input, or performing search-and-replace operations in text processing.

``` cpp
std::regex digitPattern(R"(\d)");

std::string text = "Phone number: 123-456-7890";

std::string replacedText = std::regex_replace(text, digitPattern, "X");

std::regex datePattern(R"((\d{4})-(\d{2})-(\d{2}))");
std::string dateText = "Today's date is 2024-07-12.";

std::string formattedDateText = std::regex_replace(dateText, datePattern, "$2/$3/$1");
```

???+ info
	Capturing groups in regular expressions are a powerful feature that allows portions of a match to be extracted for further use.

	A capturing group can be created by enclosing part of a pattern in parentheses