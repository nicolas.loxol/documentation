---
hide:
  - navigation
  - toc
---

# **Tools**

Here is the complete list of the **C/C++ tools** cover in the documentation:

_________________

## IDE
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[Visual studio](tools/VisualStudio.md)** | Visual Studio is an integrated development environment (IDE) from Microsoft | `Proprietary (EULA)` | :material-microsoft-windows:
**[Visual studio code](tools/VisualStudioCode.md)** | Visual Studio Code, also commonly referred to as VS Code, is a source-code editor made by Microsoft with the Electron Framework | `MIT`| :material-microsoft-windows: :simple-linux:

_________________

## Compiler
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[GCC](tools/GCC.md)** | The GNU Compiler Collection (GCC) is an optimizing compiler produced by the GNU Project | `GPLv3` | :simple-linux:
**[GNU Make](tools/Make.md)** | GNU Make is a build automation tool that builds executable programs and libraries from source code by reading files called Makefiles | `GPLv3` | :simple-linux:
**[MSbuild](tools/MSbuild.md)** | MSBuild, which stands for Microsoft Build Engine, is a build platform for managed code | `MIT` | :material-microsoft-windows:
**[Clang](tools/Clang.md)** | Clang is a compiler front end for the C and C++ programming languages | `Apache 2.0` | :material-microsoft-windows: :simple-linux:
**[Ninja](tools/Ninja.md)** | Ninja is a small build system with a focus on speed | `Apache 2.0` | :material-microsoft-windows: :simple-linux:

## Recompiler
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[CCache](tools/CCache.md)** | CCache is a software development tool used to speed up recompilation of C/C++ | `GPL` | :material-microsoft-windows: :simple-linux:

_________________

## Debugger
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[GDB](tools/GDB.md)** | The GNU Debugger (GDB) is a portable debugger that runs on many Unix-like systems | `GPLv3` | :simple-linux:
**[Visual studio debugger](tools/VisualStudio.md/#visual-studio-debugger)** | The official debugger of Visual Studio from Microsoft | `Proprietary (EULA)` | :material-microsoft-windows:

_________________

## Profiler
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[Valgrind](tools/Valgrind.md)** | Valgrind is a programming tool for memory debugging, memory leak detection, and profiling | `GPL` | :simple-linux:
**[Gprof](tools/Gprof.md)** | Gprof is a performance analysis tool for Unix applications | `GPL` | :simple-linux:
**[Visual studio profiler](tools/VisualStudio.md/#visual-studio-profiler)** | The official profiler of Visual Studio from Microsoft | `Proprietary (EULA)` | :material-microsoft-windows:

_________________

## Code coverage
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[gcov](tools/Gcov.md)** | Gcov is a source code coverage analysis and statement-by-statement profiling tool | `GPL` | :simple-linux:
**[OpenCppCoverage](tools/OpenCppCoverage.md)** | OpenCppCoverage is an open source code coverage tool for C/C++ under Windows | `GPLv3` | :material-microsoft-windows:

_________________

## Build system
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[CMake](tools/CMake.md)** | CMake is cross-platform free and open-source software for build automation, testing, packaging and installation of software | `BSD 3` | :material-microsoft-windows: :simple-linux:

_________________

## Packet manager
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[Conan](tools/Conan.md)** | Conan is a decentralized, open-source package manager for C and C++ that simplifies dependency management and builds configuration | `MIT` | :material-microsoft-windows: :simple-linux:
**[vcpkg](tools/Vcpkg.md)** | Vcpkg is an open-source package manager for C and C++ libraries that simplifies the installation and management of dependencies across different platforms | `MIT` | :material-microsoft-windows: :simple-linux:

_________________

## Analysing and code formatting
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[Clang-Tidy](tools/Clang-Tidy.md)** | Clang-Tidy is a C/C++ "linting" tool that provides static code analysis and automatically fixes coding style issues and potential bugs | `Apache 2.0` | :material-microsoft-windows: :simple-linux:
**[Clang-Format](tools/Clang-Format.md)** | Clang-Format is a tool that automatically formats C/C++, and other source code files according to specified style guidelines | `Apache 2.0` | :material-microsoft-windows: :simple-linux:
**[Include What You Use](tools/IncludeWhatYouUse.md)** | Include What You Use (IWYU) is a tool for improving build times and code quality in C/C++ by analyzing and suggesting the minimal necessary #include directives | `LLVM` | :material-microsoft-windows: :simple-linux:
**[Link what you use](tools/LinkWhatYouUse.md)** | Link What You Use (LWYU) is a build optimization technique that ensures only the necessary libraries are linked to a C/C++ project to reduce binary size and improve build times. | `GPL` | :simple-linux:

_________________

## Documentation
_________________

Name | Description | License | Platform
:------------: | :-------------: | :------------: | :------------:
**[Doxygen](tools/Doxygen.md)** | Doxygen is a documentation generator and static analysis tool for software source trees | `GPLv2` | :material-microsoft-windows: :simple-linux:
**[MkDocs](tools/MkDocs.md)** | MkDocs is a fast, simple and downright gorgeous static site generator that is geared towards building project documentation | `BSD-2` | :material-microsoft-windows: :simple-linux: